import images_input
from calibration_util import *

__author__ = 'sasha'


def calibrate(images_paths, original_images, pattern, square_size=1.0, debug_found=False):
    if not images_paths or not original_images or not pattern:
        print 'Check parameters passed.'
        return

    print '\n========================================='
    print 'Camera Calibration - OpenCV - {}'.format(pattern)
    print '=========================================\n'

    '''
    Achar os centros para cada imagem, seja por arquivo ou o usuario marcando
    '''
    # distancia entre os pontos de controle
    pattern_size = (10, 7)

    # posicao dos pontos de controle do padrao no plano z do mundo
    pattern_points = np.zeros((np.prod(pattern_size), 3), np.float32)
    pattern_points[:, :2] = np.indices(pattern_size).T.reshape(-1, 2)
    pattern_points *= square_size

    obj_points = []
    img_points = []
    h, w = 0, 0
    i = 0

    # region loop para cada imagem pegar os centros
    for img in original_images:
        h, w = img.shape[:2]
        new_points = False

        # pega centros do padrao
        try:
            centers = np.load(images_paths[i] + '.npy')
            print 'Achou centros em arquivo para imagem ' + images_paths[i]
            found = True
        except IOError:
            if pattern == 'circle':
                found, centers = cv2.findCirclesGrid(img, pattern_size)
            elif pattern == 'ring':
                found, centers = RingsGrid.findRingsGrid(img, pattern_size)

            if found:
                new_points = True

        if found:
            if new_points:
                np.save(images_paths[i], centers)

            if debug_found:
                vis = img.copy()
                cv2.drawChessboardCorners(vis, pattern_size, centers, found)
                cv2.imshow('Centros', vis)
                # cv2.imshow('undistorted', vis)
                cv2.waitKey(0)
        else:
            print('{} pattern not found'.format(pattern))
            continue

        img_points.append(centers.reshape(-1, 2))
        obj_points.append(pattern_points)

        print('   Done with centers for image.')
        i += 1
    # endregion

    '''
    Calibrar
    '''
    # Resolver a calibracao dados os pontos ideais e os pontos nas imagens
    rms, camera_matrix, dist_coefs, rvecs, tvecs = cv2.calibrateCamera(obj_points, img_points, (w, h), None, None)

    '''
    Undistort images
    '''

    print camera_matrix, dist_coefs
    # newcameramtx, roi = cv2.getOptimalNewCameraMatrix(camera_matrix, dist_coefs, (w, h), 1, (w, h))
    # print newcameramtx, roi
    for img in original_images:
        # undistort
        undistort_img = cv2.undistort(img, camera_matrix, dist_coefs)  # ,None, newcameramtx)

        DrawingUtils.mostra_imagem('img', img, 50, 50, True)
        DrawingUtils.mostra_imagem('undistort_img', undistort_img, 1300, 200, True)
        cv2.waitKey(0)
    cv2.destroyAllWindows()

    '''
    Resultados
    '''
    print('\n---------------------------------------------')
    print 'Resultado - Calibracao - {}\n'.format(pattern)
    print "RMS: ", rms

    rangeOk = cv2.checkRange(camera_matrix) and cv2.checkRange(dist_coefs)
    if rangeOk:
        totalReprojErr, CollinearityErr = computeReprojectionAndCollinearityErrors(original_images, pattern_size,
                                                                                   obj_points, img_points, rvecs, tvecs,
                                                                                   camera_matrix, dist_coefs)
        print "Avg re-projection error = ", totalReprojErr
        print "Collinearity error = ", CollinearityErr
        print "Camera matrix: = \n", camera_matrix
        print "Distortion coefficients: ", dist_coefs.ravel()

    print('---------------------------------------------')
    cv2.destroyAllWindows()


if __name__ == "__main__":

    # images_paths, images, pattern = images_input.get('ring', '640x480')
    images_paths, images, pattern = images_input.getImages('ring', 'ankur')

    if len(images) >= 1:
        calibrate(images_paths, images, pattern, debug_found=False)
    else:
        print "No images."
