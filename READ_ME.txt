Projeto de Dissertacao de Mestrado em Informatica
Autor: Sasha Nicolas

Calibracao avancada de camera usando refinamento iterativo de pontos de controle,
    segmentacao adaptativa de elipses e nova metrica de avaliacao de erro

# Pastas

images/                             - pasta para colocar imagens qq
manuel_data/                        - pasta com .txt dos dados da implementacao de calibracaod e camera do manuel e do gattass
misc/                               - conteudo miscigenado, inclui codigo-testes
pattens/                            - contem as imagens dos sets de caloibracao
resultados/                         - contem resultados em excel e txt

