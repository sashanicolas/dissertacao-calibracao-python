import settings_flags

flags = [
    # 'USE_PRAKASH_THRESHOLD_ADAP',
    # 'OPTIMIZE_WITH_COLLINEARITY'
]

skip_configs = [
    # {
    #     'USE_PRAKASH_THRESHOLD_ADAP': True,
    #     'OPTIMIZE_WITH_COLLINEARITY': True
    # },
    # {
    #     'OPTIMIZE_WITH_COLLINEARITY': False,
    #     'OPTIMIZE_EXTRINSICS': True,
    #     'FIRST_EXT_SECOND_OPTM': False
    # },
    # {
    #     'OPTIMIZE_WITH_COLLINEARITY': True,
    #     'OPTIMIZE_EXTRINSICS': False,
    #     'FIRST_EXT_SECOND_OPTM': False
    # },
    # {
    #     'OPTIMIZE_WITH_COLLINEARITY': False,
    #     'OPTIMIZE_EXTRINSICS': False,
    #     'FIRST_EXT_SECOND_OPTM': False
    # }
]


def set_flags_config(config):
    value_bits = '{0:0{1}b}'.format(config, len(flags))
    config_text = ""
    for i in range(0, len(flags)):
        exec ('settings_flags.' + flags[i] + ' = ' + str(bool(int(value_bits[i]))))
        config_text += flags[i] + ' = ' + str(bool(int(value_bits[i]))) + '\n'

    return config_text


def skip_config():
    for i in range(0, len(skip_configs)):
        match_condition = True
        for key, value in skip_configs[i].iteritems():
            if eval('settings_flags.'+key) is not value:
                match_condition = False
                break
        if match_condition is True:
            return True
    return False


if __name__ == '__main__':
    for i in range(0, 4):
        set_flags_config(i)
        print skip_config()
