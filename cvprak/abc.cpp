#include "abc.hpp"
//#include "my_cpp_library.hpp" // This is what we want to make available in python. It uses OpenCV to perform some processing.

#include "numpy/ndarrayobject.h"
#include "opencv2/core/core.hpp"
#include "opencv2/opencv.hpp"
#include "opencv2/gpu/gpu.hpp"
//#include "opencv2/highgui/highgui.hpp"

#include <numeric>
#include <iostream>
#include <cmath>

using namespace cv;

// The following conversion functions are taken from OpenCV's cv2.cpp file inside modules/python/src2 folder.
static PyObject* opencv_error = 0;

static int failmsg(const char *fmt, ...)
{
    char str[1000];

    va_list ap;
    va_start(ap, fmt);
    vsnprintf(str, sizeof(str), fmt, ap);
    va_end(ap);

    PyErr_SetString(PyExc_TypeError, str);
    return 0;
}

class PyAllowThreads
{
public:
    PyAllowThreads() : _state(PyEval_SaveThread()) {}
    ~PyAllowThreads()
    {
        PyEval_RestoreThread(_state);
    }
private:
    PyThreadState* _state;
};

class PyEnsureGIL
{
public:
    PyEnsureGIL() : _state(PyGILState_Ensure()) {}
    ~PyEnsureGIL()
    {
        PyGILState_Release(_state);
    }
private:
    PyGILState_STATE _state;
};

#define ERRWRAP2(expr) \
try \
{ \
    PyAllowThreads allowThreads; \
    expr; \
} \
catch (const cv::Exception &e) \
{ \
    PyErr_SetString(opencv_error, e.what()); \
    return 0; \
}

static PyObject* failmsgp(const char *fmt, ...)
{
  char str[1000];

  va_list ap;
  va_start(ap, fmt);
  vsnprintf(str, sizeof(str), fmt, ap);
  va_end(ap);

  PyErr_SetString(PyExc_TypeError, str);
  return 0;
}

static size_t REFCOUNT_OFFSET = (size_t)&(((PyObject*)0)->ob_refcnt) +
    (0x12345678 != *(const size_t*)"\x78\x56\x34\x12\0\0\0\0\0")*sizeof(int);

static inline PyObject* pyObjectFromRefcount(const int* refcount)
{
    return (PyObject*)((size_t)refcount - REFCOUNT_OFFSET);
}

static inline int* refcountFromPyObject(const PyObject* obj)
{
    return (int*)((size_t)obj + REFCOUNT_OFFSET);
}

class NumpyAllocator : public MatAllocator
{
public:
    NumpyAllocator() {}
    ~NumpyAllocator() {}

    void allocate(int dims, const int* sizes, int type, int*& refcount,
                  uchar*& datastart, uchar*& data, size_t* step)
    {
        PyEnsureGIL gil;

        int depth = CV_MAT_DEPTH(type);
        int cn = CV_MAT_CN(type);
        const int f = (int)(sizeof(size_t)/8);
        int typenum = depth == CV_8U ? NPY_UBYTE : depth == CV_8S ? NPY_BYTE :
                      depth == CV_16U ? NPY_USHORT : depth == CV_16S ? NPY_SHORT :
                      depth == CV_32S ? NPY_INT : depth == CV_32F ? NPY_FLOAT :
                      depth == CV_64F ? NPY_DOUBLE : f*NPY_ULONGLONG + (f^1)*NPY_UINT;
        int i;
        npy_intp _sizes[CV_MAX_DIM+1];
        for( i = 0; i < dims; i++ )
        {
            _sizes[i] = sizes[i];
        }

        if( cn > 1 )
        {
            /*if( _sizes[dims-1] == 1 )
                _sizes[dims-1] = cn;
            else*/
                _sizes[dims++] = cn;
        }

        PyObject* o = PyArray_SimpleNew(dims, _sizes, typenum);

        if(!o)
        {
            CV_Error_(CV_StsError, ("The numpy array of typenum=%d, ndims=%d can not be created", typenum, dims));
        }
        refcount = refcountFromPyObject(o);

        npy_intp* _strides = PyArray_STRIDES(o);
        for( i = 0; i < dims - (cn > 1); i++ )
            step[i] = (size_t)_strides[i];
        datastart = data = (uchar*)PyArray_DATA(o);
    }

    void deallocate(int* refcount, uchar*, uchar*)
    {
        PyEnsureGIL gil;
        if( !refcount )
            return;
        PyObject* o = pyObjectFromRefcount(refcount);
        Py_INCREF(o);
        Py_DECREF(o);
    }
};

NumpyAllocator g_numpyAllocator;

enum { ARG_NONE = 0, ARG_MAT = 1, ARG_SCALAR = 2 };

static int pyopencv_to(const PyObject* o, Mat& m, const char* name = "<unknown>", bool allowND=true)
{
    //NumpyAllocator g_numpyAllocator;
    if(!o || o == Py_None)
    {
        if( !m.data )
            m.allocator = &g_numpyAllocator;
        return true;
    }

    if( !PyArray_Check(o) )
    {
        failmsg("%s is not a numpy array", name);
        return false;
    }

    int typenum = PyArray_TYPE(o);
    int type = typenum == NPY_UBYTE ? CV_8U : typenum == NPY_BYTE ? CV_8S :
               typenum == NPY_USHORT ? CV_16U : typenum == NPY_SHORT ? CV_16S :
               typenum == NPY_INT || typenum == NPY_LONG ? CV_32S :
               typenum == NPY_FLOAT ? CV_32F :
               typenum == NPY_DOUBLE ? CV_64F : -1;

    if( type < 0 )
    {
        failmsg("%s data type = %d is not supported", name, typenum);
        return false;
    }

    int ndims = PyArray_NDIM(o);
    if(ndims >= CV_MAX_DIM)
    {
        failmsg("%s dimensionality (=%d) is too high", name, ndims);
        return false;
    }

    int size[CV_MAX_DIM+1];
    size_t step[CV_MAX_DIM+1], elemsize = CV_ELEM_SIZE1(type);
    const npy_intp* _sizes = PyArray_DIMS(o);
    const npy_intp* _strides = PyArray_STRIDES(o);
    bool transposed = false;

    for(int i = 0; i < ndims; i++)
    {
        size[i] = (int)_sizes[i];
        step[i] = (size_t)_strides[i];
    }

    if( ndims == 0 || step[ndims-1] > elemsize ) {
        size[ndims] = 1;
        step[ndims] = elemsize;
        ndims++;
    }

    if( ndims >= 2 && step[0] < step[1] )
    {
        std::swap(size[0], size[1]);
        std::swap(step[0], step[1]);
        transposed = true;
    }

    if( ndims == 3 && size[2] <= CV_CN_MAX && step[1] == elemsize*size[2] )
    {
        ndims--;
        type |= CV_MAKETYPE(0, size[2]);
    }

    if( ndims > 2 && !allowND )
    {
        failmsg("%s has more than 2 dimensions", name);
        return false;
    }

    m = Mat(ndims, size, type, PyArray_DATA(o), step);

    if( m.data )
    {
        m.refcount = refcountFromPyObject(o);
        m.addref(); // protect the original numpy array from deallocation
                    // (since Mat destructor will decrement the reference counter)
    };
    m.allocator = &g_numpyAllocator;

    if( transposed )
    {
        Mat tmp;
        tmp.allocator = &g_numpyAllocator;
        transpose(m, tmp);
        m = tmp;
    }
    return true;
}

static PyObject* pyopencv_from(const Mat& m)
{
    if( !m.data )
        Py_RETURN_NONE;
    Mat temp, *p = (Mat*)&m;
    if(!p->refcount || p->allocator != &g_numpyAllocator)
    {
        temp.allocator = &g_numpyAllocator;
        m.copyTo(temp);
        p = &temp;
    }
    p->addref();
    return pyObjectFromRefcount(p->refcount);
}

ABC::ABC() {}

ABC::~ABC() {}

// Note the import_array() from NumPy must be called else you will experience segmentation faults.
ABC::ABC(const std::string &someConfigFile)
{
  // Initialization code. Possibly store someConfigFile etc.
  import_array(); // This is a function from NumPy that MUST be called.
  // Do other stuff
}


void get_corners_roi(Point2f & inicio, Point2f & fim, Point2f center, Point side)
{
    inicio.x = max((int)((center.x - side.x/2.0f)), 0);
    inicio.y = max((int)((center.y - side.y/2.0f)), 0);
    
    fim.x = (int)((center.x + side.x/2.0f));
    fim.y = (int)((center.y + side.y/2.0f));
}

Point2f findRingCenter(vector<vector<Point> > & contours, vector<Vec4i> & hierarchy, Size s)
{
    if (contours.size()!=4){
        cout << "Contornos encontrados: " << contours.size() << endl;
        return Point2f(-1,-1);
    }
    Point2f novoCentro(0, 0);
    
    RotatedRect rr;
    int ellipses = 0;
    for(int i=0; i<4; i++){
        if( contours[i].size()>5 ){
            rr = fitEllipse( Mat(contours[i]) );
            novoCentro += rr.center;
            ellipses ++;
        }            
    }
    novoCentro.x /= (float)ellipses;
    novoCentro.y /= (float)ellipses;
    return novoCentro;
}

// The conversions functions above are taken from OpenCV. The following function is 
// what we define to access the C++ code we are interested in.
PyObject* ABC::refineUsingPrakash(PyObject* imgGrayPy, PyObject* attributesPy, PyObject* centersPy)
{
    Mat imgGray_mat;
    pyopencv_to(imgGrayPy, imgGray_mat);
    
//    std::cout << "roi size" << imgGray_mat.size() << " roi type " << imgGray_mat.type() << std::endl;
    //converter parametros
    
    double time = (double) getTickCount();
    int dim[2] = { 35, 15};
    vector<vector<Point> > contours;
    vector<Vec4i> hierarchy;
    Point2f inicio, fim;
    double factor = 4.0f;
    
    double time2 = (double) getTickCount();
    
    gpu::GpuMat imgGray_gpu;
    imgGray_gpu.upload(imgGray_mat);
    gpu::GpuMat roi, roi_4;
    
    time2 = ((double) getTickCount() - time2)/getTickFrequency();
    printf("tempo2 : %f\n", time2);
    
    for(int i=0; i< dim[0]; i++){
        for(int j=0; j< dim[1]; j++){
            get_corners_roi(inicio, fim, Point2f(135, 135), Point(250, 250));            
            roi = imgGray_gpu(Rect(inicio, fim));
            Size novoSize(roi.size().width * factor, roi.size().width * factor);
            gpu::resize(roi, roi_4, novoSize, 0, 0, INTER_CUBIC);
            
//            Mat roi_4_bin_mat(roi_4);            
//            imshow("name", roi_4_bin_mat);
//            waitKey(0);
//            destroyAllWindows();
                    
            get_contornos_adaptative_threshold(roi_4, contours, hierarchy);
            
            Point2f novoCentro = findRingCenter(contours, hierarchy, roi.size());            
            novoCentro.x /= factor;
            novoCentro.y /= factor;
            novoCentro += inicio;
            //cout << novoCentro << endl;
        }
    }
    
//    Mat centers_mat;
//    pyopencv_to(centersPy, centers_mat);
//    centers_mat.at<float>(0, 0) = 10.0f;
//    std::cout << "centers_mat size" << centers_mat.size() << 
//            " centers_mat type " << centers_mat.type() << 
//            " values: " << centers_mat << std::endl;
//    centersPy = pyopencv_from(centers_mat);

    Mat result_host(roi_4);
    
    time = ((double) getTickCount() - time)/getTickFrequency();    
    printf("tempo end : %f\n", time);
    
    return pyopencv_from(result_host);
}

void ABC::get_contornos_adaptative_threshold(
    gpu::GpuMat &roi_4, vector<vector<Point> > & contours, vector<Vec4i> & hierarchy)
{
    gpu::GaussianBlur(roi_4, roi_4, Size(5,5), 0);
    double T = findAdaptativeThreshold(roi_4);
    
    gpu::GpuMat roi_4_bin;
    gpu::threshold(roi_4, roi_4_bin, T, 255, THRESH_BINARY);
 
    gpu::GpuMat edges_gpu;
    gpu::Canny(roi_4_bin, edges_gpu, 127, 255);    

    Mat edges(edges_gpu);
    findContours(edges, contours, hierarchy, RETR_TREE, CHAIN_APPROX_NONE);    
    
}


double ABC::findAdaptativeThreshold(gpu::GpuMat &roi_gpu)
{    
    Scalar mean, stddev;
    gpu::GpuMat hist_gpu;
    
    gpu::meanStdDev(roi_gpu, mean, stddev);
    gpu::calcHist(roi_gpu, hist_gpu);
    
    Mat hist_mat(hist_gpu);
    hist_mat = hist_mat(Rect( 0, 0, round(mean[0]), 1));
    double pixels = sum(hist_mat)[0];
   
    std::vector<int> indx(round(mean[0]));
    std::iota(indx.begin(), indx.end(), 0);
    
    Mat indx_mat(indx);
    transpose(indx_mat, indx_mat);
    
//    std::cout << "indx_mat size" << indx_mat.size() << " indx_mat type " << indx_mat.type() << std::endl;
//    std::cout << "hist_mat size" << hist_mat.size() << " hist_mat type " << hist_mat.type() << std::endl;
//    std::cout << "occur_mat size" << occur_mat.size() << " occur_mat type " << occur_mat.type() << std::endl;
    
    Mat occur_mat;
    multiply(indx_mat, hist_mat, occur_mat);
    
    double occur_mat_sum = sum(occur_mat)[0];
    
    double T = round(occur_mat_sum/pixels);
    double lastT = -999.0;
    double m1, m2;
    
    Mat b;
    int i;
    
    for(i=0; i<30; i++){
        // Calcula histograma
        gpu::calcHist(roi_gpu, hist_gpu);
        hist_mat = Mat(hist_gpu);
                
        // esquerda
        // Metade T do histograma a esquerda        
        b = hist_mat(Rect( 0, 0, (int)T, 1));
        pixels = sum(b)[0];
        
        // Mat de Indices de 0 a T
        indx.resize( (int)T );
        std::iota(indx.begin(), indx.end(), 0);        
        indx_mat = Mat(indx);
        transpose(indx_mat, indx_mat);
                
        // Multiply p achar ocorrencias
        multiply(indx_mat, b, occur_mat);        
        m1 = sum( occur_mat )[0]/pixels;
        
        //direita
        // Metade T do histograma a direita
        b = hist_mat(Rect( (int)T, 0, 256 - (int)T, 1));
        pixels = sum(b)[0];
        
        // Mat de Indices de T a 256
        indx.resize( 256 - (int)T );
        std::iota(indx.begin(), indx.end(), (int)T);        
        indx_mat = Mat(indx);
        transpose(indx_mat, indx_mat);
                
        // Multiply p achar ocorrencias
        multiply(indx_mat, b, occur_mat);        
        m2 = sum( occur_mat )[0]/pixels;
        
        T = round( (m1 + m2)/2.0 );
        if((int)lastT == (int)T){
            break;
        }
        lastT = T;
    }
    
    return T;
}