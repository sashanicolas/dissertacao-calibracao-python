import numpy as np
import cv2
import pysomemodule
import time

img = cv2.imread('img.jpg', 0)

foo = pysomemodule.ABC("config.txt") # This will create an instance of ABC

s = time.time()
processedImage = foo.doSomething(img) # Where the argument "image" is a OpenCV numpy image.
print time.time() - s

s = time.time()
retval, cellbw = cv2.threshold(img, 128, 255, cv2.THRESH_BINARY)
print time.time() - s

cv2.imshow('Imagem', processedImage)
cv2.waitKey(0)



