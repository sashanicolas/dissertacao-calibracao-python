#ifndef ABC_HPP
#define ABC_HPP

#include <Python.h>
#include <string>
#include <vector>

#include "opencv2/opencv.hpp"
#include "opencv2/gpu/gpu.hpp"
#include "opencv2/core/core.hpp"

using namespace std;
using namespace cv;

class ABC
{
public:
    
    ABC();
    
    ABC(const std::string& someConfigFile);
    
    virtual ~ABC();
    
    /*
     * We want our python code to be able to call this function 
     * to do some processing using OpenCV and return the result.
     */
    PyObject* refineUsingPrakash(PyObject* imgGray, PyObject* attributes, PyObject* centers);
    
    double findAdaptativeThreshold(gpu::GpuMat &roi_gpu);
    
    void get_contornos_adaptative_threshold(
        gpu::GpuMat &imgGray_mat, vector<vector<Point> > & contours, vector<Vec4i> & hierarchy);
};

#endif
