import cv2
import numpy as np
import settings_flags
import logging

logging.basicConfig(level=logging.WARNING)

__author__ = 'Sasha Nicolas'


class Ring:
    max_distance_allowed_between_centers = 15

    def __init__(self, contours=None, center=None):
        self.ellipses = []
        self.center = [-1, -1]
        self.maxBoundingBoxSide = -1

        if center is not None:
            self.center = center

        if contours is not None:
            self.contours = contours
            self.calculateEllipses()
            if self.ellipsesCentersAreNear() and self.ellipsesAreaAreOk():
                self.calculateCenter()

    def calculateEllipses(self):
        for i in range(0, len(self.contours)):
            ellipse = cv2.fitEllipse(self.contours[i])
            self.ellipses.append(ellipse)

        for i in range(0, len(self.ellipses)):
            if self.ellipses[i][1][0] > self.maxBoundingBoxSide:
                self.maxBoundingBoxSide = self.ellipses[i][1][0]

    def calculateCenter(self):
        center = [0, 0]
        for i in range(0, len(self.ellipses)):
            center[0] += self.ellipses[i][0][0]
            center[1] += self.ellipses[i][0][1]

        self.center[0] = center[0] / len(self.ellipses)
        self.center[1] = center[1] / len(self.ellipses)

    def ellipsesCentersAreNear(self):
        if self.center[0] is not -1 and self.center[0] is not -1:
            return True

        # verificar distancias entre os centros das elipses
        # max_dist = 5
        c1 = np.array(self.ellipses[0][0])  # primeira elipse
        for i in range(1, len(self.ellipses)):
            c2 = np.array(self.ellipses[i][0])
            dist = np.linalg.norm(c2 - c1)
            if dist > Ring.max_distance_allowed_between_centers:
                print "  Ring::ellipsesCentersAreNear() => distance ", dist
                return False
        return True

    def ellipsesAreaAreOk(self):
        # TODO: como verificar se as areas estao ok?
        # se 4 elipses, duas tem area parecidas e duas tem areas maiores
        # alem disso pode-se ter um limite inf e sup para area
        return True
        # return self.ellipses[0][1][0] > 3

    @staticmethod
    def getRingsCenters(rings):
        """
        Retorna somente os centros dos rings passados por parametros, no formato do opencv
        :param rings:
        :return:
        """
        rings_centers = np.ndarray((len(rings), 1, 2), dtype=np.float32)
        for i in range(0, len(rings)):
            rings_centers[i] = rings[i].center
        return rings_centers

    @staticmethod
    def findRingCenter(contornos, hierarquia, w=0, h=0):
        # TODO: muito ruim isso aqui
        if len(contornos) is not 4:
            print "Contornos ", len(contornos)
            return False

        """
        for it in range(0, len(contornos)):
            if len(contornos[it]) > 5:
                four_contour = []
                temFilho = hierarquia[0][it][2] > -1
                temPai = hierarquia[0][it][3] >= 0
                if not temFilho and temPai:
                    four_contour.append(contornos[it])
                    pai = hierarquia[0][it][3]
                    temPai = hierarquia[0][pai][3] >= 0
                    if temPai:
                        four_contour.append(contornos[pai])
                        pai = hierarquia[0][pai][3]
                        temPai = hierarquia[0][pai][3] >= 0
                        if temPai:
                            four_contour.append(contornos[pai])
                            pai = hierarquia[0][pai][3]
                            temPai = hierarquia[0][pai][3] >= 0
                            if not temPai:
                                four_contour.append(contornos[pai])

                                ring = Ring(four_contour)

                                # check if all 4 contours make up a ellipse
                                if ring.ellipsesCentersAreNear():
                                    return ring
        """

        four_contour = []
        for it in range(0, len(contornos)):
            four_contour.append(contornos[it])

        ring = Ring(four_contour)

        # check if all 4 contours make up a ellipse
        if ring.ellipsesCentersAreNear():
            return ring
        else:
            print "distancia entre centro eh "

        img = np.zeros(shape=(w, h), dtype=np.uint8)
        RingsGrid.debugContoursEach(contornos, img)
        # RingsGrid.debugContoursAll(contornos, img)
        print "     ellipses centers are not near"
        return False

    @staticmethod
    def isRing4(hierarquia, indx):
        id = -1
        for i in range(0, len(hierarquia)):
            if len(hierarquia[i]) > 5:
                temFilho = hierarquia[0][i][2] > -1
                temPai = hierarquia[0][i][3] >= 0
                if not temFilho and temPai:
                    pai = hierarquia[0][i][3]
                    temPai = hierarquia[0][pai][3] >= 0
                    if temPai:
                        pai = hierarquia[0][pai][3]
                        temPai = hierarquia[0][pai][3] >= 0
                        if temPai:
                            pai = hierarquia[0][pai][3]
                            temPai = hierarquia[0][pai][3] >= 0
                            if not temPai:
                                id = i

        return id

    @staticmethod
    def isRing3(hierarquia, indx):
        temFilho = hierarquia[0][indx][2] > -1
        temPai = hierarquia[0][indx][3] >= 0
        if not temFilho and temPai:
            pai = hierarquia[0][indx][3]
            temPai = hierarquia[0][pai][3] >= 0
            if temPai:
                pai = hierarquia[0][pai][3]
                temPai = hierarquia[0][pai][3] >= 0
                if temPai:
                    pai = hierarquia[0][pai][3]
                    temPai = hierarquia[0][pai][3] >= 0
                    if not temPai:
                        return True
        return False

    @staticmethod
    def isRing(hierarquia, indx):
        temFilho = hierarquia[0][indx][2] > -1
        temPai = hierarquia[0][indx][3] >= 0
        if not temFilho and temPai:
            pai = hierarquia[0][indx][3]
            temPai = hierarquia[0][pai][3] >= 0
            if temPai:
                pai = hierarquia[0][pai][3]
                temPai = hierarquia[0][pai][3] >= 0
                if temPai:
                    pai = hierarquia[0][pai][3]
                    temPai = hierarquia[0][pai][3] >= 0
                    # if not temPai:
                    #     print '3'
                    #     return True, 3  # Ring3
                    # else:
                    #     pai = hierarquia[0][pai][3]
                    #     temPai = hierarquia[0][pai][3] >= 0

                    # if not temPai:  # Ring4
                    #     print '4'
                    return True, 4
                else:
                    print '3'
                    return True, 3  # Ring3

        return False, -1

    @staticmethod
    def get_ring_ellipses(contornos, hierarquia, it, size):
        contours = []
        iter_pai = it
        for i in range(0, size):
            contours.append(contornos[iter_pai])
            iter_pai = hierarquia[0][iter_pai][3]

        return contours

    @staticmethod
    def countRingsInImage(contornos, hierarquia, w=0, h=0):
        # TODO: nao eh eficiente, tem caso que acha 4 mas nao eh anel
        countRings = 0
        ringInternos = []
        rings = []

        for it in range(0, len(contornos)):
            if len(contornos[it]) > 5:
                is_ring, size = Ring.isRing(hierarquia, it)
                if is_ring:
                    contours = Ring.get_ring_ellipses(contornos, hierarquia, it, size)
                    ring = Ring(contours)

                    if ring.ellipsesCentersAreNear() and ring.ellipsesAreaAreOk():
                        countRings += 1
                        ringInternos.append(it)
                        rings.append(ring)
                    else:
                        print "not if ring.ellipsesCentersAreNear() and ring.ellipsesAreaAreOk():"

        # for it in range(0, len(contornos)):
        #     if len(contornos[it]) > 5:
        #         four_contour = []
        #         temFilho = hierarquia[0][it][2] > -1
        #         temPai = hierarquia[0][it][3] >= 0
        #         if not temFilho and temPai:
        #             four_contour.append(contornos[it])
        #             pai = hierarquia[0][it][3]
        #             temPai = hierarquia[0][pai][3] >= 0
        #             if temPai:
        #                 four_contour.append(contornos[pai])
        #                 pai = hierarquia[0][pai][3]
        #                 temPai = hierarquia[0][pai][3] >= 0
        #                 if temPai:
        #                     four_contour.append(contornos[pai])
        #                     pai = hierarquia[0][pai][3]
        #                     temPai = hierarquia[0][pai][3] >= 0
        #                     if not temPai:
        #                         four_contour.append(contornos[pai])
        #
        #                         ring = Ring(four_contour)
        #                         # img=np.zeros(shape=(w,h), dtype=np.uint8)
        #                         # RingsGrid.debugContoursAll(four_contour, img)
        #
        #                         # check if all 4 contours make up a ellipse
        #                         if ring.ellipsesCentersAreNear() and ring.ellipsesAreaAreOk():
        #                             countRings += 1
        #                             ringInternos.append(it)
        #                             rings.append(ring)

        return countRings, ringInternos, rings

    @staticmethod
    def getMinorRingCenter(rings_centers):
        distance_minor = cv2.norm(np.array(rings_centers[0], np.float64), np.array([0, 0], np.float64), cv2.NORM_L2)
        minor_center_id = 0
        for i in range(1, len(rings_centers)):
            d = cv2.norm(np.array(rings_centers[i], np.float64), np.array([0, 0], np.float64), cv2.NORM_L2)
            if d < distance_minor:
                distance_minor = d
                minor_center_id = i

        return minor_center_id


class RingsGrid:
    kernel_size = 5
    canny_min = 127
    canny_max = 255

    def __init__(self, dimension, rings):
        self.dimension = dimension
        self.rings = rings
        self.ringsGrid = [[Ring() for x in range(dimension[0])] for x in range(dimension[1])]

    def ringsAndDimentionIsOk(self):
        amountOfRings = self.dimension[0] * self.dimension[1]
        return amountOfRings == len(self.rings)

    def getGridCenters(self):
        rings_centers = np.ndarray((len(self.rings), 1, 2), dtype=np.float32)
        x = y = 0
        for i in range(0, len(self.rings)):
            rings_centers[i] = self.ringsGrid[x][y].center

            y += 1
            if y >= self.dimension[0]:
                x += 1
                y = 0

        return rings_centers

    def addRingsToRow(self, row, points):
        x = row
        y = 0
        for i in range(0, len(points)):
            self.ringsGrid[x][y] = Ring(center=points[i][0])
            y += 1

    @staticmethod
    def debugContoursEach(contornos, img, thick=1):
        if settings_flags.DEBUG_CONTORNOS_EACH:
            # debug cada cell e os contornos
            for it in range(0, len(contornos)):
                img_debug = img.copy()
                # el = cv2.fitEllipse(contornos[it])
                # DrawingUtils.desenhaCruz(el[0][0], el[0][1], 255, img_debug)
                cv2.drawContours(img_debug, contornos, it, (255, 255, 0), thickness=thick)
                DrawingUtils.mostra_imagem('contornos each', img_debug, 800, 150)

    @staticmethod
    def debugRingsFound(rings, img):
        """
        @type rings: list[Ring]
        """
        # debug cada cell e os contornos
        img_debug = img.copy()
        for it in range(0, len(rings)):
            for i in range(0, len(rings[it].contours)):
                cv2.drawContours(img_debug, rings[it].contours, i, (0, 255, 0))
            DrawingUtils.desenhaCruz(rings[it].center[0], rings[it].center[1], (255, 0, 0), img_debug, 3)
        DrawingUtils.mostra_imagem('rings achados', img_debug, 800, 150)

    @staticmethod
    def debugContoursAll(contornos, img):
        if settings_flags.DEBUG_CONTORNOS_ALL:
            # debug os contornos
            img_debug = img.copy()
            for it in range(0, len(contornos)):
                cv2.drawContours(img_debug, contornos, it, (255, 255, 0), 1)
            DrawingUtils.mostra_imagem('contornos all', img_debug, 800, 150, wait_key_zero=True)

    @staticmethod
    def findFourCornersID(points):
        '''
        O array points eh numpy, tem shape [n,1,m]
        :param points:
        :return:
        '''
        corners_id = []
        for i in range(0, len(points)):
            if 0 < i < len(points) - 1:
                a_vec = points[i] - points[i - 1]
                b_vec = points[i + 1] - points[i]
            elif i == 0:
                a_vec = points[0] - points[len(points) - 1]
                b_vec = points[1] - points[0]
            else:  # ultimo
                a_vec = points[len(points) - 1] - points[len(points) - 2]
                b_vec = points[0] - points[len(points) - 1]

            if GeometryUtils.angleBetween(a_vec, b_vec) > 0.78:  # 45 graus em radianos
                corners_id.append(i)

        return corners_id

    @staticmethod
    def organizar_centros_no_grid(ringsGrid, pattern):
        rings_centers = Ring.getRingsCenters(ringsGrid.rings)
        removed_centers = np.zeros(len(rings_centers), np.bool)

        for i in range(0, pattern[1] / 2):
            hull = cv2.convexHull(rings_centers)
            corners_id = RingsGrid.findFourCornersID(hull)

            # pega os 4 cantos somente, que sao um subset do hull
            corners = OpencvNumpyUtils.getSubset(hull, corners_id)

            removed_centers_bkp = removed_centers.copy()
            pontos_reta_baixo, removed_centers, nearestIds_baixo = GeometryUtils.getNearestPointsOfLine(
                rings_centers, corners, removed_centers, 0, pattern[0])

            if len(pontos_reta_baixo) != pattern[0]:
                removed_centers = removed_centers_bkp.copy()
                pontos_reta_baixo, removed_centers, nearestIds_baixo = GeometryUtils.getNearestPointsOfLine(
                    rings_centers, corners, removed_centers, 1, pattern[0])
                pontos_reta_cima, removed_centers, nearestIds_cima = GeometryUtils.getNearestPointsOfLine(
                    rings_centers, corners, removed_centers, 3, pattern[0])
            else:
                pontos_reta_cima, removed_centers, nearestIds_cima = GeometryUtils.getNearestPointsOfLine(
                    rings_centers, corners, removed_centers, 2, pattern[0])

            if len(pontos_reta_baixo) != pattern[0] or len(pontos_reta_cima) != pattern[0]:
                print '  --> RingsGrid::findRingsGrid Nao Achou Grid'
                return False, None

            # se achou corretamentem adiciona na matriz que representa o grid
            ringsGrid.addRingsToRow(i, pontos_reta_cima)
            ringsGrid.addRingsToRow(pattern[1] - 1 - i, pontos_reta_baixo)

            # remover de rings_centers os pontos que ja foram adicionados ao ringGrid
            remove_ids = np.concatenate((nearestIds_baixo, nearestIds_cima))
            rings_centers = OpencvNumpyUtils.removeIds(rings_centers, remove_ids)
            removed_centers = OpencvNumpyUtils.removeIds(removed_centers, remove_ids)

        # se pattern eh impar achar o do meio
        if pattern[1] % 2 != 0:
            rings_centers = GeometryUtils.ordenarPontosEmRetaPorEixo(rings_centers)
            ringsGrid.addRingsToRow(pattern[1] / 2, rings_centers)

        return True, ringsGrid

    @staticmethod
    def getCornersROI(ring, roiSide):
        x1 = max(int(ring.center[0] - roiSide / 2.0), 0)
        y1 = max(int(ring.center[1] - roiSide / 2.0), 0)

        x2 = int(ring.center[0] + roiSide / 2.0)
        y2 = int(ring.center[1] + roiSide / 2.0)

        return x1, y1, x2, y2

    @staticmethod
    def get_contornos_gaussian_blur_canny(roi):
        # blur e canny
        blur = cv2.GaussianBlur(roi, (RingsGrid.kernel_size, RingsGrid.kernel_size), 0)
        edges = cv2.Canny(blur, RingsGrid.canny_min, RingsGrid.canny_max)

        contornos, hierarquia = cv2.findContours(edges, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

        return contornos, hierarquia

    @staticmethod
    def get_contornos_threshold_weighted(roi_4):
        # threshold
        min, max, min_loc, max_loc = cv2.minMaxLoc(roi_4)

        cweight = 0.5
        thresh = min * cweight + max * (1 - cweight)
        retval, cellbw = cv2.threshold(roi_4, thresh, 255, cv2.THRESH_BINARY)
        edges2 = cv2.Canny(cellbw, RingsGrid.canny_min, RingsGrid.canny_max)

        # Encontra os contornos na roi_4
        contornos2, hierarquia2 = cv2.findContours(edges2, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

        return contornos2, hierarquia2

    @staticmethod
    def get_contornos_adaptative_threshold(roi_4):
        roi_4 = cv2.GaussianBlur(roi_4, (RingsGrid.kernel_size, RingsGrid.kernel_size), 0)

        T2 = RingsGrid.findAdaptativeThreshold(roi_4)

        # Achar centro na roi_4
        thresh2, img_bin2 = cv2.threshold(roi_4, T2, 255, cv2.THRESH_BINARY)

        edges2 = cv2.Canny(img_bin2, RingsGrid.canny_min, RingsGrid.canny_max)

        # Encontra os contornos na roi_4
        contornos2, hierarquia2 = cv2.findContours(edges2.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

        return contornos2, hierarquia2

    @staticmethod
    def refineUsingPrakash(ringsGrid, roiSide, imgGray):
        if roiSide is not None:
            factor = 4.0
            for i in range(0, ringsGrid.dimension[1]):
                for j in range(0, ringsGrid.dimension[0]):
                    x1, y1, x2, y2 = RingsGrid.getCornersROI(ringsGrid.ringsGrid[i][j], roiSide)
                    roi = imgGray[y1:y2, x1:x2]

                    # Supersample the roi
                    height, width = roi.shape[:2]

                    roi_4 = cv2.resize(roi, (int(factor * width), int(factor * height)), interpolation=cv2.INTER_CUBIC)

                    # DrawingUtils.mostra_single_center(roi_4, (ringsGrid.ringsGrid[i][j].center[0] - x1)*factor,
                    #                                         (ringsGrid.ringsGrid[i][j].center[1] - y1)*factor)

                    # Encontra os contornos na roi_4
                    contornos2, hierarquia2 = RingsGrid.get_contornos_adaptative_threshold(roi_4)

                    # RingsGrid.debugContoursAll(contornos2, np.zeros(roi_4.shape))

                    # Atualiza centro
                    ring2 = Ring.findRingCenter(contornos2, hierarquia2, roi_4.shape[1], roi_4.shape[0])

                    # Usa-se o ring2 que veio da roi4 supersampled para melhorar o centro
                    if ring2:
                        ringsGrid.ringsGrid[i][j].center[0] = ring2.center[0] / factor + x1
                        ringsGrid.ringsGrid[i][j].center[1] = ring2.center[1] / factor + y1
                    else:
                        print '  RingsGrid::refineUsingPrakash -> nao achou em ', i, ' ', j
                        # DrawingUtils.mostra_imagem('roi_4', roi_4)
                        # RingsGrid.debugContoursEach(contornos2, np.zeros(roi_4.shape))
                        continue

        return ringsGrid

    @staticmethod
    def findRingsGrid(image, pattern, roiSide=None, refine=True):
        """
        preencher o self.ringsGrid (uma matriz com os rings nas posicoes corretas de grid)
        :param image:
        :param pattern:
        :return:
        """
        if pattern[0] <= pattern[1]:
            print 'pattern[0] <= pattern[1]'
            return False, None

        # Converte para niveis de cinza
        imgGray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

        # Otsu's thresholding after Gaussian filtering
        blur = cv2.GaussianBlur(imgGray, (RingsGrid.kernel_size, RingsGrid.kernel_size), 0)
        thresh, img_bin = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)

        # Achar bordas
        edges = cv2.Canny(img_bin, RingsGrid.canny_min, RingsGrid.canny_max)

        # Encontra os contornos e conta os rings
        contornos, hierarquia = cv2.findContours(edges.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
        ringsCount, ringsIds, rings = Ring.countRingsInImage(contornos, hierarquia, imgGray.shape[1], imgGray.shape[0])

        # Instancia grid
        ringsGrid = RingsGrid(pattern, rings)

        if not ringsGrid.ringsAndDimentionIsOk():
            print '  RingsGrid::findRingsGrid -> dimension is not ok -> len(rings)=', len(rings)
            # RingsGrid.debugRingsFound(ringsGrid.rings, np.zeros(shape=imgGray.shape, dtype=imgGray.dtype))

            RingsGrid.debugContoursEach(contornos, np.zeros(shape=imgGray.shape, dtype=imgGray.dtype), 2)
            DrawingUtils.mostra_imagem('img equ', edges)
            return False, None

        # Organizar centros no grid
        # de fora pra dentro achar os 'pattern[0]' centros em ordem correta nas linhas
        ok_organizar, ringsGrid = RingsGrid.organizar_centros_no_grid(ringsGrid, pattern)
        if not ok_organizar:
            return ok_organizar, None

        # Refinar usando Prakash
        if settings_flags.USE_PRAKASH_THRESHOLD_ADAP and refine:
            print '.',
            ringsGrid = RingsGrid.refineUsingPrakash(ringsGrid, roiSide, imgGray)

        return True, ringsGrid.getGridCenters()

    @staticmethod
    def findAdaptativeThreshold(roi):
        # threshold adaptativo pelo Prakash
        # valor medio de pixel da imagem -> round
        mi = np.rint(cv2.mean(roi)[0])
        # histograma -> primeiros mi valores -> reshape array pq opencv retorna [[],[]..]
        hist = cv2.calcHist([roi], [0], None, [256], [0, 256])[:mi].reshape(mi)
        # quantidade de pixels de cada cor ate mi
        pixels = np.sum(hist)
        # array com indices(cor) ate mi para multiplicar com as ocorrencias
        indx = np.arange(mi)
        # multiplica a cor(indice) pela quantidade de pixels
        # eh isso mesmo, eh dificil de entender, se todos forem 0, a media tem que ser
        # a (soma de todos os zeros)/todos que deve ser igual a zero
        occur = hist * indx
        # soma os pixels e divide pela quantidade de pixels
        T = np.rint(np.sum(occur) / pixels)

        # # Otsu's thresholding after Gaussian filtering
        # blur = cv2.GaussianBlur(roi, (RingsGrid.kernel_size, RingsGrid.kernel_size), 0)
        # thresh, img_bin = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
        # # Achar bordas
        # edges = cv2.Canny(img_bin, RingsGrid.canny_min, RingsGrid.canny_max)
        # # Encontra os contornos e conta os rings
        # contornos, hierarquia = cv2.findContours(edges.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
        # ringsCount, ringsIds, rings = Ring.countRingsInImage(contornos, hierarquia, roi.shape[1], roi.shape[0])
        # # print "centro inicial: ", rings[0].center
        # roi3 = roi.copy()
        # DrawingUtils.desenhaCruz(rings[0].center[0], rings[0].center[1], 0, roi3)
        # DrawingUtils.mostra_imagem("roi", roi3, wait_key_not_zero=True)

        # imagens = []
        # cv2.imwrite("ta_ori.png", roi)

        indx = np.arange(256)
        lastT = -9999
        hist = cv2.calcHist([roi], [0], None, [256], [0, 256])
        for step in range(100, 0, -1):
            # print "T=", T
            # thresh2, img_bin2 = cv2.threshold(roi.copy(), T, 255, cv2.THRESH_BINARY)
            # edges2 = cv2.Canny(img_bin2, RingsGrid.canny_min, RingsGrid.canny_max)
            # # Encontra os contornos na roi_4
            # contornos2, hierarquia2 = cv2.findContours(edges2.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
            # # Atualiza centro
            # ring2 = Ring.findRingCenter(contornos2, hierarquia2, roi.shape[1], roi.shape[0])
            # if ring2:
            #     print "centro i=", 100-step, ":", ring2.center
            # else:
            #     print "centro i =", 100-step, " nao achou"
            # DrawingUtils.mostra_imagem("roi", img_bin2)
            # imagens.append(img_bin2)
            # # cv2.imwrite("ta_bin_"+str(100-step)+".png", img_bin2)

            m1 = np.sum(hist[:T].reshape(T) * indx[:T]) / np.sum(hist[:T])
            m2 = np.sum(hist[T:].reshape(256 - T) * indx[T:]) / np.sum(hist[T:])
            T = int(np.rint((m1 + m2) / 2.0))

            if lastT == T:
                break
            lastT = T

        # roi_size = imagens[0].shape[0] + 10
        # for i in xrange(len(imagens)):
        #     DrawingUtils.mostra_imagem("roi"+str(i+1), imagens[i], x=150+roi_size*(i+1), wait_key_not_zero=True)
        # cv2.waitKey(0)
        # cv2.destroyAllWindows()

        # colors = [[0, 0, 255], [0, 255, 0], [255, 0, 0], [0, 255, 255], [255, 0, 255]]
        # h, w = roi.shape[:2]
        # comp = np.ones(shape=(h, w, 3), dtype=roi.dtype)
        # comp.fill(255)
        # for i in xrange(len(imagens) - 1, -1, -1):
        #     # DrawingUtils.mostra_imagem("roi"+str(i+1), imagens[i])
        #     for col in range(w):
        #         for lin in range(h):
        #             if imagens[i][lin][col] == 0:
        #                 comp[lin][col] = colors[- i - 1 + len(colors)]  # (len(imagens)-i-1)*50
        #
        # DrawingUtils.mostra_imagem("comp", comp)
        # factor = 8
        # comp_8 = cv2.resize(comp, (int(factor * w), int(factor * h)), interpolation=cv2.INTER_NEAREST)
        # DrawingUtils.mostra_imagem("comp8", comp_8)
        # cv2.imwrite("ta_comp.png", comp_8)
        # cv2.destroyAllWindows()

        return T

    @staticmethod
    def binariza_adaptiveThreshold(input_img):
        IMAGE_WIDTH = input_img.shape[0]
        IMAGE_HEIGHT = input_img.shape[1]
        S = (IMAGE_WIDTH / 8)
        T = 0.15

        s2 = S / 2

        # DrawingUtils.mostra_imagem("gray", input_img)
        # DrawingUtils.mostra_imagem("integral", cv2.integral(input_img))

        # integralImg = np.ndarray(shape=(IMAGE_WIDTH * IMAGE_HEIGHT), dtype=np.int64)
        # input_img = np.reshape(input_img, -1)
        # for i in range(0, IMAGE_WIDTH):
        #     # reset this column sum
        #     sum = 0
        #     for j in range(0, IMAGE_HEIGHT):
        #         index = j * IMAGE_WIDTH + i
        #
        #         sum += input_img[index]
        #         if i == 0:
        #             integralImg[index] = sum
        #         else:
        #             integralImg[index] = integralImg[index - 1] + sum
        #
        # DrawingUtils.mostra_imagem("bin", integralImg.reshape(400, 400))

        integralImg = cv2.integral(input_img)
        # DrawingUtils.mostra_imagem("opencv integral", integralImg)
        # integralImg = np.reshape(integralImg, -1)

        # bin = np.ndarray(shape=(IMAGE_WIDTH * IMAGE_HEIGHT))
        bin = np.zeros(shape=input_img.shape, dtype=np.uint8)

        # perform thresholding
        for i in range(0, IMAGE_WIDTH):
            for j in range(0, IMAGE_HEIGHT):
                # index = j * IMAGE_WIDTH + i

                # set the SxS region
                x1 = i - s2
                x2 = i + s2
                y1 = j - s2
                y2 = j + s2

                # check the border
                if x1 < 0:
                    x1 = 0
                if x2 >= IMAGE_WIDTH:
                    x2 = IMAGE_WIDTH - 1
                if y1 < 0:
                    y1 = 0
                if y2 >= IMAGE_HEIGHT:
                    y2 = IMAGE_HEIGHT - 1

                count = (x2 - x1) * (y2 - y1)

                # I(x,y)=s(x2,y2)-s(x1,y2)-s(x2,y1)+s(x1,x1)
                # sum = integralImg[y2 * IMAGE_WIDTH + x2] - \
                #       integralImg[y1 * IMAGE_WIDTH + x2] - \
                #       integralImg[y2 * IMAGE_WIDTH + x1] + \
                #       integralImg[y1 * IMAGE_WIDTH + x1]

                sum = integralImg[x2][y2] - \
                      integralImg[x2][y1] - \
                      integralImg[x1][y2] + \
                      integralImg[x1][y1]

                if (input_img[i][j] * count) < (sum * (1.0 - T)):
                    bin[i][j] = 0
                else:
                    bin[i][j] = 255

        return bin


class DrawingUtils:
    def __init__(self):
        pass

    @staticmethod
    def mostra_imagem(name, image, x=150, y=150, wait_key_not_zero=False, wait_key_zero=False, image_id=None):
        if not settings_flags.FUNCAO_MOSTRA_IMAGEM_ENABLED:
            return
        cv2.namedWindow(name)

        # redimensiona se grande TODO: fazer antes das operacoes de inserir centros
        h, w = image.shape[:2]
        if h > 800 or w > 800:
            fator_scale = 800.0 / w
            image = cv2.resize(image, (800, int(h * fator_scale)))

        if image_id is not None:
            cv2.putText(image, "Image " + str(image_id), (20, 25), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255))

        cv2.moveWindow(name, x, y)
        cv2.imshow(name, image)

        if wait_key_zero:
            cv2.waitKey(0)
            return

        if not wait_key_not_zero:
            cv2.waitKey(0)
            cv2.destroyWindow(name)

    @staticmethod
    def mostra_single_center(roi, center, window_name="roi"):
        roi_cp = roi.copy()
        DrawingUtils.desenhaCruz(center[0], center[1], (255, 0, 0), roi_cp, 1)
        DrawingUtils.mostra_imagem(window_name, roi_cp)

    @staticmethod
    def desenhaCruz(x, y, color, img, thick=1):
        x = int(x)
        y = int(y)

        cv2.line(img, (x - 5, y), (x + 5, y), color, thick, 8)
        cv2.line(img, (x, y - 5), (x, y + 5), color, thick, 8)

    @staticmethod
    def desenhaCentros(img, centros, color=(255, 255, 0), thickness=None):
        for i in range(0, len(centros)):
            if thickness:
                DrawingUtils.desenhaCruz(centros[i][0], centros[i][1], color, img, thickness)
            else:
                DrawingUtils.desenhaCruz(centros[i][0], centros[i][1], color, img)

            if settings_flags.ADICIONA_NUMERO_CENTRO:
                cv2.putText(img, str(i), (int(centros[i][0]), int(centros[i][1])), cv2.FONT_HERSHEY_SIMPLEX, 0.65,
                            (0, 255, 255))


class OpencvNumpyUtils:
    def __init__(self):
        pass

    @staticmethod
    def cvtArrayTuple2_2CVArr(array):
        """
        Converter array simples de tuplas duplas para opencv numpy style
        :param array:
        :return:
        """
        array_out = np.ndarray((len(array), 1, 2), dtype=np.float32)

        for i in range(0, len(array)):
            array_out[i][0][0] = array[i][0]
            array_out[i][0][1] = array[i][1]

        return array_out

    @staticmethod
    def cvtCVArr_2ArrayTuple2_2Sort(array):
        """
        Converter array simples de tuplas duplas para opencv numpy style
        :param array:
        :return:
        """
        array_out = []

        for i in range(0, len(array)):
            array_out.append((array[i][0][0], array[i][0][1]))

        return array_out

    @staticmethod
    def convertOpenCVArrayToSingleArray(array):
        """
        Converter array opencv style para normal
        Parece que basta usar meu_array.reshape(-1, 2)
        :param array:
        :return:
        """
        array_out = np.ndarray(shape=(array.shape[0], array.shape[2]), dtype=np.float32)

        for i in range(0, len(array)):
            array_out[i] = array[i][0]

        return array_out

    @staticmethod
    def convertSingleArrayToOpenCVArray(array):
        array_out = np.ndarray(shape=(array.shape[0], array.shape[2]), dtype=np.float32)

        for i in range(0, len(array)):
            array_out[i] = array[i][0]

        return array_out

    @staticmethod
    def getSubset(array, corners_id):
        '''
        O array vem do opencv, portanto eh numpy e tem shape [n,1,m]
        :param array:
        :param corners_id:
        :return:
        '''
        shape = list(array.shape)
        shape[0] = len(corners_id)
        subset = np.ndarray(tuple(shape))

        for i in range(len(corners_id)):
            subset[i] = array[corners_id[i]]

        return subset

    @staticmethod
    def removeIds(array, ids):
        '''
        O array vem do opencv, portanto eh numpy e tem shape [n,1,m]
        :param array:
        :param corners_id:
        :return:
        '''
        shape = list(array.shape)
        shape[0] = len(array) - len(ids)
        subset = np.ndarray(tuple(shape), dtype=array.dtype)

        k = 0
        for i in range(len(array)):
            if i not in ids:
                subset[k] = array[i]
                k += 1

        return subset


class GeometryUtils:
    def __init__(self):
        pass

    @staticmethod
    def angleBetween(a_vec, b_vec):
        """ Returns the angle in radians between vectors 'v1' and 'v2'    """
        a_vec = [a_vec[0][0], a_vec[0][1]]
        b_vec = [b_vec[0][0], b_vec[0][1]]
        cosang = np.dot(a_vec, b_vec)
        sinang = np.linalg.norm(np.cross(a_vec, b_vec))
        return np.arctan2(sinang, cosang)

    @staticmethod
    def distanceToLine(point, start, end):
        """
        Usage:
            end = np.array([0, 10])
            start = np.array([0, 0])
            point = np.array([10.8654, -10])
            print Geometry.distanceToLine(point, start, end)
        :param point:
        :param start:
        :param end:
        :return:
        """
        line_vec = end - start
        line_len = np.linalg.norm(line_vec)
        line_unit = line_vec / line_len

        point_vec = point - start
        point_proj = start + line_unit * np.dot(line_unit, point_vec)

        dist = np.linalg.norm(point - point_proj)

        return dist

    @staticmethod
    def getNearestPointsOfLine(rings_centers, corners, removed_centers, id_line, max_points):
        # TODO: max_dist_out_line como definir o parametro?
        id_distances = []
        max_dist_out_line = 30

        next_id = 0 if id_line == 3 else id_line + 1

        # calcula a distancia para todos que ainda nao foram removidos
        for i in range(0, len(rings_centers)):
            if not removed_centers[i]:
                id_distances.append((i, GeometryUtils.distanceToLine(rings_centers[i][0],
                                                                     corners[id_line][0],
                                                                     corners[next_id][0])))

        # ordenar pela distancia
        dtype = [('id', int), ('distance', float)]
        id_distances = np.array(id_distances, dtype=dtype)
        id_distances = np.sort(id_distances, order='distance')

        # achar quando a distancia eh alta -> 100 vezes maior? bias de 10(max_dist_out_line)
        nearestIds = []
        for i in range(0, max_points):
            if id_distances[i][1] > max_dist_out_line:
                break
            nearestIds.append(id_distances[i][0])
            removed_centers[id_distances[i][0]] = True

        # pegar somente os proximos
        ring_centers_nearest = rings_centers[nearestIds]

        # ordenar os pontos ou por x ou por y
        ring_centers_nearest = GeometryUtils.ordenarPontosEmRetaPorEixo(ring_centers_nearest)

        return ring_centers_nearest, removed_centers, nearestIds

    @staticmethod
    def getEixoMaiorVariacao(ring_centers_nearest):
        maior_x = maior_y = -1
        menor_x = menor_y = 999999

        for i in range(0, len(ring_centers_nearest)):
            if ring_centers_nearest[i][0][0] > maior_x:
                maior_x = ring_centers_nearest[i][0][0]
            if ring_centers_nearest[i][0][1] > maior_y:
                menor_x = ring_centers_nearest[i][0][1]

            if ring_centers_nearest[i][0][0] < menor_x:
                menor_x = ring_centers_nearest[i][0][0]
            if ring_centers_nearest[i][0][1] < menor_y:
                menor_y = ring_centers_nearest[i][0][1]

        var_x = maior_x - menor_x
        var_y = maior_y - menor_y

        if var_x > var_y:
            return 'x'
        else:
            return 'y'

    @staticmethod
    def ordenarPontosEmRetaPorEixo(points):
        # aqui tem varias coisas para adaptar os arrays para as funcoes
        dtype = [('x', np.float32), ('y', np.float32)]
        eixo = GeometryUtils.getEixoMaiorVariacao(points)

        points_out = OpencvNumpyUtils.cvtCVArr_2ArrayTuple2_2Sort(points)
        points_out = np.array(points_out, dtype=dtype)
        points_out = np.sort(points_out, order=eixo)
        points_out = OpencvNumpyUtils.cvtArrayTuple2_2CVArr(points_out)

        return points_out

####################################################
#                                                  #
#                      Main                        #
#                                                  #
####################################################

# if __name__ == "__main__":
#     # # im = cv2.imread('patterns/ring/video1_plus\\img640.jpg', 1)
#     # # im = cv2.imread('patterns/ring/kinect16\KinectSnapshot-05-06-44.png', 1)
#     # im = cv2.imread('patterns/ring/video1_plus\img800.jpg', 1)
#     # # DrawingUtils.mostra_imagem('ori', im)
#     # # im = cv2.imread('patterns/ring/video1_plus\\img40.jpg', 1)
#     # pat = (10, 7)
#     # found, centers = RingsGrid.findRingsGrid(im, pat)
#     # if found:
#     #     cv2.drawChessboardCorners(im, pat, centers, found)
#     #     cv2.imshow('frame', im)
#     #     cv2.moveWindow('frame', 20, 200)
#     #     cv2.waitKey(0)
#     #     cv2.destroyAllWindows()
#     # else:
#     #     print 'not found'
#
#     # region
#     # cap = cv2.VideoCapture('C:/Users/sasha/PycharmProjects/videos/Video001_mpeg.avi')
#     cap = cv2.VideoCapture('C:/Users/sasha/PycharmProjects/videos/Video002_mpeg.avi')
#     length = int(cap.get(cv2.cv.CV_CAP_PROP_FRAME_COUNT))
#     width = int(cap.get(cv2.cv.CV_CAP_PROP_FRAME_WIDTH))
#     height = int(cap.get(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT))
#     fps = cap.get(cv2.cv.CV_CAP_PROP_FPS)
#     print length, width, height, fps
#
#     count_frames = 0
#
#     # region criar grupos de frames randomicos
#     # rnd_frames_10 = random.sample(range(0, length), 10)
#     # rnd_frames_20 = random.sample(range(0, length), 20)
#     # endregion
#
#     while cap.isOpened():
#         frame_read, frame = cap.read()
#
#         if not frame_read:
#             break
#
#         count_frames = count_frames + 1
#
#         pattern = (7, 5)
#         # frame_ring, found, corners = FindRingPattern(frame, pattern)
#         # found, centers = FindRingPattern(frame, pattern)
#         found, centers = RingsGrid.findRingsGrid(frame, pattern)
#
#         if found:
#             # if count_frames % 40 == 0:
#             #     cv2.imwrite('patterns/ring/video1_plus/img'+str(count_frames)+'.jpg', frame)
#             cv2.drawChessboardCorners(frame, pattern, centers, found)
#
#         cv2.imshow('frame', frame)
#         cv2.moveWindow('frame', 20, 200)
#
#         # if not found:
#         #     k = cv2.waitKey(0)
#
#         k = cv2.waitKey(1)
#
#         if k & 0xFF == ord('q'):
#             break
#         if k & 0xFF == ord('p'):
#             k = cv2.waitKey(0)
#
#     # print count_frames
#
#     cap.release()
#     cv2.destroyAllWindows()
#     # endregion
