__author__ = 'sasha'
import numpy as np


def load_data_manuel():
    img_points, obj_points = [], []

    img_points.append(load_points("manuel_data/data1.txt"))
    img_points.append(load_points("manuel_data/data2.txt"))
    img_points.append(load_points("manuel_data/data3.txt"))
    img_points.append(load_points("manuel_data/data4.txt"))
    img_points.append(load_points("manuel_data/data5.txt"))

    for i in range(0, 5):
        obj_points.append(load_points("manuel_data/model.txt", True))

    return img_points, obj_points, (16, 16)


def load_points(file, add_zero=False):
    with open(file) as f:
        dimension = 3 if add_zero else 2
        points = np.ndarray(shape=(64 * 4, dimension), dtype=np.float32)
        ind = 0
        for line in f:  # Line is a string
            # split the string on whitespace, return a list of numbers
            # (as strings)
            numbers_str = line.split()
            # convert numbers to floats
            numbers_float = [float(x) for x in numbers_str]  # map(float,numbers_str) works too
            # print numbers_float
            for i in range(0, len(numbers_float), 2):
                points[ind][0] = numbers_float[i]
                points[ind][1] = numbers_float[i + 1]
                if add_zero:
                    points[ind][2] = 0
                ind += 1

        return points
