import numpy as np
import cv2
import lmfit
import calibration_util
import settings_flags


########################################################
#
#    Novos metodos de otimizacao, com LMFIT
#
########################################################


def get_params(cam_matrix, dist_coef, rvecs=None, tvecs=None):
    if rvecs is not None and tvecs is not None:
        c = [0] * (10 + len(rvecs) * 6)
    else:
        c = [0] * 10

    c[0] = cam_matrix[0][0]
    c[1] = cam_matrix[0][1]  # nao se preocupar pq nao ta sendo usado p criar parametro
    c[2] = cam_matrix[1][1]
    c[3] = cam_matrix[0][2]
    c[4] = cam_matrix[1][2]

    c[5:10] = dist_coef[0, :]

    # v = 0.015625
    # region params to lmfit
    params = lmfit.Parameters()

    VARY_PRIN = True  # not settings_flags.OTM_FIX_PRIN_OPENCV
    VARY_INT = not settings_flags.OTM_FIX_INT
    params.add('alfa', value=c[0], min=c[0] - 20.0, max=c[0] + 20.0, vary=VARY_INT)
    params.add('beta', value=c[2], min=c[2] - 20.0, max=c[2] + 20.0, vary=VARY_INT)
    params.add('x0', value=c[3], min=c[3] - 20.0, max=c[3] + 20.0, vary=VARY_PRIN)
    params.add('y0', value=c[4], min=c[4] - 20.0, max=c[4] + 20.0, vary=VARY_PRIN)

    VARY_DIST = not settings_flags.OTM_FIX_DIST
    for i in range(0, 5):
        params.add('d' + str(i), value=c[5 + i], vary=VARY_DIST)

    VARY_EXT = not settings_flags.OTM_FIX_EXT
    if rvecs is not None and tvecs is not None:
        for i in range(0, len(rvecs)):
            params.add('rtw' + str(i) + 'w0', rvecs[i][0], vary=VARY_EXT)
            params.add('rtw' + str(i) + 'w1', rvecs[i][1], vary=VARY_EXT)
            params.add('rtw' + str(i) + 'w2', rvecs[i][2], vary=VARY_EXT)

            params.add('rtw' + str(i) + 'w3', tvecs[i][0], vary=VARY_EXT)
            params.add('rtw' + str(i) + 'w4', tvecs[i][1], vary=VARY_EXT)
            params.add('rtw' + str(i) + 'w5', tvecs[i][2], vary=VARY_EXT)

    return params


def extract_params(lmfit_result_params, extract_rt=False, num_images=0):
    cam_matrix = np.zeros(shape=(3, 3), dtype=np.float64)

    c = lmfit_result_params
    g = [c['alfa'].value, 0, c['beta'].value, c['x0'].value, c['y0'].value,
         c['d0'].value, c['d1'].value, c['d2'].value, c['d3'].value, c['d4'].value]

    if extract_rt:
        for i in range(0, num_images):
            g.append(c['rtw' + str(i) + 'w0'].value)
            g.append(c['rtw' + str(i) + 'w1'].value)
            g.append(c['rtw' + str(i) + 'w2'].value)

            g.append(c['rtw' + str(i) + 'w3'].value)
            g.append(c['rtw' + str(i) + 'w4'].value)
            g.append(c['rtw' + str(i) + 'w5'].value)

    cam_matrix[0][0] = g[0]
    cam_matrix[0][1] = 0  # g[1]
    cam_matrix[0][2] = g[3]
    cam_matrix[1][1] = g[2]
    cam_matrix[1][2] = g[4]
    cam_matrix[2][2] = 1

    dist_coef = np.array([g[5:10]])

    if extract_rt:
        # preencher rt
        rvecs, tvecs = [], []
        for i in range(0, num_images):
            rvecs.append(np.array([g[10 + i * 6 + 0], g[10 + i * 6 + 1], g[10 + i * 6 + 2]]))
            tvecs.append(np.array([g[10 + i * 6 + 3], g[10 + i * 6 + 4], g[10 + i * 6 + 5]]))

        return cam_matrix, dist_coef, rvecs, tvecs
    else:
        return cam_matrix, dist_coef


def get_params_dist(dist_coef):
    c = dist_coef[0, :]

    # region params to lmfit
    params = lmfit.Parameters()

    for i in range(0, 5):
        params.add('d' + str(i), value=c[i], vary=True)

    return params


def extract_params_dist(lmfit_result_params):
    c = lmfit_result_params
    g = [c['d0'].value, c['d1'].value, c['d2'].value, c['d3'].value, c['d4'].value]

    dist_coef = np.array([g])

    return dist_coef


########################################################
#    Funcoes de otimizacao com Colinearidade (all)
########################################################


# funcao objetivo com collinearidade
def funcao_obj_cam_coll(param, orig_pontos, pattern_size, obj_points):
    camera_matrix, dist_coefs, rvecs, tvecs = extract_params(param, True, len(orig_pontos))

    # Pontos projetados da Colinearidade
    fit_line_points_back = calibration_util.getProjectedPointsInFittedLine(
        obj_points, pattern_size, camera_matrix, dist_coefs, rvecs, tvecs)

    # Pontos reprojetados como normalmente eh feito
    dist_model_points = []
    for i in range(0, len(orig_pontos)):
        imagePointsProj, jacobian = cv2.projectPoints(obj_points[i], rvecs[i], tvecs[i], camera_matrix, dist_coefs)
        dist_model_points.append(imagePointsProj)

    # converte todos em um soh array
    # all_images_points = calibration_util.reshape_list_of_np_arrays(orig_pontos)
    # all_points_fit_lined = calibration_util.reshape_list_of_np_arrays(fit_line_points_back)

    # coloca os dois ao mesmo tempo
    # min(x-x_proj)
    # min(x-x_coll)
    orig_pontos_r = calibration_util.reshape_list_of_np_arrays(orig_pontos)
    fit_line_points_back_r = calibration_util.reshape_list_of_np_arrays(fit_line_points_back)
    dist_model_points_r = calibration_util.reshape_list_of_np_arrays(dist_model_points)

    all_images_points = np.concatenate((orig_pontos_r, orig_pontos_r))
    all_points_fit_lined = np.concatenate((fit_line_points_back_r, dist_model_points_r))

    # Calculando a diferenca (distancia euclidiana)
    all_dist = np.linalg.norm(all_images_points - all_points_fit_lined, axis=1)

    return all_dist


# Otimizar com Colinearidade
def optimize_calibration_coll(pattern_size, camera_matrix, dist_coefs, img_points, rvecs, tvecs, obj_points):
    print '\n===== Optimizing with Collinearity ====='
    # Initialize parameters
    params = get_params(camera_matrix, dist_coefs, rvecs, tvecs)

    # Run optimization
    res = lmfit.minimize(funcao_obj_cam_coll, params, args=(img_points, pattern_size, obj_points))

    # Retrieve parameters
    camera_matrix, dist_coefs, rvecs, tvecs = extract_params(res.params, True, len(img_points))

    if settings_flags.SHOW_OPTIMIZING_RESULT:
        print lmfit.fit_report(res)

    return camera_matrix, dist_coefs, rvecs, tvecs


########################################################
#    Funcoes de otimizacao normal all
########################################################


# funcao objetivo com collinearidade
def funcao_obj_cam(param, obj_points, orig_pontos):
    camera_matrix, dist_coefs, rvecs, tvecs = extract_params(param, True, len(orig_pontos))

    # pegar os pontos do modelo e projetar e aplicar distorcao
    dist_model_points = []
    for i in range(0, len(orig_pontos)):
        imagePointsProj, jacobian = cv2.projectPoints(obj_points[i], rvecs[i], tvecs[i], camera_matrix, dist_coefs)
        dist_model_points.append(imagePointsProj)

    all_images_points = calibration_util.reshape_list_of_np_arrays(orig_pontos)
    all_points_fit_lined = calibration_util.reshape_list_of_np_arrays(dist_model_points)

    # calcular erro de reprojecao (distancia euclidiana)
    all_dist = np.linalg.norm(all_images_points - all_points_fit_lined, axis=1)

    return all_dist


def optimize_calibration(objectPoints, imagePoints, camera_matrix, rvecs, tvecs, dist_coefs=None):
    if dist_coefs is None:
        dist_coefs = np.array([[0, 0, 0, 0, 0]], dtype=np.float64)

    print '\n===== Optimizing Extrinsics ====='

    # Initialize parameters
    params = get_params(camera_matrix, dist_coefs, rvecs, tvecs)

    # Run optimization
    res = lmfit.minimize(funcao_obj_cam, params, args=(objectPoints, imagePoints))

    print '===== Done ====='

    if settings_flags.SHOW_OPTIMIZING_RESULT:
        print lmfit.fit_report(res)

    # Retrieve parameters
    camera_matrix, dist_coefs, rvecs, tvecs = extract_params(res.params, True, len(imagePoints))
    rms = calibration_util.getRMS(objectPoints, imagePoints, camera_matrix, dist_coefs, rvecs, tvecs)

    return rms, camera_matrix, dist_coefs, rvecs, tvecs


########################################################
#    Funcoes de otimizacao com Colinearidade new (dist only)
########################################################


# funcao objetivo com collinearidade
def funcao_obj_cam_coll_new(param, orig_pontos, pattern_size, obj_points, camera_matrix, rvecs, tvecs):
    dist_coefs = extract_params_dist(param)
    # camera_matrix, dist_coefs, rvecs, tvecs = extract_params(param, True, len(orig_pontos))

    # Pontos reprojetados como normalmente eh feito
    dist_model_points = []
    for i in range(0, len(orig_pontos)):
        imagePointsProj, jacobian = cv2.projectPoints(obj_points[i], rvecs[i], tvecs[i], camera_matrix, dist_coefs)
        dist_model_points.append(imagePointsProj)

    # remover distorcao
    centros_undistorted = []
    for i in range(0, len(orig_pontos)):
        pts = np.zeros((1, len(dist_model_points[i]), 2), dtype=np.float64)
        for k in range(0, len(dist_model_points[i])):
            pts[0][k][0] = dist_model_points[i][k][0][0]
            pts[0][k][1] = dist_model_points[i][k][0][1]
        centros_undist = cv2.undistortPoints(pts, camera_matrix, dist_coefs, None, camera_matrix)[0]
        centros_undistorted.append(centros_undist)

    # calcular pontos projetados na linha ajustada (colinearidade)
    fit_line_points_back = calibration_util.getProjectedPointsInFittedLine2(
        centros_undistorted, pattern_size, camera_matrix, dist_coefs)

    # converte todos em um soh array
    # all_images_points = calibration_util.reshape_list_of_np_arrays(orig_pontos)
    # all_points_fit_lined = calibration_util.reshape_list_of_np_arrays(fit_line_points_back)

    # coloca os dois ao mesmo tempo: min(x-x_proj) e min(x-x_coll)
    orig_pontos_r = calibration_util.reshape_list_of_np_arrays(orig_pontos)
    fit_line_points_back_r = calibration_util.reshape_list_of_np_arrays(fit_line_points_back)
    dist_model_points_r = calibration_util.reshape_list_of_np_arrays(dist_model_points)

    # # pegar media entre os pontos projetados e os distorcidos do modelo
    # pontos_medio = np.zeros(shape=(len(orig_pontos) * len(orig_pontos[0]), 2))
    # for i in range(len(pontos_medio)):
    #     pontos_medio[i][0] = (fit_line_points_back_r[i][0] + dist_model_points_r[i][0])/2.0
    #     # pontos_medio[i][0] = (fit_line_points_back_r[i][0] + dist_model_points_r[i][0] + orig_pontos_r[i][0])/3.0
    #     pontos_medio[i][1] = (fit_line_points_back_r[i][1] + dist_model_points_r[i][1])/2.0
    #     # pontos_medio[i][1] = (fit_line_points_back_r[i][1] + dist_model_points_r[i][1] + orig_pontos_r[i][1])/3.0

    # peso igual
    all_images_points = np.concatenate((orig_pontos_r, orig_pontos_r))
    all_points_fit_lined = np.concatenate((fit_line_points_back_r, dist_model_points_r))
    # all_points_fit_lined = np.concatenate((pontos_medio, dist_model_points_r))

    # # # maior peso para o dist_model_points
    # all_images_points = np.concatenate((orig_pontos_r, orig_pontos_r, orig_pontos_r, orig_pontos_r, orig_pontos_r))
    # all_points_fit_lined = np.concatenate(
    #     (fit_line_points_back_r, dist_model_points_r, dist_model_points_r, dist_model_points_r, dist_model_points_r))

    # Calculando a diferenca (distancia euclidiana)
    # all_dist = np.linalg.norm(all_images_points - all_points_fit_lined, axis=1)
    all_dist = np.linalg.norm(all_images_points - all_points_fit_lined, axis=1)
    # all_dist = np.linalg.norm(orig_pontos_r - dist_model_points_r, axis=1) +\
    #            np.linalg.norm(orig_pontos_r - fit_line_points_back_r, axis=1)

    # for i in range(10):
    #     print all_images_points[i], all_points_fit_lined[i], dist_model_points[0][i]

    return all_dist


# Otimizar com Colinearidade
def optimize_calibration_coll_new(pattern_size, camera_matrix, dist_coefs, img_points, rvecs, tvecs, obj_points):
    print '\n===== Optimizing with Collinearity ====='
    # Initialize parameters
    params = get_params_dist(dist_coefs)
    # params = get_params(camera_matrix, dist_coefs, rvecs, tvecs)

    # Run optimization
    res = lmfit.minimize(funcao_obj_cam_coll_new, params,
                         args=(img_points, pattern_size, obj_points, camera_matrix, rvecs, tvecs))

    # Retrieve parameters
    dist_coefs = extract_params_dist(res.params)
    # camera_matrix, dist_coefs, rvecs, tvecs = extract_params(res.params, True, len(img_points))

    if settings_flags.SHOW_OPTIMIZING_RESULT:
        print lmfit.fit_report(res)

    rms = calibration_util.getRMS(obj_points, img_points, camera_matrix, dist_coefs, rvecs, tvecs)

    return rms, camera_matrix, dist_coefs, rvecs, tvecs


########################################################
#    Funcoes de otimizacao com Colinearidade new
########################################################


# funcao objetivo com collinearidade
def funcao_obj_cam_coll_residual(param, orig_pontos, pattern_size, obj_points, camera_matrix, rvecs, tvecs):
    dist_coefs = extract_params_dist(param)

    # Pontos reprojetados como normalmente eh feito
    dist_model_points = []
    for i in range(0, len(orig_pontos)):
        imagePointsProj, jacobian = cv2.projectPoints(obj_points[i], rvecs[i], tvecs[i], camera_matrix, dist_coefs)
        dist_model_points.append(imagePointsProj)

    # remover distorcao
    centros_undistorted = []
    for i in range(0, len(orig_pontos)):
        pts = np.zeros((1, len(dist_model_points[i]), 2), dtype=np.float64)
        for k in range(0, len(dist_model_points[i])):
            pts[0][k][0] = dist_model_points[i][k][0][0]
            pts[0][k][1] = dist_model_points[i][k][0][1]
        centros_undist = cv2.undistortPoints(pts, camera_matrix, dist_coefs, None, camera_matrix)[0]
        centros_undistorted.append(centros_undist)

    # receber soma dos erros de cada linha de cada imagem
    residuals_lines = calibration_util.getResidualsOfFittedLines(
        centros_undistorted, pattern_size)

    out = residuals_lines.reshape(-1, len(residuals_lines)*len(residuals_lines[0]))

    # return all_dist
    return out


# Otimizar com Colinearidade
def optimize_calibration_coll_residual(pattern_size, camera_matrix, dist_coefs, img_points, rvecs, tvecs, obj_points):
    print '\n===== Optimizing with Collinearity ====='
    # Initialize parameters
    params = get_params_dist(dist_coefs)

    # Run optimization
    res = lmfit.minimize(funcao_obj_cam_coll_residual, params,
                         args=(img_points, pattern_size, obj_points, camera_matrix, rvecs, tvecs))

    # Retrieve parameters
    dist_coefs = extract_params_dist(res.params)

    if settings_flags.SHOW_OPTIMIZING_RESULT:
        print lmfit.fit_report(res)

    rms = calibration_util.getRMS(obj_points, img_points, camera_matrix, dist_coefs, rvecs, tvecs)

    return rms, camera_matrix, dist_coefs, rvecs, tvecs
