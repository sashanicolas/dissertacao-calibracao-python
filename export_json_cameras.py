__author__ = 'sasha'

import json
import numpy as np
import cv2
import os

import logging

logging.basicConfig(level=logging.DEBUG)

data_cameras = {
    "version": "1.0",
    "data": {
        "cameras": []
    }
}

data_cam_txt = 'C:\Users\sasha\Documents\ComputerVisionUnity\ComputerVision\data_cam.txt'

import os.path
if os.path.isfile(data_cam_txt):
    with open(data_cam_txt, 'w') as outfile:
            json.dump(data_cameras, outfile, indent=4)


def exportCameraPositionsJSON(rvecs, tvecs):
    global data_cameras
    # print rvecs
    # print tvecs

    for i in range(0, len(rvecs)):
        matrix4x4 = convertCVtoGL(rvecs[i], tvecs[i])
        # print "matrix4x4 \n", matrix4x4
        matrix4x4list = matrix4x4.reshape(16).tolist()
        # print "matrix4x4.tolist() \n", matrix4x4list
        # os.system("pause")
        if len(data_cameras["data"]["cameras"]) < i+1:
            data_cameras["data"]["cameras"].append([])
        data_cameras["data"]["cameras"][i].append({
            "model_view_matrix": matrix4x4list
        })

    import os.path
    if os.path.isfile(data_cam_txt):
        with open(data_cam_txt, 'w') as outfile:
                json.dump(data_cameras, outfile, indent=4)


def convertCVtoGL(rvec, tvec):
    rotation, jacobian = cv2.Rodrigues(rvec)

    # print "rotation \n", rotation
    viewMatrix = np.zeros((4, 4), dtype=np.float64)

    for row in range(0, 3):
        for col in range(0, 3):
            viewMatrix[row][col] = rotation[row][col]
        viewMatrix[row][3] = tvec[row]

    viewMatrix[3][3] = 1.0

    cvToGl = np.zeros((4, 4), dtype=np.float64)
    cvToGl[0][0] = 1.0
    cvToGl[1][1] = -1.0
    cvToGl[2][2] = -1.0
    cvToGl[3][3] = 1.0

    logging.debug("cvToGl")
    # logging.debug("cvToGl \n", cvToGl)
    # logging.debug("viewMatrix \n", viewMatrix)
    viewMatrix = np.dot(cvToGl, viewMatrix)
    # logging.debug("cvToGl * viewMatrix \n", viewMatrix)

    # glViewMatrix = np.zeros((4, 4), dtype=np.float64)
    # viewMatrix = cv2.transpose(viewMatrix)
    # logging.debug("cv2.transpose(viewMatrix) \n", glViewMatrix)

    return viewMatrix


if __name__ == "__main__":
    exportCameraPositionsJSON(1, 2)

    # cv::Mat cvToGl = cv::Mat::zeros(4, 4, CV_64F);
    # cvToGl.at<double>(0, 0) = 1.0f;
    # cvToGl.at<double>(1, 1) = -1.0f; // Invert the y axis
    # cvToGl.at<double>(2, 2) = -1.0f; // invert the z axis
    # cvToGl.at<double>(3, 3) = 1.0f;
    # viewMatrix = cvToGl * viewMatrix;
