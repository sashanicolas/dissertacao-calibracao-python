# -*- coding: utf-8 -*-
import cv2
import copy
import numpy as np
import settings_flags
from find_ring_grid import DrawingUtils, RingsGrid

try:
    import matplotlib.pyplot as plt
except ImportError:
    plt = None
import export_json_cameras

__author__ = 'Sasha Nicolas'

########################################################
#
#    Metodos auxiliares
#
########################################################

colors2 = ["#1CE6FF", "#FF34FF", "#FF4A46", "#008941", "#006FA6", "#A30059",
           "#FFDBE5", "#7A4900", "#0000A6", "#63FFAC", "#B79762", "#004D43", "#8FB0FF", "#997D87",
           "#5A0007", "#809693", "#FEFFE6", "#1B4400", "#4FC601", "#3B5DFF", "#4A3B53", "#FF2F80"]


def pause():
    import os
    os.system('pause')


def _ind2(linha, coluna, columns):
    return linha * columns + coluna


def distancePointLine(p, start, end):
    end = end - start
    p = p - start

    end = end / np.linalg.norm(end)

    p_proj = np.dot(p, end) * end
    dist = p - p_proj

    return np.linalg.norm(dist)


def distancePointLine2(line, ponto, correct):
    a = np.array([ponto[0] - 10.0, line(ponto[0] - 10.0)], dtype=np.float64)
    b = np.array([ponto[0] + 10.0, line(ponto[0] + 10.0)], dtype=np.float64)
    if correct:
        a = np.array([ponto[1] - 10.0, line(ponto[1] - 10.0)], dtype=np.float64)
        b = np.array([ponto[1] + 10.0, line(ponto[1] + 10.0)], dtype=np.float64)
    ab = b - a
    ab /= np.linalg.norm(ab)  # unitario
    ap = ponto - a
    p_proj = np.dot(ap, ab) * ab

    return np.linalg.norm(a + p_proj - ponto)


def reshape_list_of_np_arrays(list_of_np_array):
    single_np_array = np.ndarray(shape=(len(list_of_np_array) * len(list_of_np_array[0]), 2))

    for i in range(0, len(list_of_np_array)):
        idx = i * len(list_of_np_array[i])

        for j in range(0, len(list_of_np_array[i])):
            single_np_array[idx + j] = list_of_np_array[i][j]

    return single_np_array


def computeCollinearityReprojError(img_pontos, pattern_size, camera_matrix, dist_coefs, rvecs, tvecs, obj_points):
    fit_line_points_back = getProjectedPointsInFittedLine(
        obj_points, pattern_size, camera_matrix, dist_coefs, rvecs, tvecs)

    all_points_fit_lined = reshape_list_of_np_arrays(fit_line_points_back)
    all_images_points = reshape_list_of_np_arrays(img_pontos)

    # Calculando a diferenca (distancia euclidiana)
    all_dist = np.linalg.norm(all_images_points - all_points_fit_lined, axis=1) ** 2

    return np.sum(all_dist)


def computeCollinearityError(points_in_grid_form, nVert, nHoriz):
    error = 0

    x = np.ndarray((nVert), dtype=np.float32)
    y = np.ndarray((nVert), dtype=np.float32)
    # para cada linha i
    for i in range(0, nHoriz):
        # para cada ponto na linha i
        for j in range(0, nVert):
            x[j] = points_in_grid_form[_ind2(i, j, nVert)][0]
            y[j] = points_in_grid_form[_ind2(i, j, nVert)][1]
        error += np.polyfit(x, y, 1, full=True)[1][0]

    # x = np.ndarray((nHoriz), dtype=np.float32)
    # y = np.ndarray((nHoriz), dtype=np.float32)
    # # para cada coluna j
    # for j in range(0, nVert):
    #     # para cada ponto na coluna j
    #     for i in range(0, nHoriz):
    #         x[i] = points_in_grid_form[_ind2(i, j, nVert)][0]
    #         y[i] = points_in_grid_form[_ind2(i, j, nVert)][1]
    #
    #     error += np.polyfit(x, y, 1, full=True)[1][0]

    # para diagonais principais, nao eh quadrado
    x = np.ndarray((nHoriz), dtype=np.float32)
    y = np.ndarray((nHoriz), dtype=np.float32)
    for j in range(0, nVert - nHoriz + 1):
        if _ind2(0, nVert - nHoriz, nVert) >= len(points_in_grid_form):
            break
        # para cada ponto na linha i
        for k in range(0, nHoriz):
            x[k] = k
            y[k] = points_in_grid_form[_ind2(k, j + k, nVert)][1]

        error += np.polyfit(x, y, 1, full=True)[1][0]

    # img = np.ones((480, 640, 3))
    # DrawingUtils.desenhaCruz(p[0], p[1], (255, 0, 0), img)
    # DrawingUtils.desenhaCruz(start[0], start[1], (0, 255, 0), img)
    # DrawingUtils.desenhaCruz(end[0], end[1], (0, 0, 255), img)
    # DrawingUtils.desenhaCentros(img, points_in_grid_form, color=(0,0,0))
    # DrawingUtils.mostra_imagem('img', img)

    # pause()
    return error


def computeCollinearityErrorOLD(grid_in_vector, nVert, nHoriz):
    # print "grid_in_vector \n",grid_in_vector
    error = 0

    # para cada linha i
    for i in range(0, nHoriz):
        # para cada ponto na linha i
        for j in range(1, nVert - 1):
            start = grid_in_vector[_ind2(i, 0, nVert)][:2]
            end = grid_in_vector[_ind2(i, nVert - 1, nVert)][:2]
            p = grid_in_vector[_ind2(i, j, nVert)][:2]
            # print start, end, p
            error += distancePointLine(p, start, end)

    # para cada coluna j
    for j in range(0, nVert):
        # para cada ponto na coluna j
        for i in range(1, nHoriz - 1):
            start = grid_in_vector[_ind2(0, j, nVert)][:2]
            end = grid_in_vector[_ind2(nHoriz - 1, j, nVert)][:2]
            p = grid_in_vector[_ind2(i, j, nVert)][:2]
            error += distancePointLine(p, start, end)

    # para diagonais principais, nao eh quadrado
    for j in range(0, nVert - nHoriz + 1):
        if _ind2(0, nVert - nHoriz, nVert) >= len(grid_in_vector):
            break
        # para cada ponto na linha i
        for k in range(1, nHoriz - 1):
            start = grid_in_vector[_ind2(0, j, nVert)][:2]
            end = grid_in_vector[_ind2(nHoriz - 1, j + nHoriz - 1, nVert)][:2]
            p = grid_in_vector[_ind2(k, j + k, nVert)][:2]
            # print p, start, end
            error += distancePointLine(p, start, end)

    # img = np.ones((480, 640, 3))
    # DrawingUtils.desenhaCruz(p[0], p[1], (255, 0, 0), img)
    # DrawingUtils.desenhaCruz(start[0], start[1], (0, 255, 0), img)
    # DrawingUtils.desenhaCruz(end[0], end[1], (0, 0, 255), img)
    # DrawingUtils.desenhaCentros(img, grid_in_vector, color=(0,0,0))
    # DrawingUtils.mostra_imagem('img', img)

    # pause()
    return error


def init_cloud_chart():
    if plt is None:
        return

    plt.close('all')
    fig = plt.figure(figsize=(8, 8), dpi=100)
    ax = plt.subplot(111)
    plt.grid(True)
    ax.set_xlim([-1.5, 1.5])
    ax.set_ylim([-1.5, 1.5])
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_aspect('equal', adjustable='box')


def plot_on_cloud_chart(reprojected_normalized_points, color):
    plt.plot(reprojected_normalized_points[:, 0], reprojected_normalized_points[:, 1], "+", c=color)


def computeReprojectionAndCollinearityErrors(
        images, pattern_size, obj_points, img_points, rvecs, tvecs, camera_matrix, dist_coefs,
        cloud_file=None, rms=None):
    # TODO: parece que o nHorizonte eh o vetical e vice versa, nao parece que soh o nome ta trocado
    nHorizontal = pattern_size[0]
    nVertical = pattern_size[1]
    n = nHorizontal * nVertical
    totalReprojErr = 0
    totalCollinearityErr = 0
    totalPoints = 0
    # reprojected_normalized_points = []

    # inicializa grafico de nuvem
    if cloud_file is not None:
        init_cloud_chart()

    # para cada imagem
    for i in range(0, len(obj_points)):
        imagePointsProj, jacobian = cv2.projectPoints(obj_points[i], rvecs[i], tvecs[i], camera_matrix, dist_coefs)

        # region if MOSTRA_REPROJECAO
        if settings_flags.MOSTRA_REPROJECAO:

            img_aux = images[i].copy()

            for k in range(0, nVertical):
                for j in range(0, nHorizontal):
                    DrawingUtils.desenhaCruz(
                        img_points[i][k * nHorizontal + j][0],
                        img_points[i][k * nHorizontal + j][1], (0, 255, 255), img_aux)

                    DrawingUtils.desenhaCruz(
                        imagePointsProj[k * nHorizontal + j][0][0],
                        imagePointsProj[k * nHorizontal + j][0][1], (0, 0, 255), img_aux)

            state = settings_flags.FUNCAO_MOSTRA_IMAGEM_ENABLED
            settings_flags.FUNCAO_MOSTRA_IMAGEM_ENABLED = True

            DrawingUtils.mostra_imagem('Reprojecao', img_aux, wait_key_zero=True)

            settings_flags.FUNCAO_MOSTRA_IMAGEM_ENABLED = state

        # endregion

        # necessario pq tem que ser tupla
        imagePointsProj2 = np.array(img_points[i])
        for g in range(0, len(imagePointsProj)):
            imagePointsProj2[g] = (imagePointsProj[g][0])

        # Calcula erro de reprojecao para grid i
        normalized = imagePointsProj2 - img_points[i]
        sum = 0
        for k in range(len(normalized)):
            sum += (normalized[k][0] ** 2 + normalized[k][1] ** 2) ** 0.5
        totalReprojErr += sum
        totalPoints += n

        # Calcula erro de colinearidade para grid i da imagem i, com os pontos sem distorcao
        totalCollinearityErr += computeCollinearityReprojError(
            [img_points[i]], pattern_size, camera_matrix, dist_coefs, [rvecs[i]], [tvecs[i]], [obj_points[i]])

        # normaliza pontos para o grafico de nuvem
        if cloud_file is not None:
            # plot_on_cloud_chart(normalized, color=colors.keys()[i])
            plot_on_cloud_chart(normalized, color=colors2[i])

    cv2.destroyWindow("Reprojecao")

    # Mostrar Grafico de nuvem
    if cloud_file is not None and rms is not None:
        ax = plt.gca()
        ax.set_title(u"Erro de reprojeção (em pixels): {0:.6f}".format(rms))
        plt.savefig(cloud_file + ".png")

    return totalReprojErr / float(totalPoints), totalCollinearityErr / float(totalPoints)


def showResultadoCalibracao(
        pattern, rms, camera_matrix, dist_coefs, original_images, pattern_size, obj_points, img_points, rvecs, tvecs,
        iteration=None, fronto_points=None, undist_points=None, cloud_file=None):
    if settings_flags.SHOW_PARCIAL_CALIBRATION_RESULT:
        print('\n---------------------------------------------')
        # if iteration is not None:
        #     print '>>>>>>>>>>> Iteracao ', iteration, ' <<<<<<<<<<<<'
        print 'Resultado - Calibracao - {}\n'.format(pattern)
        print "RMS: ", rms

    totalReprojErr = -1
    rangeOk = cv2.checkRange(camera_matrix) and cv2.checkRange(dist_coefs)

    CollinearityErr = None
    if rangeOk:
        totalReprojErr, CollinearityErr = computeReprojectionAndCollinearityErrors(
            original_images, pattern_size, obj_points, img_points, rvecs, tvecs, camera_matrix, dist_coefs,
            cloud_file, rms=rms)

        # region Show partial calibration result
        if settings_flags.SHOW_PARCIAL_CALIBRATION_RESULT:
            if settings_flags.SHOW_ROTATION_TRANSLATION_VETORS:
                print "Own RMS = ", totalReprojErr
            else:
                print "Avg re-projection error = ", totalReprojErr
            if fronto_points is not None or undist_points is not None:
                print "Collinearity error = ", CollinearityErr
            print "Camera matrix: \n", camera_matrix
            print "Distortion coefficients: \n", dist_coefs.ravel()
            if settings_flags.SHOW_ROTATION_TRANSLATION_VETORS:
                print "Rotation values:"
                for i in rvecs: print i.ravel()
                print "Translation values:"
                for i in tvecs: print i.ravel()

                # export_json_cameras.exportCameraPositionsJSON(rvecs, tvecs)
                pass
        pass
        # endregion

    print('---------------------------------------------')

    return totalReprojErr, CollinearityErr


# Aqui nao serao necessarias as imagens
def computeReprojectionErrors2(pattern_size, obj_points, img_points, rvecs, tvecs, camera_matrix, dist_coefs):
    nHorizontal = pattern_size[0]
    nVertical = pattern_size[1]
    n = nHorizontal * nVertical
    totalErr = 0
    totalPoints = 0

    totalErr_2 = 0
    for i in range(0, len(obj_points)):
        imagePointsProj, jacobian = cv2.projectPoints(obj_points[i], rvecs[i], tvecs[i], camera_matrix, dist_coefs)

        imagePointsProj2 = np.array(img_points[i])
        for g in range(0, len(imagePointsProj)):
            imagePointsProj2[g] = (imagePointsProj[g][0])

        # print 'img_points[i] ', repr(img_points[i])
        # print 'imagePointsProj2 ', repr(imagePointsProj2)

        err = 0
        err_2 = 0
        for g in range(0, len(imagePointsProj2)):
            err += cv2.norm(img_points[i][g], imagePointsProj2[g],
                            cv2.NORM_L2)  # distancia entre ponto original e projetado
            err_2 += cv2.norm(img_points[i][g], imagePointsProj2[g], cv2.NORM_L2) ** 2

        totalErr += err
        totalErr_2 += err_2
        totalPoints += len(imagePointsProj2)

    # print "RMS meu = ", (totalErr_2/float(totalPoints))**(0.5)
    return totalErr / float(totalPoints)


# Aqui nao serao necessarias as imagens
def showResultadoCalibracao2(rms, camera_matrix, dist_coefs, pattern_size, obj_points, img_points, rvecs, tvecs):
    print('---------------------------------------------')
    print 'Resultado - Calibracao'
    print "RMS: ", rms

    totalAvgErr = -1
    rangeOk = cv2.checkRange(camera_matrix)  # and cv2.checkRange(dist_coefs)
    if rangeOk:
        totalAvgErr = computeReprojectionErrors2(
            pattern_size, obj_points, img_points, rvecs, tvecs, camera_matrix, dist_coefs)

        # if SHOW_PARCIAL_CALIBRATION_RESULT:
        print "Dist Euclid Media da Reproj= ", totalAvgErr
        print "Camera matrix:\n", camera_matrix
        print "Distortion coefficients:\n", dist_coefs.ravel()
        if settings_flags.SHOW_ROTATION_TRANSLATION_VETORS:
            print "Rotation values:"
            for i in rvecs: print i.ravel()
            print "Translation values:"
            for i in tvecs: print i.ravel()

            # export_json_cameras.exportCameraPositionsJSON(rvecs, tvecs)

    print('\n---------------------------------------------\n')

    return totalAvgErr


def getPatternPointsInZ(pattern_size, square_size):
    pattern_points = np.zeros((np.prod(pattern_size), 3), np.float32)
    pattern_points[:, :2] = np.indices(pattern_size).T.reshape(-1, 2)
    pattern_points *= square_size
    return pattern_points


def zoomRing(img, zoom_ponto, zoom_dim, centers1, centers2, center_id):
    imgCopy = img.copy()
    y1, y2, x1, x2 = zoom_ponto[1], zoom_ponto[1] + zoom_dim[1], zoom_ponto[0], zoom_ponto[0] + zoom_dim[0]
    zoomedImg = imgCopy[y1:y2, x1:x2]

    f = 7
    height, width = zoomedImg.shape[:2]
    zoomedImg = cv2.resize(zoomedImg, (f * width, f * height), interpolation=cv2.INTER_CUBIC)

    DrawingUtils.desenhaCruz(f*(centers1[center_id][0] - x1), f*(centers1[center_id][1] - y1), (0, 255, 0), zoomedImg)
    DrawingUtils.desenhaCruz(f*(centers2[center_id][0] - x1), f*(centers2[center_id][1] - y1), (0, 0, 255), zoomedImg)

    cv2.imwrite("dist_paral_ring.png", zoomedImg)
    state = settings_flags.FUNCAO_MOSTRA_IMAGEM_ENABLED
    settings_flags.FUNCAO_MOSTRA_IMAGEM_ENABLED = True
    DrawingUtils.mostra_imagem("zoomed", zoomedImg)
    settings_flags.FUNCAO_MOSTRA_IMAGEM_ENABLED = state


def debugCompareCenters(showFlag, title, img, centers1, centers2, color1=(0, 255, 0), color2=(0, 0, 255),
                        image_id=None):
    if img is None:
        return

    if showFlag:
        imgCopy = img.copy()
        # zoomRing(imgCopy, (243, 119), (70, 60), centers1, centers2, 0)
        zoomRing(imgCopy, (800, 163), (53, 50), centers1, centers2, 9)
        DrawingUtils.desenhaCentros(imgCopy, centers1, color1, thickness=1)
        DrawingUtils.desenhaCentros(imgCopy, centers2, color2, thickness=1)

        state = settings_flags.FUNCAO_MOSTRA_IMAGEM_ENABLED
        settings_flags.FUNCAO_MOSTRA_IMAGEM_ENABLED = True
        DrawingUtils.mostra_imagem(title, imgCopy, wait_key_zero=True, image_id=image_id)

        settings_flags.FUNCAO_MOSTRA_IMAGEM_ENABLED = state


########################################################
#
#    Metodos Sobre o Processo Ankur
#
########################################################


def get_centros(original_images, images_paths, pattern, pattern_size, pattern_points, find_again=False):
    obj_points = []
    img_points = []
    i = 0
    for img in original_images:
        new_points = False

        gotError = False
        found = False

        # pega centros do padrao, tentar achar em arquivo
        try:
            if not find_again:
                centers = np.load(images_paths[i] + '.npy')
                if settings_flags.SHOW_MESSAGE_FOUND_CENTERS:
                    print 'Achou centros em arquivo para imagem ' + images_paths[i]
                found = True
        except IOError:
            gotError = True

        if find_again or gotError:
            if pattern == 'circle':
                found, centers = cv2.findCirclesGrid(img, pattern_size)
            elif pattern == 'ring':
                found, centers = RingsGrid.findRingsGrid(img, pattern_size, refine=False)

            if found:
                new_points = True

        if found:
            if new_points:
                np.save(images_paths[i], centers)

            if settings_flags.MOSTRA_CENTROS:
                vis = img.copy()
                cv2.drawChessboardCorners(vis, pattern_size, centers, found)

                state = settings_flags.FUNCAO_MOSTRA_IMAGEM_ENABLED
                settings_flags.FUNCAO_MOSTRA_IMAGEM_ENABLED = True

                DrawingUtils.mostra_imagem('MOSTRA_CENTROS Centros', vis)

                settings_flags.FUNCAO_MOSTRA_IMAGEM_ENABLED = state
        else:
            print '  ## {} pattern not found'.format(pattern), ' image=', images_paths[i]
            continue

        img_points.append(centers.reshape(-1, 2))
        obj_points.append(pattern_points)

        if settings_flags.SHOW_MESSAGE_FOUND_CENTERS:
            print('   Done with centers for image.')
        i += 1

    cv2.destroyAllWindows()
    return img_points, obj_points


def undistortImages(original_images, camera_matrix, dist_coefs, centros_original):
    undistort_images = []
    centros_undistorted = []

    for i in range(0, len(original_images)):
        img = original_images[i]

        # arruma dimensao de array
        pts = np.zeros((1, len(centros_original[i]), 2), dtype=np.float64)
        for k in range(0, len(centros_original[i])):
            pts[0][k][0] = centros_original[i][k][0]
            pts[0][k][1] = centros_original[i][k][1]

        # remove a distorcao da imagem e dos centros
        undistort_img = cv2.undistort(img, camera_matrix, dist_coefs)
        centros_undist = cv2.undistortPoints(pts, camera_matrix, dist_coefs, None, camera_matrix)[0]

        # adiciona a imagem e os centros sem a distorcao
        undistort_images.append(undistort_img)
        centros_undistorted.append(centros_undist)

        if settings_flags.MOSTRA_UNDISTORTED:
            img_centros_ori = img.copy()
            img_centros_dist = undistort_img.copy()
            DrawingUtils.desenhaCentros(img_centros_ori, centros_original[i])
            DrawingUtils.desenhaCentros(img_centros_dist, centros_undistorted[i], (0, 255, 255))

            state = settings_flags.FUNCAO_MOSTRA_IMAGEM_ENABLED
            settings_flags.FUNCAO_MOSTRA_IMAGEM_ENABLED = True

            DrawingUtils.mostra_imagem('Original', img_centros_ori, 50, 50)
            DrawingUtils.mostra_imagem('Undistoted', img_centros_dist, 1300, 200)

            settings_flags.FUNCAO_MOSTRA_IMAGEM_ENABLED = state

            cv2.waitKey(0)

    cv2.destroyAllWindows()
    return undistort_images, centros_undistorted


def unprojectToFrontoParallel(pattern_size, undistort_images, centros_undistorted):
    nVertical = pattern_size[0]
    nHorizon = pattern_size[1]

    inputQuad = np.zeros((1, 4, 2), np.float32)
    outputQuad = np.zeros((1, 4, 2), np.float32)

    h, w = undistort_images[0].shape[:2]

    d = w / nVertical
    offset = d / 2
    w = d * nVertical
    h = d * nHorizon

    homografias = []
    unproject_images = []
    centros_unprojected = []

    distX = 0
    distY = 0

    for i in range(0, len(undistort_images)):
        inputImage = undistort_images[i].copy()

        # The 4 points that select quadilateral on the inputImage , from top-left in clockwise order
        # These four pts are the sides of the rect box used as inputImage
        inputQuad[0][0] = centros_undistorted[i][0]
        inputQuad[0][1] = centros_undistorted[i][nVertical - 1]
        inputQuad[0][2] = centros_undistorted[i][nHorizon * nVertical - 1]
        inputQuad[0][3] = centros_undistorted[i][(nHorizon - 1) * nVertical]

        outputQuad[0][0] = [offset, offset]
        outputQuad[0][1] = [w - offset, offset]
        outputQuad[0][2] = [w - offset, h - offset]
        outputQuad[0][3] = [offset, h - offset]

        # Get the Perspective Transform Matrix i.e. lambda
        perspectiveTransformMatrix = cv2.getPerspectiveTransform(inputQuad, outputQuad)
        # salvar homografia
        homografias.append(perspectiveTransformMatrix)

        # Apply the Perspective Transform just found to the src image
        outputImage = cv2.warpPerspective(inputImage, perspectiveTransformMatrix, (w, h))
        # outputImage = cv2.warpPerspective(inputImage, perspectiveTransformMatrix, (w - int(diametro), h))

        unproject_images.append(outputImage)

        # calcular os centros no plano fronto paralelo tbm
        centros_unproject = cv2.perspectiveTransform(np.array([centros_undistorted[i]]), perspectiveTransformMatrix)[0]
        centros_unprojected.append(centros_unproject)

        # calcula distancia entre centros no fronto paralelo
        if i == 0:
            distX = cv2.norm(centros_unproject[1] - centros_unproject[0])
            distY = cv2.norm(centros_unproject[nVertical] - centros_unproject[0])

        # region Debug fronto paralelo
        if settings_flags.MOSTRA_FRONTO_PARALELO:
            img_centros_undist = inputImage.copy()
            img_centros_fronto = outputImage.copy()
            DrawingUtils.desenhaCentros(img_centros_undist, centros_undistorted[i])
            DrawingUtils.desenhaCentros(img_centros_fronto, centros_unprojected[i], (0, 0, 255))
            DrawingUtils.desenhaCruz(offset, offset, (255, 255, 0), img_centros_fronto)

            state = settings_flags.FUNCAO_MOSTRA_IMAGEM_ENABLED
            settings_flags.FUNCAO_MOSTRA_IMAGEM_ENABLED = True

            DrawingUtils.mostra_imagem('Undistorted', img_centros_undist, 50, 50)
            DrawingUtils.mostra_imagem('Unprojected', img_centros_fronto, 800, 200)

            settings_flags.FUNCAO_MOSTRA_IMAGEM_ENABLED = state

            cv2.waitKey(0)
        pass
        # endregion
    cv2.destroyAllWindows()
    return unproject_images, centros_unprojected, homografias, [distX, distY]


def find_centros_fronto_parallel(unproject_images, centros_unprojected, roiSide, pattern, pattern_size):
    '''
    Aqui, deve se encontrar as novas posicoes dos centros nas imagens em fronto paralelo.
    Se o padrao for circulo, usa o proprio finCircles do OpenCV. Caso seja Ring, percorre os centros que foram
        colocados em fronto paralelo, criando uma roi de lado roiSide em torno desse centro e procurando os
        contornos dos aneis, fazendo o fitEllipse simples
    :param unproject_images:
    :param centros_unprojected:
    :param roiSide:
    :param pattern:
    :param pattern_size:
    :return:
    '''
    novos_centros_fronto = np.zeros((len(unproject_images), pattern_size[0] * pattern_size[1], 2), dtype=np.float32)

    if pattern == 'circle':
        for i in range(0, len(unproject_images)):
            found, refined_centers = cv2.findCirclesGrid(unproject_images[i], pattern_size)
            if not found:
                print 'Circle grid not found for image', i
            novos_centros_fronto[i] = refined_centers.reshape(-1, 2)

    elif pattern == 'ring':
        if settings_flags.USE_PRAKASH_THRESHOLD_ADAP:
            print 'Usando Prakash Adaptative Threshold'
            for i in range(0, len(unproject_images)):
                print '.',
            print ''

        for i in range(0, len(unproject_images)):
            found, centers = RingsGrid.findRingsGrid(unproject_images[i], pattern_size, roiSide)
            if not found:
                print '   Ring grid not found for image', i
                return None
            novos_centros_fronto[i] = centers.reshape(-1, 2)

            debugCompareCenters(settings_flags.MOSTRA_NOVOS_FRONTO, 'Novos centros no paralelo (novos = red)',
                                unproject_images[i].copy(), centros_unprojected[i], novos_centros_fronto[i], image_id=i)

        cv2.destroyAllWindows()

    return novos_centros_fronto


def distortCenters(centrosProjetados, camera_matrix, distCoeffs):
    distortedCenters = np.zeros((len(centrosProjetados), 2), dtype=np.float32)
    cx = camera_matrix[0][2]
    cy = camera_matrix[1][2]
    fx = camera_matrix[0][0]
    fy = camera_matrix[1][1]

    k1 = distCoeffs[0][0]
    k2 = distCoeffs[0][1]
    p1 = distCoeffs[0][2]
    p2 = distCoeffs[0][3]
    k3 = distCoeffs[0][4]

    for i in range(0, len(centrosProjetados)):
        # To relative coordinates
        x = (centrosProjetados[i][0] - cx) / fx
        y = (centrosProjetados[i][1] - cy) / fy

        r2 = x * x + y * y

        # Radial distorsion
        xDistort = x * (1 + k1 * r2 + k2 * r2 * r2 + k3 * r2 * r2 * r2)
        yDistort = y * (1 + k1 * r2 + k2 * r2 * r2 + k3 * r2 * r2 * r2)

        # Tangential distorsion
        xDistort += (2 * p1 * x * y + p2 * (r2 + 2 * x * x))
        yDistort += (p1 * (r2 + 2 * y * y) + 2 * p2 * x * y)

        # Back to absolute coordinates.
        xDistort = xDistort * fx + cx
        yDistort = yDistort * fy + cy

        distortedCenters[i][0] = xDistort
        distortedCenters[i][1] = yDistort

    return distortedCenters


def apply_distortion(centros_undist, camera_matrix, dist_coefs):
    # distorce
    centrosDistorted = []

    for i in range(0, len(centros_undist)):
        centrosDist = distortCenters(centros_undist[i], camera_matrix, dist_coefs)
        centrosDistorted.append(centrosDist)

    cv2.destroyAllWindows()

    return centrosDistorted


def apply_projection_and_distortion(
        original_images, undistort_images, novos_centros_fronto, centros_undistorted, img_points, homografias,
        camera_matrix, dist_coefs):
    # projeta
    centrosProjetados = []

    for i in range(0, len(novos_centros_fronto)):
        centrosProj = cv2.perspectiveTransform(np.array([novos_centros_fronto[i]]), np.linalg.inv(homografias[i]))[0]
        centrosProjetados.append(centrosProj)

        if undistort_images is not None:
            debugCompareCenters(settings_flags.MOSTRA_NOVOS_PROJ, 'NOVOS_PROJ (red)', undistort_images[i],
                                centros_undistorted[i], centrosProj, image_id=i)
    cv2.destroyAllWindows()

    # distorce
    centrosDistorted = []

    for i in range(0, len(centrosProjetados)):
        centrosDist = distortCenters(centrosProjetados[i], camera_matrix, dist_coefs)
        centrosDistorted.append(centrosDist)

        if original_images is not None:
            debugCompareCenters(settings_flags.MOSTRA_NOVOS_DISTORTED, 'NOVOS_DISTORTED (red)', original_images[i],
                                img_points[i], centrosDist, image_id=i)
    cv2.destroyAllWindows()

    return centrosDistorted


########################################################
#
#    Metodos Sobre o Meu Processo
#
########################################################


def getRMS(objectPoints, imagePoints, camera_matrix, dist_coefs, rvecs, tvecs):
    totalErr_2 = 0
    totalPoints = 0

    for i in range(0, len(objectPoints)):
        imagePointsProj, jacobian = cv2.projectPoints(objectPoints[i], rvecs[i], tvecs[i], camera_matrix, dist_coefs)

        imagePointsProj2 = np.array(imagePoints[i])
        for g in range(0, len(imagePointsProj)):
            imagePointsProj2[g] = (imagePointsProj[g][0])

        err_2 = 0
        for g in range(0, len(imagePointsProj2)):
            # v = imagePoints[i][g] - imagePointsProj2[g]
            # err_2 += v[0] ** 2 + v[1] ** 2
            err_2 += cv2.norm(imagePoints[i][g], imagePointsProj2[g], cv2.NORM_L2) ** 2

        totalErr_2 += err_2
        totalPoints += len(objectPoints[i])

    rms = (totalErr_2 / float(totalPoints)) ** 0.5

    return rms


def getRMScoll(objectPoints, imagePoints, camera_matrix, dist_coefs, rvecs, tvecs):
    pass


def proj_points_min_dist(ponto, line, correct=False):
    ponto = np.array(ponto, dtype=np.float64)
    a = np.array([ponto[0] - 10.0, line(ponto[0] - 10.0)], dtype=np.float64)
    b = np.array([ponto[0] + 10.0, line(ponto[0] + 10.0)], dtype=np.float64)
    if correct:
        a = np.array([ponto[1] - 10.0, line(ponto[1] - 10.0)], dtype=np.float64)
        b = np.array([ponto[1] + 10.0, line(ponto[1] + 10.0)], dtype=np.float64)
    ab = b - a
    ab /= np.linalg.norm(ab)
    ap = ponto - a
    s = np.dot(ap, ab)
    ab *= s
    # if correct:
    #     a = a + ab
    #     return np.array([a[1], a[0]])
    # else:
    return a + ab


def proj_point_on_line(a, l):
    p0 = np.array([0, l[1]], dtype=np.float64)
    p1 = np.array([1, l[0] + l[1]], dtype=np.float64)

    a_ = a - p0

    p = p1 - p0
    p_ = p / np.linalg.norm(p)

    s = np.dot(a_, p_)

    return s * p_ + p0


def correctPoints(x, y):
    # check x coords
    good = False
    for i in range(0, len(x)):
        if abs(x[i] - x[i - 1]) > 1e-09:
            good = True
            break

    if not good:
        return y, x, True

    return x, y, False


# Projeta pontos na linha ajustada
def projectPointsToLine(grid_points, pattern_size):
    fit_line_points = copy.deepcopy(grid_points)
    fit_line_points[0].fill(0)

    nVert, nHoriz = pattern_size[0], pattern_size[1]
    x = np.ndarray(nVert, dtype=np.float32)
    y = np.ndarray(nVert, dtype=np.float32)

    # fitted_line_result = np.empty(shape=(len(grid_points), nHoriz))
    fitted_line_result = [[None] * nHoriz for j in xrange(len(grid_points))]

    # error_medio_fitting = 0
    # Para cada conjunto de pontos (n imagens)
    for idx in range(0, len(grid_points)):
        # calcular fitting de cada linha (nHoriz linhas)
        for i in range(0, nHoriz):  # para cada linha i
            # im2 = img.copy()
            # separa pontos da linha para fazer o fit
            for j in range(0, nVert):  # para cada ponto na linha i
                jdx = _ind2(i, j, nVert)
                x[j] = grid_points[idx][jdx][0]
                y[j] = grid_points[idx][jdx][1]

            x, y, corrected = correctPoints(x, y)

            # faz o fit e salva o resultado
            fitted_line_result[idx][i] = np.polyfit(x, y, 1, full=True)

            # projetar pontos nas linhas e salvar pontos
            line = np.poly1d(fitted_line_result[idx][i][0])

            erro = 0
            # import math
            for j in range(0, nVert):  # para cada ponto na linha i
                fit_line_points[idx][_ind2(i, j, nVert)] = proj_points_min_dist(
                    grid_points[idx][_ind2(i, j, nVert)], line, corrected)

                erro += distancePointLine2(line, grid_points[idx][_ind2(i, j, nVert)], corrected)
                pass
                # print 'erro fitting line {0:.18f}'.format(erro)
                # print 'erro fitting line {0:.18f}'.format(float(fitted_line_result[idx][i][1]))

    return fit_line_points


def getProjectedPointsInFittedLine(model_pontos, pattern_size, camera_matrix, dist_coefs, rvecs, tvecs):
    # projetar pontos do modelo sem distorcao
    proj_points = []
    for i in range(0, len(model_pontos)):
        imagePointsProj, jacobian = cv2.projectPoints(model_pontos[i], rvecs[i], tvecs[i], camera_matrix, np.zeros(5))
        imagePointsProj = np.reshape(np.ravel(imagePointsProj), (len(imagePointsProj), 2))
        proj_points.append(imagePointsProj)

    # pegar os pontos projetados nas linhas de fit
    fit_line_points = projectPointsToLine(proj_points, pattern_size)

    # aplicar distorcao
    fit_line_points_proj_dist = apply_distortion(fit_line_points, camera_matrix, dist_coefs)

    return fit_line_points_proj_dist


# ja recebe os pontos sem a distorcao
def getProjectedPointsInFittedLine2(proj_points, pattern_size, camera_matrix, dist_coefs):
    # pegar os pontos projetados nas linhas de fit
    fit_line_points = projectPointsToLine(proj_points, pattern_size)

    # aplica distorcao
    fit_line_points_proj_dist = apply_distortion(fit_line_points, camera_matrix, dist_coefs)

    return fit_line_points_proj_dist


def getResidualsOfFittedLines(grid_points, pattern_size):
    fit_line_points = copy.deepcopy(grid_points)
    fit_line_points[0].fill(0)

    nVert, nHoriz = pattern_size[0], pattern_size[1]
    x = np.ndarray(nVert, dtype=np.float32)
    y = np.ndarray(nVert, dtype=np.float32)

    # fitted_line_result = np.empty(shape=(len(grid_points), nHoriz))
    fitted_line_result = [[None] * nHoriz for j in xrange(len(grid_points))]

    # error_medio_fitting = [[0] * nHoriz for j in range(len(grid_points))]
    error_medio_fitting = np.zeros(shape=(len(grid_points), nHoriz))

    # Para cada conjunto de pontos (n imagens)
    for img_i in range(0, len(grid_points)):
        # calcular fitting de cada linha (nHoriz linhas)
        for linha_i in range(0, nHoriz):  # para cada linha linha_i
            # im2 = img.copy()
            # separa pontos da linha para fazer o fit
            for j in range(0, nVert):  # para cada ponto na linha linha_i
                jdx = _ind2(linha_i, j, nVert)
                x[j] = grid_points[img_i][jdx][0]
                y[j] = grid_points[img_i][jdx][1]

            x, y, corrected = correctPoints(x, y)

            # faz o fit e salva o resultado
            fitted_line_result[img_i][linha_i] = np.polyfit(x, y, 1, full=True)

            # projetar pontos nas linhas e salvar pontos
            line = np.poly1d(fitted_line_result[img_i][linha_i][0])

            erro = 0
            # import math
            for j in range(0, nVert):  # para cada ponto na linha linha_i
                fit_line_points[img_i][_ind2(linha_i, j, nVert)] = proj_points_min_dist(
                    grid_points[img_i][_ind2(linha_i, j, nVert)], line, corrected)

                erro += distancePointLine2(line, grid_points[img_i][_ind2(linha_i, j, nVert)], corrected)
                pass
            error_medio_fitting[img_i][linha_i] += erro

    return error_medio_fitting
