import numpy as np
import scipy
import calibration_util
import images_input
import math
import cv2
from open_data_manuel import load_data_manuel
from optimizing import optimize_calibration
import copy

__author__ = 'sasha'

# Flags
NORMALIZE = True
USE_MANUEL_DATA = True
np.set_printoptions(suppress=True)

########################################################
#
#    Funcoes de
#
########################################################


def normalizePoints(imagePoints, imageSize, N):
    # imagePointsNorm = np.ndarray(shape=imagePoints.shape, dtype=imagePoints.dtype)
    imagePointsNorm = copy.deepcopy(imagePoints)

    sx = 2.0 / imageSize[0]
    sy = 2.0 / imageSize[1]
    x0 = imageSize[0] / 2.0
    y0 = imageSize[1] / 2.0

    for i in range(0, len(imagePointsNorm)):
        for j in range(0, len(imagePointsNorm[i])):
            # print np.append(imagePoints[i][j], 1), np.dot(N, np.append(imagePoints[i][j], 1).T)
            # new_p = np.dot(N, np.append(imagePoints[i][j], 1).T)
            new_p = [sx * (imagePointsNorm[i][j][0] - x0), sy * (imagePointsNorm[i][j][1] - y0)]
            # print new_p
            imagePointsNorm[i][j] = np.array([new_p[0], new_p[1]])

    return imagePointsNorm


def getHomographies(objectPoints, imagePoints):
    homographies = []

    for i in range(0, len(imagePoints)):
        src_pts = np.delete(objectPoints[i], 2, 1).reshape(-1, 1, 2)
        dst_pts = np.array(imagePoints[i]).reshape(-1, 1, 2)

        M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC, 5.0)

        homographies.append(M)

    return homographies


def closed_form_solution(U):
    e_vals, e_vecs = np.linalg.eig(np.dot(U.T, U))
    return e_vecs[:, np.argmin(e_vals)]  # coluna


def getVij(H, i, j):
    v = np.array([[H[0][i] * H[0][j]],
                  [H[0][i] * H[1][j] + H[1][i] * H[0][j]],
                  [H[1][i] * H[1][j]],

                  [H[2][i] * H[0][j] + H[0][i] * H[2][j]],
                  [H[2][i] * H[1][j] + H[1][i] * H[2][j]],
                  [H[2][i] * H[2][j]]])
    return v


def normalizeVector(v):
    norm = np.linalg.norm(v)
    if norm > 1e-15:
        norm = math.sqrt(norm)
        v = v / norm

    return v


def calculate_b(homographies):
    V = np.zeros(2 * len(homographies) * 6).reshape(2 * len(homographies), 6)
    # prepara o vetorzao V com os coeficientes
    for i in range(0, len(V), 2):
        # print 'homographies[i / 2]\n', homographies[i / 2]
        # H = homographies[i / 2]
        # print H[0][0] * H[0][1]

        if NORMALIZE:
            V[i] = normalizeVector(getVij(homographies[i / 2], 0, 1).T)
            V[i + 1] = normalizeVector(getVij(homographies[i / 2], 0, 0).T - getVij(homographies[i / 2], 1, 1).T)
        else:
            V[i] = getVij(homographies[i / 2], 0, 1).T
            V[i + 1] = getVij(homographies[i / 2], 0, 0).T - getVij(homographies[i / 2], 1, 1).T

    # print V
    return closed_form_solution(V).T  # b = [B11, B12, B22, B13, B23, B33].T


def toMatrix3x3(b):
    B = np.array([[b[0], b[1], b[3]],
                  [b[1], b[2], b[4]],
                  [b[3], b[4], b[5]]])
    return B


def extractIntrinsicParams(B):
    K = np.zeros((3, 3))

    # B = B*-1

    den = (B[0][0] * B[1][1] - B[0][1] ** 2)
    # print 'B\n', B

    if abs(den) <= 1e-14:
        print '# Error # On extractIntrinsicParams-(1)(B[0][0] * B[1][1] - math.pow(B[0][1], 2)) =', den
        return None
    v0 = (B[0][1] * B[0][2] - B[0][0] * B[1][2]) / den

    if abs(B[0][0]) <= 1e-14:
        print '# Error # On extractIntrinsicParams-(2) B[0][0] =', B[0][0]
        return None
    lamb_scale = B[2][2] - (B[0][2] ** 2 + v0 * (B[0][1] * B[0][2] - B[0][0] * B[1][2])) / B[0][0]

    if lamb_scale / B[0][0] < 0:
        print '# Error # On extractIntrinsicParams-(3) (lamb_scale / B[0][0]) =', lamb_scale / B[0][0], 'L=', lamb_scale
        return None
    alfa = math.sqrt(lamb_scale / B[0][0])

    if lamb_scale * B[0][0] / den < 0:
        print '# Error # On extractIntrinsicParams-(4) (lamb_scale * B[0][0] / den) =', lamb_scale * B[0][0] / den
        return None
    beta = math.sqrt(lamb_scale * B[0][0] / den)

    gamma = - B[0][1] * (alfa ** 2) * beta / lamb_scale
    u0 = (gamma * v0 / beta) - (B[0][2] * (alfa ** 2) / lamb_scale)

    K[0][0] = alfa
    K[0][1] = 0  # gamma
    K[0][2] = u0

    K[1][1] = beta
    K[1][2] = v0

    K[2][2] = 1

    # print K

    return K


def getExtrinsicParams(homographies, A_coef, N):
    rvecs, tvecs = [], []

    A_coef_inv = np.linalg.inv(A_coef)
    Ninv = np.linalg.inv(N)
    # print camera_matrix_inv

    for i in range(0, len(homographies)):
        Hinv = np.dot(Ninv, homographies[i])

        lamb1 = 1 / np.linalg.norm(np.dot(A_coef_inv, Hinv[:, 0]))
        lamb2 = 1 / np.linalg.norm(np.dot(A_coef_inv, Hinv[:, 1]))
        # lamb3 = 1 / np.linalg.norm(np.dot(A_coef_inv, Hinv[:, 2]))
        lamb3 = (lamb1 + lamb2) / 2.0

        r1 = lamb1 * (np.dot(A_coef_inv, Hinv[:, 0]))
        r2 = lamb2 * (np.dot(A_coef_inv, Hinv[:, 1]))
        r3 = np.cross(r1, r2)  # / 2.0

        t = lamb3 * (np.dot(A_coef_inv, Hinv[:, 2]).reshape(3, 1))

        # K = np.column_stack((r1, r2, r3, t))
        # print 'K (R|t)\n', K

        R = np.column_stack((r1, r2, r3))
        # Corrigir ortogonalidade da matriz de rotacao
        u, p = scipy.linalg.polar(R)
        R_rodrigues, jac = cv2.Rodrigues(u)

        tvecs.append(t)
        rvecs.append(R_rodrigues)

    return rvecs, tvecs


def calibrateCamera(objectPoints, imagePoints, imageSize=(800, 400)):
    # Matriz de Normalizacao
    N = np.array([[2.0 / imageSize[0], 0, -1],
                  [0, 2.0 / imageSize[1], -1],
                  [0, 0, 1]])

    imagePointsNorm = normalizePoints(imagePoints, imageSize, N)

    '''
    # Calcular as Homografias
    '''
    homographies = getHomographies(objectPoints, imagePointsNorm)

    '''
    # Calcular matriz B
    '''
    b = calculate_b(homographies)
    B = toMatrix3x3(b)

    '''
    # Calcular matriz dos Coeficientes
    '''
    camera_matrix_norm = extractIntrinsicParams(B)

    if camera_matrix_norm is None:
        print 'camera_matrix is\n', camera_matrix_norm
        return [0] * 4

    # corrigindo normalizacao da matrix de coeficientes
    Ninv = np.linalg.inv(N)
    camera_matrix = np.dot(Ninv, camera_matrix_norm)

    '''
    # Calcular parametros Extrinsecos
    '''
    rvecs, tvecs = getExtrinsicParams(homographies, camera_matrix, N)

    '''
    # Otimizar
    '''
    rms, camera_matrix, dist_coefs, rvecs, tvecs = optimize_calibration(
        objectPoints, imagePoints, camera_matrix, rvecs, tvecs)

    return rms, camera_matrix, dist_coefs, rvecs, tvecs


####################################################
#                                                  #
#                      Main                        #
#                                                  #
####################################################

if __name__ == "__main__":

    # USE_MANUEL_DATA = True
    USE_MANUEL_DATA = False

    if USE_MANUEL_DATA:
        # Pegar os centros de cada imagem
        img_points, obj_points, pattern_size = load_data_manuel()
        h, w = 480, 640

    else:
        square_size = 23.0
        padrao, fonte, pattern_size, legenda = 'ring', '640x480', (10, 7), "Ring Sasha"
        images_paths, images, pattern = images_input.getImages(padrao, fonte)

        # dimensoes das imagens
        h, w = images[0].shape[:2]

        # posicao dos pontos de controle do padrao no plano z do mundo
        pattern_points = calibration_util.getPatternPointsInZ(pattern_size, square_size)

        # Pegar os centros de cada imagem
        img_points, obj_points = calibration_util.get_centros(images, images_paths, pattern, pattern_size,
                                                              pattern_points, find_again=False)

    img_points2 = copy.deepcopy(img_points)
    obj_points2 = copy.deepcopy(obj_points)

    '''
    Opencv
    Resolver a calibracao usando OpenCV
    '''
    rms, camera_matrix, dist_coefs, rvecs, tvecs = cv2.calibrateCamera(obj_points, img_points, (w, h))
    print '=============================================\n' \
          '                  Opencv'
    r = calibration_util.showResultadoCalibracao2(rms, camera_matrix, dist_coefs,
                                                  pattern_size, obj_points, img_points, rvecs, tvecs)

    '''
    Sasha
    Resolver a calibracao usando algoritmo meu implementado de Zhang
    '''
    rms2, camera_matrix2, dist_coefs2, rvecs2, tvecs2 = calibrateCamera(obj_points2, img_points2, (w, h))
    print '=============================================' \
          '\n              Sasha Nicolas'
    r = calibration_util.showResultadoCalibracao2(rms2, camera_matrix2, dist_coefs2,
                                                  pattern_size, obj_points2, img_points2, rvecs2, tvecs2)
