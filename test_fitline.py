import numpy as np
import matplotlib.pyplot as plt
from calibration_util import proj_points_min_dist


def proj_point_on_line(a, l):
    p0 = np.array([0, l[1]], dtype=np.float64)
    p1 = np.array([1, l[0] + l[1]], dtype=np.float64)

    a_ = a - p0

    p = p1 - p0
    p_ = p / np.linalg.norm(p)

    s = np.dot(a_, p_)

    return s * p_ + p0


# x = [190.47344971, 217.94929504, 246.54656982, 276.33532715, 307.3916626,
#      339.79821777, 373.64498901, 409.0302124, 446.06115723, 484.85540771]
# y = [112.7824173, 111.53439331, 110.23542786, 108.88234711, 107.47167969,
#      105.99968719, 104.46228027, 102.8549881, 101.17294312, 99.41080475]

# # exemplo ankur
# x = [258.34088135, 336.07525635, 410.59317017, 482.090271, 550.74658203,
#      616.72808838, 680.18811035, 741.26849365, 800.10064697, 856.80645752]
# y = [130.31184387, 135.68122864, 140.82846069, 145.76701355, 150.50935364,
#      155.06692505, 159.4503479, 163.66938782, 167.73313904, 171.65000916]
#
# # Pontos originais
# plt.scatter(x, y)
#
# res = np.polyfit(x, y, 1, full=True)
# print res
# # Linha
# line = np.poly1d(res[0])
# x = np.arange(150, 500)
# y = line(x)
# plt.plot(x, y)
#
# # Pontos projetados
# x = [258.34088135, 336.07525635, 410.59317017, 482.090271, 550.74658203,
#      616.72808838, 680.18811035, 741.26849365, 800.10064697, 856.80645752]
# y = [130.31184387, 135.68122864, 140.82846069, 145.76701355, 150.50935364,
#      155.06692505, 159.4503479, 163.66938782, 167.73313904, 171.65000916]
# print x[0], y[0]
# for i in range(len(x)):
#     x[i], y[i] = proj_point_on_line(np.array([x[i], y[i]], dtype=np.float64),
#                                     np.array([res[0][0], res[0][1]], dtype=np.float64))
#     # x[i], y[i] = proj_points_min_dist([x[i], y[i]], line, False)
# plt.plot(x, y, "*r")
# print x[0], y[0]
#
# axes = plt.gca()
# axes.set_xlim([0, 800])
# axes.set_ylim([0, 800])
# axes.set_aspect('equal', adjustable='box')
# plt.show()

x = [0, 100]
y = [0, 500]

plt.plot(x, y)

plt.plot([50], [90], ".r")
proj = proj_point_on_line(np.array([50, 90], dtype=np.float64), np.array([5, 0], dtype=np.float64))
plt.plot([proj[0]], [proj[1]], ".g")

axes = plt.gca()
axes.set_xlim([0, 200])
axes.set_ylim([0, 200])
axes.set_aspect('equal', adjustable='box')
axes.set_xlabel('xlabel')
plt.show()
