# =====================================================================================================================
# Mostrar mensagem indicando quais imagens abrindo
# =====================================================================================================================
SHOW_PARCIAL_CALIBRATION_RESULT = True
SHOW_OPTIMIZING_RESULT = True

SHOW_MESSAGE_ABRINDO_IMAGENS = True
SHOW_MESSAGE_FOUND_CENTERS = False
SHOW_ROTATION_TRANSLATION_VETORS = False

# =====================================================================================================================
# Adicionar numero que indica a ordem do ponto de controle
# =====================================================================================================================
ADICIONA_NUMERO_CENTRO = False

# =====================================================================================================================
# Flags para mostrar imagens com centros
# =====================================================================================================================
MOSTRA_CENTROS = False
MOSTRA_REPROJECAO = False
MOSTRA_UNDISTORTED = False
MOSTRA_FRONTO_PARALELO = False
MOSTRA_NOVOS_FRONTO = False
MOSTRA_NOVOS_PROJ = False
MOSTRA_NOVOS_DISTORTED = True

# =====================================================================================================================
# Debug imagens
# =====================================================================================================================
DEBUG_CONTORNOS_ALL = False
DEBUG_CONTORNOS_EACH = False
FUNCAO_MOSTRA_IMAGEM_ENABLED = True   # ativar a funcao mostra_imagem

# =====================================================================================================================
# Flags para configurar execucao
# =====================================================================================================================

USE_PRAKASH_THRESHOLD_ADAP = True
OPTIMIZE_WITH_COLLINEARITY = False

OPTIMIZE_ALL = False

OTM_FIX_PRIN_OPENCV = True
USE_INTRINSIC_GUESS = True

OTM_FIX_INT = False
OTM_FIX_DIST = False
OTM_FIX_EXT = False

# =====================================================================================================================
# Flags para configurar display e valores dos grafico
# =====================================================================================================================
NORMALIZE_RMS = False
ADD_FIRST_OPENCV_RESULT_TO_CHART = True  # quando gerar o grafico

