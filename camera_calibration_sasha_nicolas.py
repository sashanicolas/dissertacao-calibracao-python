# -*- coding: utf-8 -*-
############################################################################
#
#  Camera Calibration
#  Author: Sasha Nicolas. Tecgraf 2016. Puc-Rio
#  sasha@tecgraf.puc-rio.br
#
#  Thesis project for PUC-Rio Informatics Master's Program
#
#  Implementation based on methods of: Z. Zhang, Ankur Datta, and Prakash.
#  Additional of Ring Pattern detection.
#  New: Optimizing regarding collinearity error
#
############################################################################

import images_input
import time

try:
    import matplotlib.pyplot as plt
except ImportError:
    plt = None
import xlsxwriter
from my_own_calibration_methods import calibrateCamera
from calibration_util import *
import optimizing

__author__ = 'Sasha Nicolas'


####################################################
#                                                  #
#              Grafico e Prints                    #
#                                                  #
####################################################


def printConfig():
    print 'USE_PRAKASH_THRESHOLD_ADAP = ',
    print 'On' if settings_flags.USE_PRAKASH_THRESHOLD_ADAP else 'Off'

    print 'OPTIMIZE_WITH_COLLINEARITY = ',
    print 'On' if settings_flags.OPTIMIZE_WITH_COLLINEARITY else 'Off'


def createExcelFile(name, resultados_rms, legendas):
    if len(resultados_rms) < 1:
        return
    if len(resultados_rms[0]) < 1:
        return

    # Create a workbook and add a worksheet.
    workbook = xlsxwriter.Workbook(name + '.xlsx')
    worksheet = workbook.add_worksheet()
    worksheet.set_column('A:E', 15)

    for i in xrange(len(resultados_rms)):
        worksheet.write(0, i, legendas[i])
        for j in xrange(len(resultados_rms[i])):
            worksheet.write(j + 1, i, resultados_rms[i][j])

    # Grafico para todos juntos
    chart = workbook.add_chart({'type': 'line'})

    for i in xrange(len(resultados_rms)):
        col = chr(ord('A') + i)
        end = str(len(resultados_rms[i]) + 1)
        # Add a series to the chart.
        chart.add_series({'values': '=Sheet1!$' + col + '$2:$' + col + '$' + end,
                          'name': legendas[i],
                          'marker': {'type': 'diamond'}})

    # Insert the chart into the worksheet.
    worksheet.insert_chart(len(resultados_rms[0]) + 5, 0, chart)

    # Graficos separados para cada set de imagens
    for i in xrange(len(resultados_rms)):
        chart = workbook.add_chart({'type': 'line'})
        col = chr(ord('A') + i)
        end = str(len(resultados_rms[i]) + 1)
        # Add a series to the chart.
        chart.add_series({'values': '=Sheet1!$' + col + '$2:$' + col + '$' + end,
                          'name': legendas[i],
                          'marker': {'type': 'diamond'}})
        worksheet.insert_chart(len(resultados_rms[0]) + 5 + (i + 1) * 15, 0, chart)

    workbook.close()


def createExcelFileCompare(name, resultados_rms, legendas):
    if len(resultados_rms) < 1:
        return
    if len(resultados_rms[0]) < 1:
        return

    # Create a workbook and add a worksheet.
    workbook = xlsxwriter.Workbook(name + '.xlsx')
    worksheet = workbook.add_worksheet()
    worksheet.set_column('A:E', 15)

    # p/cada rms escreve a legenda e os valores
    for i in xrange(len(resultados_rms)):
        worksheet.write(0, i, legendas[i])
        for j in xrange(len(resultados_rms[i])):
            worksheet.write(j + 1, i, resultados_rms[i][j])

    # Graficos separados para comparar com e sem prak no mesmo grafico e mesmo set
    for i in range(0, len(resultados_rms), 2):
        chart = workbook.add_chart({'type': 'line'})
        # adiciona valores para com prak
        col = chr(ord('A') + i)
        end = str(len(resultados_rms[i]) + 1)
        chart.add_series({'values': '=Sheet1!$' + col + '$2:$' + col + '$' + end,
                          'name': legendas[i],
                          'marker': {'type': 'diamond'}})

        # adiciona valores para sem prak
        col = chr(ord('A') + i + 1)
        end = str(len(resultados_rms[i + 1]) + 1)
        chart.add_series({'values': '=Sheet1!$' + col + '$2:$' + col + '$' + end,
                          'name': legendas[i + 1],
                          'marker': {'type': 'diamond'}})

        worksheet.insert_chart(len(resultados_rms[0]) + 5 + (i + 1) * 15, 0, chart)

    workbook.close()


def geraGraficoPython(data, legendas, fileName=None, title="RMS", config_text=None, data2=None,
                      annotatePoint=None, annotatePoint2=None):
    # se importou a matplotlib
    if plt is None:
        return

    plt.close('all')
    if data2 is None:
        fig = plt.figure(figsize=(8, 6), dpi=100)
    else:
        fig = plt.figure(figsize=(12, 6), dpi=80)

    ax2 = None
    if config_text is not None:
        # se eh p colocar legenda da config
        if data2 is not None:
            # dois graficos
            ax = fig.add_axes((0.05, 0.3, 0.4, 0.6))
            ax.set_xlabel(u'Iterações')
            ax2 = fig.add_axes((0.55, 0.3, 0.4, 0.6))
            ax2.set_xlabel(u'Iterações')
        else:
            # soh um grafico
            ax = fig.add_axes((0.1, 0.3, 0.8, 0.6))
            ax.set_xlabel(u'Iterações')
    else:
        if data2 is not None:
            # dois graficos
            ax = plt.subplot(121)
            ax.set_xlabel(u"Iterações")
            ax2 = plt.subplot(122)
            ax2.set_title("Erro de Colinearidade")
            ax2.set_xlabel(u"Iterações")
        else:
            # soh um grafico
            ax = plt.subplot(111)
            ax.set_xlabel(u'Iterações')

    ax.set_title(title)
    ax.ticklabel_format(useOffset=False)
    x = np.arange(len(data[0]))
    for i in xrange(len(data)):
        ax.plot(x, data[i], label=legendas[i])

    # annotate
    if annotatePoint is not None:
        ax.annotate("Minimum", xy=annotatePoint, xycoords='data', xytext=(0, 30), textcoords='offset points',
                    fontsize=16, arrowprops=dict(arrowstyle='->', connectionstyle='arc3, rad=.2'))
    if annotatePoint2 is not None:
        ax2.annotate("Minimum", xy=annotatePoint2, xycoords='data', xytext=(0, 30), textcoords='offset points',
                     fontsize=16, arrowprops=dict(arrowstyle='->', connectionstyle='arc3, rad=.2'))

    if data2 is not None:
        ax2.set_title("Erro de Colinearidade")
        ax2.ticklabel_format(useOffset=False)
        for i in xrange(len(data2)):
            ax2.plot(np.arange(0, len(data2[i])), data2[i])

    # Shrink current axis by 20%
    # box = ax.get_position()
    # ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

    # Put a legend to the right of the current axis
    # ax.legend(loc='center left', bbox_to_anchor=(1.2, -0.2), ncol=4, fontsize=9)
    # ax.legend(ncol=3, fontsize=9)
    if config_text is not None:
        ax.legend(loc='center left', bbox_to_anchor=(1.2, -0.2), ncol=4, fontsize=9)
    else:
        ax.legend()

    if fileName is not None:
        if config_text is not None:
            fig.text(.1, .1, config_text)
        plt.savefig(fileName + '.png', dpi=200)
        plt.savefig(fileName + '.eps', format='eps', dpi=1000)
    else:
        plt.show()

    # end geraGrafico()
    pass


####################################################
#                                                  #
#                  calibrate                       #
#                                                  #
####################################################


def calibrate(images_paths, original_images, pattern, pattern_size, circulo_anel_distancia=23.0):
    '''
    Calibrar Camera
    :param images_paths: caminhos das imagens
    :param original_images: imagens
    :param pattern: tipo do padrao 'circle' ou 'ring'
    :param pattern_size: dimensoes do padrao tipo (10, 5), maior primeiro
    :param circulo_anel_distancia: distancia em milimetros dos centros dos aneis/circulos
    :return: listas com resultados de cada iteracao rmss, rpes, colls
    '''
    global cloud_file
    '''
    Inicio
    '''
    # region Check inical e print
    if not images_paths or not original_images or not pattern:
        print 'Check parameters passed.'
        return

    print '\n===================================================================='
    print ' Camera Calibration - Sasha Nicolas - {}'.format(pattern)
    print '====================================================================\n'
    # endregion

    '''
    Achar os centros para cada imagem, seja por arquivo ou deteccao com Find Ring Class
    '''
    # region Extract centros
    # dimensoes das imagens
    h, w = original_images[0].shape[:2]
    print "Image dimensions: ", w, h

    # posicao dos pontos de controle do padrao no plano z do mundo
    pattern_points = getPatternPointsInZ(pattern_size, circulo_anel_distancia)

    # Pegar os centros de cada imagem
    img_points, obj_points = get_centros(
        original_images, images_paths, pattern, pattern_size, pattern_points, find_again=False)

    # verificar se o conjunto de imagens esta de acordo com o de pontos
    if len(img_points) != len(original_images):
        print '\n= ERROR = Centros iniciais nao foram achados by get_centros(). Halt'
        return None, None, None

    rmss, rpes, colls = [], [], []
    # endregion

    '''
    Calibracao inicial com centros detectados pelas elipses na imagem original
    '''
    # region Calibracao Inicial
    print 'Primeira calibracao Opencv iniciada.'

    flagFixPrin = cv2.CALIB_FIX_PRINCIPAL_POINT if settings_flags.OTM_FIX_PRIN_OPENCV else 0
    flagUseIntrinsicGuess = cv2.CALIB_USE_INTRINSIC_GUESS if settings_flags.USE_INTRINSIC_GUESS else 0

    # Resolver a calibracao dados os pontos ideais e os pontos nas imagens
    rms, camera_matrix, dist_coefs, rvecs, tvecs = cv2.calibrateCamera(
        obj_points, img_points, (w, h), None, None, flags=flagFixPrin)

    '''
    Resultado Inicial
    '''
    # mostrar resultado da calibracao e atualizar o rms
    rpe, coll = showResultadoCalibracao(
        pattern, rms, camera_matrix, dist_coefs, original_images, pattern_size, obj_points, img_points, rvecs, tvecs,
        cloud_file=cloud_file + "0" if cloud_file is not None else None)

    # rmss.append(rms)
    # rpes.append(rpe)
    # colls.append(coll)
    # endregion

    '''
    Loop - Refinamento iterativo
    '''
    # region Loop - Refinamento iterativo
    for it in range(1, n_iter + 1):
        print '\n       ### Iteracao ', it, '###'

        # Remover distorcao das imagens
        undistort_images, centros_undistorted = undistortImages(original_images, camera_matrix, dist_coefs, img_points)

        # Remover projecao levando para Fronto Paralelo
        unproject_images, centros_unprojected, homografias, dist = unprojectToFrontoParallel(
            pattern_size, undistort_images, centros_undistorted)

        # Achar centros no fronto paralelo
        novos_centros_fronto = find_centros_fronto_parallel(
            unproject_images, centros_unprojected, dist[0], pattern, pattern_size)

        # verificar se achou centros no fronto paralelo
        if novos_centros_fronto is None:
            print '\n ===== Nao achou centros no fronto paralelo. Next iteration. ====='
            rmss.append(rms)
            rpes.append(rpe)
            colls.append(coll)
            continue

        # Projetar e distorcer novamente
        img_points = apply_projection_and_distortion(
            original_images, undistort_images, novos_centros_fronto, centros_undistorted, img_points, homografias,
            camera_matrix, dist_coefs)

        # Calibracao com centros detectados na imagem fronto paralelo
        rms, camera_matrix, dist_coefs, rvecs, tvecs = cv2.calibrateCamera(
            obj_points, img_points, (w, h), camera_matrix, dist_coefs)  # ,
            #flags=flagFixPrin)  # |
            # flags=flagUseIntrinsicGuess)

        # if settings_flags.OPTIMIZE_ALL:
        #     import time
        #     s = time.time()
        #     rms, camera_matrix, dist_coefs, rvecs, tvecs = optimizing.optimize_calibration(
        #         obj_points, img_points, camera_matrix, rvecs, tvecs, dist_coefs)
        #     print "Optimization Ext/Int/Dist time: ", time.time() - s

        if settings_flags.OPTIMIZE_WITH_COLLINEARITY:
            import time
            s = time.time()
            rms, camera_matrix, dist_coefs, rvecs, tvecs = optimizing.optimize_calibration_coll_new(
                pattern_size, camera_matrix, dist_coefs, img_points, rvecs, tvecs, obj_points)
            print "Optimization Coll time: ", time.time() - s

        # Resultado intermediario da iteracao i
        rpe, coll = showResultadoCalibracao(
            pattern, rms, camera_matrix, dist_coefs, original_images, pattern_size, obj_points, img_points, rvecs,
            tvecs, it, fronto_points=novos_centros_fronto, undist_points=centros_undistorted,
            cloud_file=cloud_file + str(it) if cloud_file is not None else None)

        rmss.append(rms)
        rpes.append(rpe)
        colls.append(coll)

    # endregion

    '''
    Finalizacao
    '''
    # region Resultado iteracoes
    cv2.destroyAllWindows()
    print '================================'
    print pattern, ','  # , fonte, ',',

    if RUN_MODE is not "EXECUTE_RUN_CONFIGURATIONS":
        printConfig()

    print '================================'

    def printResultVector(name, vector):
        print name
        for idx, e in enumerate(vector):
            print "%02d)" % idx, e

    printResultVector('RMS', rmss)
    printResultVector('RPE', rpes)
    printResultVector('Colls', colls)

    return rmss, rpes, colls
    # endregion Resultado iteracoes

    # end def calibrate


####################################################
#                                                  #
#                   Variaveis                      #
#                                                  #
####################################################

# region Sets for Multiple Run
sets = [
    ['ring', '640x480', (10, 7), "Ring Sasha",
     'Off', 23.0],
    ['ring', 'kinect16', (10, 7), "Ring Kinect",
     'Off', 23.0],
    ['ring', 'ankur', (10, 7), "Ring Ankur",
     'On', 23.0],
    ['ring', 'video1', (7, 5), "Ring Video",
     'Off', 23.0],
    ['ring', 'video1_plus', (7, 5), "Ring Video Plus",
     'Off', 23.0],
    ['ring', 'new_manuel_data', (4, 3), "new_manuel_data",
     'Off', 66.92],
    ['ring', 'manuel_set2', (7, 5), "manuel_set2",
     'Off', 20.6],
    ['ring', 'gopro_set1', (10, 7), "gopro_set1",
     'Off', 22.0]
]
# endregion

####################################
# Configurar Modo de Execucao
####################################
# RUN_MODE = "SINGLE_SET"  # o utilizado sera o que nao tiver comentado na regiao de sets
# RUN_MODE = "ALL_SETS"
# RUN_MODE = "COMPARE_METHODS_COM_SEM_PRAK"
RUN_MODE = "COMPARE_METHODS_COM_SEM_flag"
# RUN_MODE = "EXECUTE_RUN_CONFIGURATIONS"

GRAPH_OUTPUT_DATA = 'RMS'
# GRAPH_OUTPUT_DATA = 'COLL'

n_iter = 15

cloud_file = None

####################################################
#                                                  #
#                      Main                        #
#                                                  #
####################################################

if __name__ == "__main__":

    if RUN_MODE == "SINGLE_SET":
        # region Sets For Single Run
        # padrao, fonte, pattern_size, legenda, circulo_dist = 'circle', '640x480', (10, 7), "Circle Sasha", 23.0
        # padrao, fonte, pattern_size, legenda, circulo_dist = 'circle', 'ankur', (10, 7), "Circle ankur", 23.0
        # padrao, fonte, pattern_size, legenda, circulo_dist = 'circle', 'kinect16', (10, 7), "Circle kinect16", 23.0

        # padrao, fonte, pattern_size, legenda, circulo_dist = 'ring', '640x480', (10, 7), "Ring Sasha", 23.0
        padrao, fonte, pattern_size, legenda, circulo_dist = 'ring', 'kinect16', (10, 7), "Ring kinect16", 23.0
        # padrao, fonte, pattern_size, legenda, circulo_dist = 'ring', 'ankur', (10, 7), "Ring ankur", 23.0
        # padrao, fonte, pattern_size, legenda, circulo_dist = 'ring', 'video1', (7, 5), "Ring video1", 23.0
        # padrao, fonte, pattern_size, legenda, circulo_dist = 'ring', 'video1_plus', (7, 5), "Ring video1_plus", 23.0
        # padrao, fonte, pattern_size, legenda, circulo_dist = 'ring', 'Lens_2f_63z', (4, 3), "Ring Lens_2f_63z", 57.7
        # padrao, fonte, pattern_size, legenda, circulo_dist = 'ring', 'Lens_0.4f_15z', (4, 3), "Ring Lens_0.4f_15z", 57.7
        # padrao, fonte, pattern_size, legenda, circulo_dist = 'ring', 'new_manuel_data', (4, 3), "new_manuel_data", 66.92
        # padrao, fonte, pattern_size, legenda, circulo_dist = 'ring', 'manuel_set2', (7, 5), "manuel_set2", 20.6
        # padrao, fonte, pattern_size, legenda, circulo_dist = 'ring', 'gopro_set1', (10, 7), 'gopro_set1', 22.0

        # endregion Sets

        # region RUN_SINGLE_SET
        images_paths, images, pattern = images_input.getImages(padrao, fonte)

        if len(images) >= 1:
            resultados_rms = []
            legendas = []

            rmss, rpes, colls = calibrate(images_paths, images, pattern, pattern_size, circulo_dist)

            if rmss is not None:
                min = np.min(rmss)
                variancia = np.max(rmss) - min
                if settings_flags.NORMALIZE_RMS:
                    resultados_rms.append((rmss - min) / variancia)
                else:
                    resultados_rms.append(rmss)
                legendas.append(legenda)
                geraGraficoPython(data=[rmss], legendas=legendas, data2=[colls], fileName="resultado_linux",
                                  annotatePoint=[rmss.index(np.min(rmss)), np.min(rmss)],
                                  annotatePoint2=[colls.index(np.min(colls)), np.min(colls)])
        else:
            print "No images loaded."
        pass
        # endregion

    elif RUN_MODE == "ALL_SETS":
        # region RUN_ALL_SETS
        # rodar todos os conjuntos de uma vez, mostra grafico no final

        resultados_data = []
        legendas = []

        start = time.time()
        for i in range(0, len(sets)):
            # pular o set que tenha off
            if sets[i][4] == 'Off':
                continue

            print '\n===================================================================='
            print ' Set de imagem ', sets[i]
            print '====================================================================\n'

            # carregar imagens
            images_paths, images, pattern = images_input.getImages(sets[i][0], sets[i][1])
            fonte = sets[i][1]
            if len(images) >= 1:
                pattern_size = sets[i][2]
                rmss, rpes, colls = calibrate(images_paths, images, pattern, pattern_size)

                if rmss is not None:
                    if GRAPH_OUTPUT_DATA == "RMS":
                        min = np.min(rmss)
                        resultados_data.append(rmss - min)

                    elif GRAPH_OUTPUT_DATA == "COLL":
                        min = np.min(colls)
                        # variancia = np.max(colls) - min
                        resultados_data.append(colls - min)
                    legendas.append(sets[i][3])

                # geraGraficoPython([resultados_rms[i]], legendas)
                pass
            else:
                print "No images."

            # geraGraficoPython(resultados_rms, legendas)
            pass
        # createExcelFile('run_all', resultados_rms, legendas)
        tempo = time.time() - start
        print 'Fim da execucao. Tempo total: ', int(tempo) / 60, 'min', int(tempo) % 60, 's'
        # endregion
        geraGraficoPython(resultados_data / np.max(resultados_data), legendas)

    elif RUN_MODE == "COMPARE_METHODS_COM_SEM_PRAK":
        # region RUN_COMPARE_METHODS
        resultados_rms = []
        legendas = []
        for i in range(0, len(sets)):
            # pular o set que tenha off
            if sets[i][4] == 'Off':
                continue

            print '\n===================================================================='
            print ' Set de imagem ', sets[i]
            print '====================================================================\n'

            # carregar imagens
            images_paths, images, pattern = images_input.getImages(sets[i][0], sets[i][1])
            fonte = sets[i][1]
            if len(images) >= 1:
                pattern_size = sets[i][2]

                # calibra com refinamento do Prakash
                settings_flags.USE_PRAKASH_THRESHOLD_ADAP = True
                rmss, rpes, colls = calibrate(images_paths, images, pattern, pattern_size)

                if rmss is not None:
                    resultados_rms.append(rmss)
                    legendas.append("(Com Prak) " + sets[i][3])

                # calibra sem refinamento do Prakash
                settings_flags.USE_PRAKASH_THRESHOLD_ADAP = False
                rmss2, rpes2, colls2 = calibrate(images_paths, images, pattern, pattern_size)

                if rmss2 is not None:
                    resultados_rms.append(rmss2)
                    legendas.append("(Sem Prak) " + sets[i][3])

                geraGraficoPython(resultados_rms, legendas)
                pass
            else:
                print "No images."

        # createExcelFileCompare('compare_com_sem_prak', resultados_rms, legendas)

        print 'Fim da execucao.'
        # endregion

    elif RUN_MODE == "COMPARE_METHODS_COM_SEM_flag":
        # region COMPARE_METHODS_COM_SEM_OPTIMIZATION
        for i in range(0, len(sets)):
            # pular o set que tenha off
            if sets[i][4] == 'Off':
                continue
            resultados_rms = []
            resultados_colls = []
            legendas = []
            print '\n===================================================================='
            print ' Set de imagem ', sets[i]
            print '====================================================================\n'

            # carregar imagens
            images_paths, images, pattern = images_input.getImages(sets[i][0], sets[i][1])
            fonte = sets[i][1]
            if len(images) >= 1:
                pattern_size = sets[i][2]

                # calibra com refinamento do Prakash
                settings_flags.OPTIMIZE_WITH_COLLINEARITY = False
                cloud_file = "cloud_charts/opt_off_"
                rmss, rpes, colls = calibrate(images_paths, images, pattern, pattern_size)
                if rmss is not None:
                    resultados_rms.append(rmss)
                    resultados_colls.append(colls)
                    legendas.append(u"Sem otimização colinearidade")  # + sets[i][3])

                # calibra sem refinamento do Prakash
                settings_flags.OPTIMIZE_WITH_COLLINEARITY = True
                cloud_file = "cloud_charts/opt_on_"
                rmss2, rpes2, colls2 = calibrate(images_paths, images, pattern, pattern_size)
                if rmss2 is not None:
                    resultados_rms.append(rmss2)
                    resultados_colls.append(colls2)
                    legendas.append(u"Com otimização colinearidade")  # + sets[i][3])

                # geraGraficoPython(data=resultados_rms, legendas=legendas, title="Com/Sem Otimizacao")
                geraGraficoPython(data=resultados_rms, legendas=legendas,  # data2=resultados_colls,
                                  title="RMS", fileName="resultado_linux_" + str(i))
                pass
            else:
                print "No images."

        # createExcelFileCompare('compare_com_sem_prak', resultados_rms, legendas)

        print 'Fim da execucao.'
        # endregion

    elif RUN_MODE == "EXECUTE_RUN_CONFIGURATIONS":
        # region EXECUTE_RUN_CONFIGURATIONS
        import sys
        from run_configs import set_flags_config, flags, skip_config

        # percorre todos os sets
        for i in range(0, len(sets)):
            # pular o set que tenha off
            if sets[i][4] == 'Off':
                continue

            ##########################
            #    Carregar imagens    #
            ##########################

            images_paths, images, pattern = images_input.getImages(sets[i][0], sets[i][1])
            fonte = sets[i][1]

            print 'Set de imagem ', sets[i]
            start = time.time()

            if len(images) >= 1:
                pattern_size = sets[i][2]

                resultados_rms = []
                resultados_colls = []
                legendas = []
                menor_rms = {'config': 0, 'valor': sys.float_info.max, 'iter': 0}
                menor_coll = {'config': 0, 'valor': sys.float_info.max, 'iter': 0}

                #############################
                #  Executar Configurations  #
                #############################

                for k in range(0, 2 ** len(flags)):
                    config_text = set_flags_config(k)
                    print 'Config ', k,
                    if skip_config():
                        print 'skipped'
                        continue

                    # prepara fileName e tempo inicio
                    fileName = sets[i][1] + '_config_' + str(k)
                    start_parc = time.time()

                    # troca saida default
                    orig_stdout = sys.stdout
                    f = file('resultados/all_executions/' + fileName + '.txt', 'w')
                    sys.stdout = f

                    # prints iniciais
                    print 'Config ', k
                    printConfig()
                    print '\n===================================================================='
                    print ' Set de imagem ', sets[i]
                    print '====================================================================\n'

                    # calibra
                    rmss, rpes, colls = calibrate(images_paths, images, pattern, pattern_size)

                    # arruma resultado
                    if rmss is not None and len(rmss) is n_iter + 1:
                        resultados_rms.append(rmss)
                        resultados_colls.append(colls)
                        legendas.append("Config " + str(k))

                        if np.min(rmss) < menor_rms['valor']:
                            menor_rms['valor'] = np.min(rmss)
                            menor_rms['iter'] = rmss.index(np.min(rmss))
                            menor_rms['config'] = k

                        if np.min(colls) < menor_coll['valor']:
                            menor_coll['valor'] = np.min(colls)
                            menor_coll['iter'] = colls.index(np.min(colls))
                            menor_coll['config'] = k

                        geraGraficoPython(data=[rmss], legendas=[sets[i][3]], config_text=config_text, data2=[colls],
                                          fileName='resultados/all_executions/' + fileName,
                                          annotatePoint=[rmss.index(np.min(rmss)), np.min(rmss)],
                                          annotatePoint2=[colls.index(np.min(colls)), np.min(colls)])

                    # recupera saida
                    sys.stdout = orig_stdout
                    f.close()

                    # tempo
                    tempo_parc = time.time() - start_parc
                    print 'Tempo parcial: ', int(tempo_parc) / 60, 'min', int(tempo_parc) % 60, 's'

                # gera gfaficos de todos
                winner_txt = 'RMS Winner: config ' + str(menor_rms['config']) + \
                             '        Collinearity Winner: config ' + str(menor_coll['config'])
                if menor_rms['config'] == menor_coll['config']:
                    winner_txt += "\n" + set_flags_config(menor_coll['config'])

                geraGraficoPython(data=resultados_rms, legendas=legendas, data2=resultados_colls,
                                  fileName='resultados/all_executions/' + str(sets[i][1]) + "_all",
                                  annotatePoint=[menor_rms['iter'], menor_rms['valor']],
                                  annotatePoint2=[menor_coll['iter'], menor_coll['valor']],
                                  config_text=winner_txt)
            else:
                print "No images."

            # tempo total
            tempo = time.time() - start
            print '\nTempo total: ', int(tempo) / 60, 'min', int(tempo) % 60, 's'

        print 'Fim da execucao.'
        # endregion
