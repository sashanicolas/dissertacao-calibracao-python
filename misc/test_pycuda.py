import pycuda.autoinit
import pycuda.driver as drv
import numpy

from pycuda.compiler import SourceModule
# mod = SourceModule("""
# __global__ void multiply_them(float *dest, float *a, float *b)
# {
#   const int i = threadIdx.x;
#   dest[i] = a[i] * b[i];
# }
# """)
#
# multiply_them = mod.get_function("multiply_them")
#
# a = numpy.random.randn(400).astype(numpy.float32)
# b = numpy.random.randn(400).astype(numpy.float32)
#
# dest = numpy.zeros_like(a)
# multiply_them(
#         drv.Out(dest), drv.In(a), drv.In(b),
#         block=(400,1,1), grid=(1,1))
#
# print dest-a*b

# import pycuda.driver as cuda
# import pycuda.autoinit
# from pycuda.compiler import SourceModule
#
# import numpy as np
#
# a = np.random.randn(4, 4)
# a = a.astype(np.float32)
#
# a_gpu = cuda.mem_alloc(a.nbytes)
# cuda.memcpy_htod(a_gpu, a)
#
# mod = SourceModule("""
#   __global__ void doublify(float *a)
#   {
#     //int idx = threadIdx.x + threadIdx.y*4;
#     //a[idx] *= 2;
#   }
#   """)
#
# func = mod.get_function("doublify")
# func(a_gpu, block=(4, 4, 1))
#
# a_doubled = np.empty_like(a)
# cuda.memcpy_dtoh(a_doubled, a_gpu)
# print a_doubled
# print a

import pycuda.gpuarray as gpuarray
import pycuda.driver as cuda
import pycuda.autoinit
import numpy

a_gpu = gpuarray.to_gpu(numpy.random.randn(4,4).astype(numpy.float32))
a_doubled = (2*a_gpu).get()
print a_doubled
print a_gpu