import xlsxwriter

# fileName = '../resultados/old_all_execution_results/prak_coll_fixPrin_guess_90x/640x480_config_0.txt'
# fileName = '../resultados/all_executions/640x480_config_0.txt'


def get_values(file, size):
    values = []
    for i in range(0, size):
        line = file.readline()
        tokens = line.split(' ')
        # print str(i) + ']', float(tokens[1])
        values.append(float(tokens[1]))
    return values


def get_rmss_colls(fileName, size):
    rmss, colls = [], []
    with open(fileName) as file:
        # Encontra RMS
        while True:
            last_pos = file.tell()
            line = file.readline()
            if not line:
                break
            if '00)' in line[:3]:
                file.seek(last_pos)
                rmss = get_values(file, size)
                break

        # Encontra Coll
        while True:
            line = file.readline()
            if not line:
                break
            if 'Colls' in line:
                colls = get_values(file, size)
                break

    return rmss, colls


def createExcelFile(name, resultados_rms, legendas):
    if len(resultados_rms) < 1:
        return
    if len(resultados_rms[0]) < 1:
        return

    # Create a workbook and add a worksheet.
    workbook = xlsxwriter.Workbook(name + '.xlsx')
    worksheet = workbook.add_worksheet()
    worksheet.set_column('A:E', 15)

    for i in xrange(len(resultados_rms)):
        worksheet.write(0, i, legendas[i])
        for j in xrange(len(resultados_rms[i])):
            worksheet.write(j + 1, i, resultados_rms[i][j])

    # Grafico para todos juntos
    chart = workbook.add_chart({'type': 'line'})

    for i in xrange(len(resultados_rms)):
        col = chr(ord('A') + i)
        end = str(len(resultados_rms[i]) + 1)
        # Add a series to the chart.
        chart.add_series({'values': '=Sheet1!$' + col + '$2:$' + col + '$' + end,
                          'name': legendas[i],
                          'marker': {'type': 'diamond'}})

    # Insert the chart into the worksheet.
    worksheet.insert_chart(len(resultados_rms[0]) + 5, 0, chart)

    # Graficos separados para cada set de imagens
    for i in xrange(len(resultados_rms)):
        chart = workbook.add_chart({'type': 'line'})
        col = chr(ord('A') + i)
        end = str(len(resultados_rms[i]) + 1)
        # Add a series to the chart.
        chart.add_series({'values': '=Sheet1!$' + col + '$2:$' + col + '$' + end,
                          'name': legendas[i],
                          'marker': {'type': 'diamond'}})
        worksheet.insert_chart(len(resultados_rms[0]) + 5 + (i + 1) * 15, 0, chart)

    workbook.close()


def createExcelFileCompare(name, resultados_rms, legendas):
    if len(resultados_rms) < 1:
        return
    if len(resultados_rms[0]) < 1:
        return

    # Create a workbook and add a worksheet.
    workbook = xlsxwriter.Workbook(name + '.xlsx')
    worksheet = workbook.add_worksheet()
    worksheet.set_column('A:E', 15)

    # p/cada rms escreve a legenda e os valores
    for i in xrange(len(resultados_rms)):
        worksheet.write(0, i, legendas[i])
        for j in xrange(len(resultados_rms[i])):
            worksheet.write(j + 1, i, resultados_rms[i][j])

    # Graficos separados para comparar com e sem prak no mesmo grafico e mesmo set
    for i in range(0, len(resultados_rms), 2):
        chart = workbook.add_chart({'type': 'line'})
        # adiciona valores para com prak
        col = chr(ord('A') + i)
        end = str(len(resultados_rms[i]) + 1)
        chart.add_series({'values': '=Sheet1!$' + col + '$2:$' + col + '$' + end,
                          'name': legendas[i],
                          'marker': {'type': 'diamond'}})

        # adiciona valores para sem prak
        col = chr(ord('A') + i + 1)
        end = str(len(resultados_rms[i + 1]) + 1)
        chart.add_series({'values': '=Sheet1!$' + col + '$2:$' + col + '$' + end,
                          'name': legendas[i + 1],
                          'marker': {'type': 'diamond'}})

        worksheet.insert_chart(len(resultados_rms[0]) + 5 + (i + 1) * 15, 0, chart)

    workbook.close()


if __name__ == '__main__':
    configs = 16
    iterations = 91
    # folder = '../resultados/all_executions/'
    folder = '/Users/sashanicolas/Dropbox/dissertacao_all_execution/prak_coll_fixPrin_guess_90x/'
    radicais = [
        '640x480',
        'ankur',
        'kinect16',
        'video1',
        'video1_plus',
        'new_manuel_data',
        'manuel_set2'
    ]
    saveFolder = '../resultados/excel_analise/'

    for i in range(0, len(radicais)):
        rmss, colls, leg = [], [], []
        for config in range(0, configs):
            fileName = folder + radicais[i] + '_config_' + str(config) + '.txt'
            print fileName
            rms, coll = get_rmss_colls(fileName, iterations)
            rmss.append(rms)
            colls.append(coll)
            leg.append('Config ' + str(config))

        createExcelFile(saveFolder + radicais[i], rmss, leg)
