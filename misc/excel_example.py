import xlsxwriter

# Create a workbook and add a worksheet.
workbook = xlsxwriter.Workbook('Expenses01.xlsx')
worksheet = workbook.add_worksheet()

# Some data we want to write to the worksheet.
expenses = (
    ['Rent', 4.5],
    ['Gas', 100.3],
    ['Food', 300.4],
    ['Gym', 50.5],
)

# Start from the first cell. Rows and columns are zero indexed.
row = 0
col = 0

# Iterate over the data and write it out row by row.
for item, cost in (expenses):
    worksheet.write(row, col, item)
    worksheet.write(row, col + 1, cost)
    row += 1

# Write a total using a formula.
worksheet.write(row, 0, 'Total')
worksheet.write(row, 1, '=SUM(B1:B4)')

workbook.close()


def createExcelFile(name, resultados_rms, legendas):
    # Create a workbook and add a worksheet.
    workbook = xlsxwriter.Workbook(name + '.xlsx')
    worksheet = workbook.add_worksheet()

    for i in xrange(len(resultados_rms)):
        # ax.plot(x, data[i], label=legendas[i])
        worksheet.write(0, i, legendas[i])
        for j in xrange(len(resultados_rms[i])):
            worksheet.write(j+1, i, resultados_rms[i][j])

    workbook.close()