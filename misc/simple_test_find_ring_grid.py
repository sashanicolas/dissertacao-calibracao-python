__author__ = 'sasha'

import cv2 as cv2
import numpy as np
from matplotlib import pyplot as plt


#
# def desenhaCruz(x, y, color, img):
#     x = int(x)
#     y = int(y)
#     cv2.line(img, (x - 5, y), (x + 5, y), color, 1, 8)
#     cv2.line(img, (x, y - 5), (x, y + 5), color, 1, 8)


def mostraImagem(name, image, x=150, y=150, keepActiveAndContinue=False):
    cv2.namedWindow(name)
    cv2.imshow(name, image)
    cv2.moveWindow(name, x, y)

    if not keepActiveAndContinue:
        cv2.waitKey(0)
        cv2.destroyWindow(name)


def desenhaCruz(x, y, color, img=None):
    global image_aux

    x = int(x)
    y = int(y)
    if img is not None:
        cv2.line(img, (x - 5, y), (x + 5, y), color, 1, 8)
        cv2.line(img, (x, y - 5), (x, y + 5), color, 1, 8)
    else:
        cv2.line(image_aux, (x - 5, y), (x + 5, y), color, 1, 8)
        cv2.line(image_aux, (x, y - 5), (x, y + 5), color, 1, 8)


# variaveia para auxiliar a selecao dos 4 cantos
windowSelect4Corners = 'Selecione 4 quantos'
cornersCount = 0
cornersPoints = []
image_aux = []


def calculaGridROI(cornersPoints, pattern_size):
    """ Calcula os pontos que definem o grid em torno do padrao
    Retorna uma matriz (nx2) - representam coordenadas cartesianas em float
    """

    planePoints = np.array([(0, 0), (1, 0), (1, 1), (0, 1)], dtype='float64')
    H = cv2.findHomography(planePoints, np.array(cornersPoints, dtype='float64'))
    H = H[0]

    # if MOSTRA_GRID:
    # print "H = ", H

    nHorizontal = pattern_size[0]
    nVertical = pattern_size[1]

    quantPontosGrid = (nHorizontal + 1) * (nVertical + 1)

    pontosGridPlano = np.zeros((quantPontosGrid, 3), dtype=np.float64)

    # calculando pontos no grid ideal
    k = 0
    for i in range(0, nHorizontal + 1):
        x = i / float(nHorizontal)
        for j in range(0, nVertical + 1):
            y = j / float(nVertical)
            pontosGridPlano[k] = (x, y, 1)
            k += 1

    pontosGridPlano = np.transpose(pontosGridPlano)

    # if MOSTRA_GRID:
    #    print "pontosGridPlano = ", pontosGridPlano

    # multiplica pela homographia
    pontosGridImagem = np.dot(H, pontosGridPlano)

    pontos2dImagem = np.array(pontosGridImagem[[0, 1], :] / pontosGridImagem[2, :])
    pontos2dImagem = np.transpose(pontos2dImagem)

    return pontos2dImagem


def onmouse(event, x, y, flags, param):
    global windowSelect4Corners
    global image_aux
    global cornersCount

    if event == cv2.EVENT_LBUTTONUP:
        desenhaCruz(x, y, (255, 0, 0))
        cv2.imshow(windowSelect4Corners, image_aux)
        print x, y
        cornersPoints.append((x, y))
        cornersCount += 1


def findRind(img, (i, j), debug=False):
    imgGray = img.copy()
    imgGray = cv2.cvtColor(imgGray, cv2.COLOR_BGR2GRAY)

    blur = cv2.GaussianBlur(imgGray, (5, 5), 0)
    edges = cv2.Canny(blur, 20, 200)

    contornos, hierarquia = cv2.findContours(edges, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    if debug:
        for i in range(0, len(contornos)):
            img_deb = imgGray.copy()
            cv2.drawContours(img_deb, contornos, i, (255, 255, 255))

    x = 0
    y = 0

    for it in range(0, len(contornos)):
        if len(contornos[it]) > 5:
            el = cv2.fitEllipse(contornos[it])
            x += el[0][0]
            y += el[0][1]

    cx = x / float(len(contornos))
    cy = y / float(len(contornos))

    return [cx, cy], hierarquia


def removeOnMouse(event, x, y, flags, param):
    pass


def _ind(i, j, nVertical):
    return j * (nVertical + 1) + i


def getMenorPonto(p):
    c = [999999999, 999999999]
    for i in range(0, 4):
        if p[i][0] < c[0]: c[0] = p[i][0]
        if p[i][1] < c[1]: c[1] = p[i][1]

    return c


def getMaiorPonto(p):
    c = [0, 0]
    for i in range(0, 4):
        if (p[i][0] > c[0]): c[0] = p[i][0]
        if (p[i][1] > c[1]): c[1] = p[i][1]

    return c


def findRingGrid(img, pattern_size, image_path):
    global cornersCount
    global windowSelect4Corners
    global image_aux
    global cornersPoints

    imgGray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    centers = np.ones((pattern_size[0] * pattern_size[1], 1, 2), dtype='float32')

    cornersPoints = [(61, 63), (485, 58), (494, 279), (65, 283)]

    # criar grid
    image_aux = imgGray.copy()
    pontosDoGrid = calculaGridROI(cornersPoints, pattern_size)
    # converter pontos do grid para inteiro
    for i in range(0, len(pontosDoGrid)):
        pontosDoGrid[i][0] = int(pontosDoGrid[i][0])
        pontosDoGrid[i][1] = int(pontosDoGrid[i][1])

    # para cada area do grid achar o anel (duas circunferencias) e o centro adicina em centers
    nHorizontal = pattern_size[0]
    nVertical = pattern_size[1]

    # percorre o grid
    ind = 0
    bias = 3  # aumentar a celula do grid para pesquisa
    for i in range(0, nVertical):
        for j in range(0, nHorizontal):
            p = [pontosDoGrid[_ind(i, j, nVertical)],
                 pontosDoGrid[_ind(i, j + 1, nVertical)],
                 pontosDoGrid[_ind(i + 1, j + 1, nVertical)],
                 pontosDoGrid[_ind(i + 1, j, nVertical)]]
            c1 = getMenorPonto(p)
            c2 = getMaiorPonto(p)

            c1[0] -= bias
            c1[1] -= bias
            c2[0] += bias
            c2[1] += bias

            cell = imgGray[int(c1[1]):int(c2[1]), int(c1[0]):int(c2[0])]
            # mostra_imagem('cell', cell, 800, 150)

            # blur e canny
            blur = cv2.GaussianBlur(cell, (5, 5), 0)
            edges = cv2.Canny(blur, 100, 200)
            # mostra_imagem('edges', edges, 800, 150)
            # retval, cellbw = cv2.threshold(cell, thresh, 255, cv2.THRESH_BINARY)
            contornos, hierarquia = cv2.findContours(edges, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

            # mostra_imagem('edges', edges, 800, 150)

            idCentral = -1
            for it in range(0, len(contornos)):
                if len(contornos[it]) > 5:
                    temFilho = hierarquia[0][it][2] > -1
                    temPai = hierarquia[0][it][3] >= 0
                    if not temFilho and temPai:
                        pai = hierarquia[0][it][3]
                        temPai = hierarquia[0][pai][3] >= 0
                        if temPai:
                            pai = hierarquia[0][pai][3]
                            temPai = hierarquia[0][pai][3] >= 0
                            if temPai:
                                pai = hierarquia[0][pai][3]
                                temPai = hierarquia[0][pai][3] >= 0
                                if not temPai:
                                    idCentral = it

            if idCentral == -1:
                print 'Nao achou as elipses em ({},{})'.format(i)
                return False, None

            x = 0
            y = 0
            for it in range(0, 4):
                el = cv2.fitEllipse(contornos[idCentral])
                x += el[0][0]
                y += el[0][1]
                idCentral = hierarquia[0][idCentral][3]

            cx = x / 4.0
            cy = y / 4.0

            centers[ind] = [cx + c1[0], cy + c1[1]]

            ind += 1

    return True, centers


def findRingGrid_UsingOpencv_findCircles(img, pattern_size, image_path, centers):
    # para cada centro, criar uma regiao ao redor para procurar duas elipses
    roi_side = 50
    imgGray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    ell_in = []
    ell_out = []
    adicao = 0
    # cria roi para cada centro e salva as elipses de dentro e de fora para cada centro
    for i in range(0, len(centers)):
        y1 = int(centers[i][0][1] - roi_side)
        x1 = int(centers[i][0][0] - roi_side)
        cell = imgGray[y1:int(centers[i][0][1] + roi_side), x1:int(centers[i][0][0] + roi_side)]

        # blur e canny
        blur = cv2.GaussianBlur(cell, (5, 5), 0)
        edges = cv2.Canny(blur, 100, 200)

        contornos, hierarquia = cv2.findContours(edges, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

        idCentral = -1
        for it in range(0, len(contornos)):
            if len(contornos[it]) > 5:
                temFilho = hierarquia[0][it][2] > -1
                temPai = hierarquia[0][it][3] >= 0
                if not temFilho and temPai:
                    pai = hierarquia[0][it][3]
                    temPai = hierarquia[0][pai][3] >= 0
                    if temPai:
                        pai = hierarquia[0][pai][3]
                        temPai = hierarquia[0][pai][3] >= 0
                        if temPai:
                            pai = hierarquia[0][pai][3]
                            temPai = hierarquia[0][pai][3] >= 0
                            if not temPai:
                                idCentral = it

        if idCentral == -1:
            print 'Nao achou as elipses do centro {}'.format(i)
            return False, None

        # sao 4 ellipses, duas pra de dentro e duas pra de fora
        # adicionar somente uma elipse de cada, uma de dentr e uma de fora

        # pega id dos contornos circunscritos
        idPai1 = hierarquia[0][idCentral][3]
        idPai2 = hierarquia[0][idPai1][3]
        idPai3 = hierarquia[0][idPai2][3]

        # elipse central
        el1 = cv2.fitEllipse(contornos[idCentral])
        # elipse externa
        el2 = cv2.fitEllipse(contornos[idPai3])

        # adicao = meio entre as elipses
        adicao = (int((el2[1][0] - el1[1][0]) / 4.0), int((el2[1][1] - el1[1][1]) / 4.0))

        # ajusta espaco da imagem maior e append
        el1 = (
            (int(el1[0][0]) + x1, int(el1[0][1]) + y1),
            (int(el1[1][0] / 2) + adicao[0], int(el1[1][1] / 2) + adicao[1]),
            int(el1[2]))
        ell_in.append(el1)
        el2 = (
            (int(el2[0][0]) + x1, int(el2[0][1]) + y1),
            (int(el2[1][0] / 2) + adicao[0], int(el2[1][1] / 2) + adicao[0]),
            int(el2[2]))
        ell_out.append(el2)

    #### Cria duas imagens ####
    ones_mat = np.ones(img.shape, img.dtype)
    ones_mat[:] = (255, 255, 255)
    img_neg = cv2.subtract(ones_mat, img)

    # imagem 1 somente com elipses do centro
    mask_in = np.zeros(img.shape, img.dtype)
    for i in range(0, len(centers)):
        cv2.ellipse(mask_in, ell_in[i][0], ell_in[i][1], ell_in[i][2], 0, 380, (255, 255, 255), cv2.cv.CV_FILLED)
    # mostra_imagem('mask_in', mask_in)

    mask_in2 = np.zeros(img.shape, img.dtype)
    mask_in2[:] = (255, 255, 255)
    for i in range(0, len(centers)):
        cv2.ellipse(mask_in2, ell_in[i][0], ell_in[i][1], ell_in[i][2], 0, 380, (0, 0, 0), cv2.cv.CV_FILLED)
    # mostra_imagem('mask_in2', mask_in2)

    # pegando a cor entre as elipses
    angle = ell_in[0][2]
    axes = ell_in[0][1]
    axesMaior = ell_out[0][1]
    center = ell_in[0][0]

    dx = np.cos(np.radians(angle))
    dy = np.sin(np.radians(angle))
    scale = (axes[0] + axesMaior[0]) / 2.0
    nx, ny = dx * scale + center[0], dy * scale + center[1]
    pixel_cor = (255 - img.item(ny, nx, 0), 255 - img.item(ny, nx, 1), 255 - img.item(ny, nx, 2))

    # Circulos de dentro
    fundo = np.zeros(img.shape, img.dtype)
    fundo[:] = pixel_cor

    fundo = cv2.bitwise_and(fundo, mask_in2)
    result_img_m1 = cv2.bitwise_and(img_neg, mask_in)
    result_img_m1 = cv2.bitwise_or(fundo, result_img_m1)

    found1, centers1 = cv2.findCirclesGrid(result_img_m1, pattern_size)
    cv2.drawChessboardCorners(result_img_m1, pattern_size, centers1, found1)

    if not found1:
        return False, None

    # mostra_imagem('Cantos achados dentro', result_img_m1)

    # Circulos de fora
    pixel_cor = (255 - pixel_cor[0], 255 - pixel_cor[1], 255 - pixel_cor[2])
    fundo[:] = pixel_cor

    result_img_m1 = cv2.bitwise_and(fundo, mask_in)
    result_img_m2 = cv2.bitwise_and(img, mask_in2)
    result_img_m3 = cv2.bitwise_or(result_img_m1, result_img_m2)

    found2, centers2 = cv2.findCirclesGrid(result_img_m3, pattern_size)
    cv2.drawChessboardCorners(result_img_m3, pattern_size, centers1, found1)

    if not found2:
        return False, None

    # mostra_imagem('Cantos achados fora', result_img_m3)

    # Media entre os centros e retorna
    centers = centers1.copy()
    for i in range(0, len(centers1)):
        centers[i][0] = [(centers1[i][0][0] + centers2[i][0][0]) / 2.0, (centers1[i][0][1] + centers2[i][0][1]) / 2.0]

    # print centers
    return True, centers


img = cv2.imread('../images/circles.png', 1)

e1 = cv2.getTickCount()
e2 = cv2.getTickCount()
time = (e2 - e1) / cv2.getTickFrequency()
# print time

pattern_size = (6, 3)

# found, centers = cv2.findCirclesGrid(img, pattern_size)
found1, centers = findRingGrid(img, pattern_size, 'images/circles.png')

# refinar usando o findCirclesGrid
if found1:
    found2, centers2 = findRingGrid_UsingOpencv_findCircles(img, pattern_size, 'images/circles.png', centers)

    if found2:
        vis = img.copy()
        cv2.drawChessboardCorners(vis, pattern_size, centers2, found1)
        print centers2
        mostraImagem('vis', vis)
    else:
        print 'nao conseguiu refinar'
else:
    print 'nao achou ring'