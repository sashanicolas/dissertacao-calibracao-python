import numpy as np
import cv2
import sys
sys.path.append("../cvprak/")
import pysomemodule
import time


def findAdaptativeThreshold(roi):
    # threshold adaptativo pelo Prakash
    # valor medio de pixel da imagem -> round
    mi = np.rint(cv2.mean(roi)[0])
    # histograma -> primeiros mi valores -> reshape array pq opencv retorna [[],[]..]
    hist = cv2.calcHist([roi], [0], None, [256], [0, 256])[:mi].reshape(mi)
    # quantidade de pixels de cada cor ate mi
    pixels = np.sum(hist)
    # print pixels
    # array com indices(cor) ate mi para multiplicar com as ocorrencias
    indx = np.arange(mi)
    # multiplica a cor(indice) pela quantidade de pixels
    occur = hist * indx
    # soma os pixels e divide pela quantidade de pixels
    T = np.rint(np.sum(occur) / pixels)

    # print 'python T: ', T
    # print 'put1: ', hist

    indx = np.arange(256)

    lastT = -9999
    for step in range(1000, 0, -1):
        hist = cv2.calcHist([roi], [0], None, [256], [0, 256])
        m1 = np.sum(hist[:T].reshape(T) * indx[:T]) / np.sum(hist[:T])
        m2 = np.sum(hist[T:].reshape(256 - T) * indx[T:]) / np.sum(hist[T:])
        T = int(np.rint((m1 + m2) / 2.0))

        if lastT == T:
            break
        lastT = T

    return T


def get_contornos_adaptative_threshold(roi_4):
    roi_4 = cv2.GaussianBlur(roi_4, (5, 5), 0)

    T2 = findAdaptativeThreshold(roi_4)

    # Achar centro na roi_4
    thresh2, img_bin2 = cv2.threshold(roi_4, T2, 255, cv2.THRESH_BINARY)

    edges2 = cv2.Canny(img_bin2, 127, 255)

    # Encontra os contornos na roi_4
    contornos2, hierarquia2 = cv2.findContours(edges2.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

    return contornos2, hierarquia2


img = cv2.imread('../images/manuel2.jpg', 0)

Prakash = pysomemodule.ABC("")  # This will create an instance of ABC

s = time.time()
# Where the argument "image" is a OpenCV numpy image.
b = np.ones(shape=(4, 4), dtype=np.float32)
retval = Prakash.refineUsingPrakash(img, img, b)
# print b
print 'tempo c++) ', time.time() - s
print 'retval c++) ', retval.shape
# cv2.imshow('Imagem', retval)
# cv2.waitKey(0)

s = time.time()
# retval = findAdaptativeThreshold(img)
# for i in range(0, 10):
retval, retval2 = get_contornos_adaptative_threshold(img)
print 'tempo python) ', time.time() - s
# print 'retval python) ', retval

