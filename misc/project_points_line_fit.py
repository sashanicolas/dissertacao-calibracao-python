import matplotlib.pyplot as plt
import numpy as np
from planar.line import Line

# instanciar pontos
# pontos = np.array([[1, 2.5], [2, 4], [3, 1], [4, 4.3], [5, 3], [6, 5.5]])
# pontos = np.array([[1.0, 3.0], [2.0, 1.0], [3.0, 3.0], [4.0, 1.0], [5.0, 3.0], [6.0, 1.0]])
# pontos = np.array([[1, 1], [2, 2], [3, 3], [4, 4], [5, 5], [6, 6]])
# pontos = np.array([[1.0, 1.0], [2.0, 4.0], [3.0, 3.0], [4.0, 7.0], [5.0, 5.0], [6.0, 8.0]])
pontos = np.array([[1.0, 1.0], [1.0, 2.0], [1.0, 3.0], [1.0, 4.0], [1.0, 5.0], [1.0, 6.0]])
# pontos = np.array([[5, 2]])


def proj_points_min_dist3(pontos, line):
    planarLine = Line.from_points([[0.0, line(0.0)], [1.0, line(1.0)]])
    novos_pontos = np.zeros(shape=pontos.shape, dtype=np.float)

    for i in range(0, len(pontos)):
        novos_pontos[i] = planarLine.project(pontos[i])

    return novos_pontos


def proj_points_min_dist2(pontos, line):
    novos_pontos = np.zeros(shape=pontos.shape, dtype=np.float)

    for i in range(0, len(pontos)):
        a = [pontos[i][0] - 10.0, line(pontos[i][0] - 10.0)]
        b = [pontos[i][0] + 10.0, line(pontos[i][0] + 10.0)]
        ab = np.array(b) - np.array(a)
        ab /= np.linalg.norm(ab)
        ap = np.array(pontos[i]) - np.array(a)
        s = np.dot(ap, ab)
        ab *= s
        novos_pontos[i][0] = (a + ab)[0]
        novos_pontos[i][1] = (a + ab)[1]

    return novos_pontos


def proj_points_min_dist(pontos, line_data):
    novos_pontos = np.zeros(shape=pontos.shape, dtype=pontos.dtype)
    m_sasha = line_data[0]
    b_sasha = line_data[1]

    a = -m_sasha
    b = 1
    c = -b_sasha

    k = np.array([-c, b, a])
    # k = np.array([a, b, c])
    k /= np.linalg.norm(k)

    for i in range(0, len(pontos)):
        s = np.dot(k, np.array([pontos[i][0], pontos[i][1], 1]))
        # s = np.dot(k, np.array([pontos[i][0], pontos[i][1]]))
        p = k * s
        # novos_pontos[i][0] = p[0]
        # novos_pontos[i][1] = p[1]
        novos_pontos[i][0] = p[0] / p[2]
        novos_pontos[i][1] = p[1] / p[2]

    return novos_pontos


# check x coords
good = False
for i in range( 1, len(pontos)):
    if abs(pontos[i][0] - pontos[i - 1][0]) > 1e-09:
        good = True
        break

if not good:
    print "Inverter coordenadas"
    pontos = np.column_stack([pontos[:, 1], pontos[:, 0]])


# fazer fit line
line_fit_data = np.polyfit(pontos[:, 0], pontos[:, 1], 1, full=True)

# mostrar resultado
print line_fit_data

# tranforma dados p linha object
line_z = np.poly1d(line_fit_data[0])
# line_z = np.poly1d([0.5, 0])
print line_z

# mostrar lina em grafico
xz = np.linspace(-1, 8)
plt.plot(xz, line_z(xz))


# mostrar pontos
plt.plot(pontos[:, 0], pontos[:, 1], 'bo')

# projetar pontos na linha
new_proj_points = proj_points_min_dist3(pontos, line_z)
# new_proj_points = proj_points_min_dist3(pontos.astype(float), line_z)
print new_proj_points

# mostrar pontos projetados no grafico tbm
plt.plot(new_proj_points[:, 0], new_proj_points[:, 1], 'ro')

new_proj_points = proj_points_min_dist2(pontos, line_z)
print new_proj_points

# grafico configs
plt.axis([-1, 8, -1, 8])
plt.grid(True)
plt.axhline(color='k')
plt.axvline(color='k')
plt.show()

###########
# Teste de norma (Independente dos testes anteriores)
#
# a = np.array([[1, 0], [1, 0], [1, 0], [1, 0]])
# b = np.array([[0, 1], [0, 1000], [4, 0], [5, 0]])
# import cv2
#
# print cv2.norm(a, b, cv2.NORM_L2)
# print np.linalg.norm(a - b, axis=1)
