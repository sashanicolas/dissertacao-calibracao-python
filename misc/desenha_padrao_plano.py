from PIL import ImageDraw
from PIL import Image
from PIL import ImageFont

'''
=================================================================================================

Script para gerar imagem com padrao de anel em fronto paralelo

Configurar imagem

=================================================================================================
'''
factor = 4 * 4

back_ground_color = (255, 255, 255)
image_dimension = (400 * factor, 400 * factor)  # (1024, 768)
name_local_to_save = "../images/single_anel2.png"  # "../images/circles.png"
ring_color = (60, 60, 60)

# raio dos circulos
raio_maior = 150 * factor
raio_menor = 100 * factor
# posicao inicial dos aneis (superior esquerdo)
x_ini = 200 * factor
y_ini = 200 * factor
# quantidade de aneis por linha e coluna
linhas = 1  # 7
colunas = 1  # 10
# distancia entre os centros dos aneis
dx = 70 * factor
dy = 70 * factor

'''
=================================================================================================
Programa DO NOT CHANGE
=================================================================================================
'''
image = Image.new('RGB', image_dimension, back_ground_color)
draw = ImageDraw.Draw(image)

font = ImageFont.load_default()

for j in range(0, linhas):
    cy = j * dy + y_ini
    for i in range(0, colunas):
        cx = i * dx + x_ini
        box = [cx - raio_maior, cy - raio_maior, cx + raio_maior, cy + raio_maior]
        draw.ellipse(box, ring_color)

        box = [cx - raio_menor, cy - raio_menor, cx + raio_menor, cy + raio_menor]
        draw.ellipse(box, back_ground_color)

        # draw.ellipse(box, (i*50, j*50, 0))
        # draw.text((cx, cy), str(i)+' '+str(j), font=font)
        print (box[2] + box[0]) / 2.0, ',', (box[3] + box[1]) / 2.0, ','

image = image.resize((400, 400), resample=Image.ANTIALIAS)
image.save(name_local_to_save)
