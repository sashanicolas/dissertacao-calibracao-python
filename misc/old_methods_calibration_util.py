############################################################################
#
#                      ATENCAO
#  Este arquivo contem implementacao depreciada
#  Foi escrito no comeco do projeto mas foi melhorado.
#  Esta aqui para historico.
#
#                                        Sasha Nicolas
############################################################################

import cv2
import numpy as np

MOSTRA_GRID = False
USE_PRAKASH_THRESHOLD_ADAP = True
REFINE_RING_TO_CIRCLE = False
MOSTRA_NOVOS_FRONTO = False
MOSTRA_RING_TO_CIRCLE = False

# variaveia para auxiliar a selecao dos 4 cantos
windowSelect4Corners = 'Selecione 4 quantos'
cornersCount = 0
cornersPoints = []
image_aux = []


def desenhaQuadrilatero(img, p, c):
    for i in range(0, len(p)):
        p[i] = (int(p[i][0]), int(p[i][1]))

    cv2.line(img, p[0], p[1], c)
    cv2.line(img, p[1], p[2], c)
    cv2.line(img, p[2], p[3], c)
    cv2.line(img, p[3], p[0], c)


def desenhaLinha(a, b, color):
    global image_aux

    cv2.line(image_aux, a, b, color, 1, 8)


def mostraImagem(name, image=None, x=150, y=150, doNotWaitKey=False):
    global image_aux

    cv2.namedWindow(name)

    if image is not None:
        aux = image.copy()
        h, w = aux.shape[:2]
        if h > 800 or w > 800:
            fator_scale = 800.0 / w
            aux = cv2.resize(aux, (800, int(h * fator_scale)))
        cv2.imshow(name, aux)
    else:
        cv2.imshow(name, image_aux)

    cv2.moveWindow(name, x, y)

    if not doNotWaitKey:
        cv2.waitKey(0)
        cv2.destroyWindow(name)


def calculaGridROI(cornersPoints, pattern_size):
    """ Calcula os pontos que definem o grid em torno do padrao
    Retorna uma matriz (nx2) - representam coordenadas cartesianas em float
    """

    planePoints = np.array([(0, 0), (1, 0), (1, 1), (0, 1)], dtype='float64')
    H = cv2.findHomography(planePoints, np.array(cornersPoints, dtype='float64'))
    H = H[0]

    # if MOSTRA_GRID:
    # print "H = ", H

    nHorizontal = pattern_size[0]
    nVertical = pattern_size[1]

    quantPontosGrid = (nHorizontal + 1) * (nVertical + 1)

    pontosGridPlano = np.zeros((quantPontosGrid, 3), dtype=np.float64)

    # calculando pontos no grid ideal
    k = 0
    for i in range(0, nHorizontal + 1):
        x = i / float(nHorizontal)
        for j in range(0, nVertical + 1):
            y = j / float(nVertical)
            pontosGridPlano[k] = (x, y, 1)
            k += 1

    pontosGridPlano = np.transpose(pontosGridPlano)

    # if MOSTRA_GRID:
    #    print "pontosGridPlano = ", pontosGridPlano

    # multiplica pela homographia
    pontosGridImagem = np.dot(H, pontosGridPlano)

    pontos2dImagem = np.array(pontosGridImagem[[0, 1], :] / pontosGridImagem[2, :])
    pontos2dImagem = np.transpose(pontos2dImagem)

    # Imprime grid
    if MOSTRA_GRID:
        # linhas horizontais
        x = (nVertical + 1) * (nHorizontal)
        for i in range(0, nVertical + 1):
            desenhaLinha(tuple(pontos2dImagem[i].astype(int)), tuple(pontos2dImagem[i + x].astype(int)), (150, 0, 0))

        # linhas verticais
        for i in range(0, nHorizontal + 1):
            desenhaLinha(tuple(pontos2dImagem[i * (nVertical + 1)].astype(int)),
                         tuple(pontos2dImagem[i * (nVertical + 1) + nVertical].astype(int)), (150, 0, 0))

        # mostra imagem
        mostraImagem('Grid')

    # converter pontos do grid para inteiro
    for i in range(0, len(pontos2dImagem)):
        pontos2dImagem[i][0] = int(pontos2dImagem[i][0])
        pontos2dImagem[i][1] = int(pontos2dImagem[i][1])

    return pontos2dImagem


def getMenorPonto(p):
    c = [999999999, 999999999]
    for i in range(0, 4):
        if p[i][0] < c[0]:
            c[0] = p[i][0]
        if p[i][1] < c[1]:
            c[1] = p[i][1]

    return c


def getMaiorPonto(p):
    c = [0, 0]
    for i in range(0, 4):
        if p[i][0] > c[0]:
            c[0] = p[i][0]
        if p[i][1] > c[1]:
            c[1] = p[i][1]

    return c


def desenhaCruz(x, y, color, img=None, thickness=1):
    global image_aux

    x = int(x)
    y = int(y)
    if img is not None:
        cv2.line(img, (x - 5, y), (x + 5, y), color, thickness, 8)
        cv2.line(img, (x, y - 5), (x, y + 5), color, thickness, 8)
    else:
        cv2.line(image_aux, (x - 5, y), (x + 5, y), color, thickness, 8)
        cv2.line(image_aux, (x, y - 5), (x, y + 5), color, thickness, 8)


def onmouse(event, x, y, flags, param):
    global windowSelect4Corners
    global image_aux
    global cornersCount

    if event == cv2.EVENT_LBUTTONUP:
        desenhaCruz(x, y, (150, 0, 0))
        cv2.imshow(windowSelect4Corners, image_aux)
        # print x, y
        cornersPoints.append((x, y))
        cornersCount += 1


def removeOnMouse(event, x, y, flags, param):
    pass


def getCornersPoints(image_path, imgGray):
    global cornersCount
    global windowSelect4Corners
    global image_aux
    global cornersPoints

    try:
        cornersPoints = np.load(image_path + '_CANTOS.npy')
    except IOError:
        # mostrar imagem e pegar 4 pontos de clique de mouse
        cornersCount = 0
        cornersPoints = []

        image_aux = imgGray.copy()
        cv2.namedWindow(windowSelect4Corners)
        cv2.setMouseCallback(windowSelect4Corners, onmouse, imgGray)
        cv2.imshow(windowSelect4Corners, imgGray)

        while cornersCount < 4:
            cv2.waitKey(20)

        cv2.setMouseCallback(windowSelect4Corners, removeOnMouse)
        cv2.destroyWindow(windowSelect4Corners)

        np.save(image_path + '_CANTOS.npy', cornersPoints)

    return cornersPoints


def getIdCentralFourConcentricEllipses(contornos, hierarquia):
    idCentral = -1
    for it in range(0, len(contornos)):
        if len(contornos[it]) > 5:
            temFilho = hierarquia[0][it][2] > -1
            temPai = hierarquia[0][it][3] >= 0
            if not temFilho and temPai:
                pai = hierarquia[0][it][3]
                temPai = hierarquia[0][pai][3] >= 0
                if temPai:
                    pai = hierarquia[0][pai][3]
                    temPai = hierarquia[0][pai][3] >= 0
                    if temPai:
                        pai = hierarquia[0][pai][3]
                        temPai = hierarquia[0][pai][3] >= 0
                        if not temPai:
                            idCentral = it

    return idCentral


def getAreaOfContour(contorno):
    # achar menor e maior coordenadas em x e em y
    menor_x = menor_y = 999999
    maior_x = maior_y = -99999

    for i in range(0, len(contorno)):
        if contorno[i][0][0] < menor_x:
            menor_x = contorno[i][0][0]
        if contorno[i][0][1] < menor_y:
            menor_y = contorno[i][0][1]
        if contorno[i][0][0] > maior_x:
            maior_x = contorno[i][0][0]
        if contorno[i][0][1] > maior_y:
            maior_y = contorno[i][0][1]

    area = (maior_x - menor_x) * (maior_y - menor_y)
    return area


def getCenterOfEllipses(contornos, hierarquia, (i, j), max_area):
    x = 0
    y = 0
    idCentral = getIdCentralFourConcentricEllipses(contornos, hierarquia)
    if idCentral == -1:
        print '> getCenterOfEllipses :: Nao achou as elipses em ({},{})'.format(i, j) + ' #contours = ' + str(
            len(contornos))
        count = 0
        for it in range(0, len(contornos)):
            contour_area = getAreaOfContour(contornos[it])
            # print contour_area
            if 5 < len(contornos[it]) and max_area / 40 < contour_area < max_area:
                el = cv2.fitEllipse(contornos[it])
                x += el[0][0]
                y += el[0][1]
                count += 1
    else:
        for it in range(0, 4):
            el = cv2.fitEllipse(contornos[idCentral])
            x += el[0][0]
            y += el[0][1]
            idCentral = hierarquia[0][idCentral][3]
        count = 4

    if count == 0:
        return -1, -1

    cx = x / float(count)
    cy = y / float(count)

    return cx, cy


def findSingleRind(img, (i, j)):
    imgGray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    if USE_PRAKASH_THRESHOLD_ADAP:
        # threshold adaptativo
        # valor medio de pixel da imagem -> round
        mi = np.rint(cv2.mean(imgGray)[0])
        # histograma -> primeiros mi valores -> reshape array pq opencv retorna [[],[]..]
        hist = cv2.calcHist([imgGray], [0], None, [256], [0, 256])[:mi].reshape(mi)
        # quantidade de pixels de cada cor ate mi
        pixels = np.sum(hist)
        # array com indices(cor) ate mi para multiplicar com as ocorrencias
        indx = np.arange(mi)
        # multiplica a cor(indice) pela quantidade de pixels
        occur = hist * indx
        # soma os pixels e divide pela quantidade de pixels
        T = np.rint(np.sum(occur) / pixels)

        indx = np.arange(256)
        for step in range(10, 0, -1):
            hist = cv2.calcHist([imgGray], [0], None, [256], [0, 256])
            m1 = np.sum(hist[:T].reshape(T) * indx[:T]) / np.sum(hist[:T])
            m2 = np.sum(hist[T:].reshape(256 - T) * indx[T:]) / np.sum(hist[T:])
            T = np.rint((m1 + m2) / 2.0)
            # print T

        thresh, img_bin = cv2.threshold(imgGray, T, 255, cv2.THRESH_BINARY)

        blur = cv2.GaussianBlur(img_bin, (3, 3), 0)
        edges = cv2.Canny(blur, 100, 150)
    else:
        # Otsu's thresholding after Gaussian filtering
        blur = cv2.GaussianBlur(imgGray, (5, 5), 0)
        thresh, img_bin = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
        edges = cv2.Canny(img_bin, 100, 150)

    contornos, hierarquia = cv2.findContours(edges, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

    # if len(contornos) ==3:
    #     for i in range(0, len(contornos)):
    #         cv2.drawContours(imgGray, contornos, i, (255, 0, 255))
    #         mostra_imagem('cont', imgGray)

    # procurar quatro elipses, uma dentro da outra, usando a hierarquia
    h, w = img.shape[:2]
    max_area = h * w
    cx, cy = getCenterOfEllipses(contornos, hierarquia, (i, j), max_area)

    return [cx, cy], hierarquia


def _ind(i, j, nVertical):
    return j * (nVertical + 1) + i


def findRingGrid(img, pattern_size, image_path):
    imgGray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # se ja salvou os 4 cantos
    cornersPoints = getCornersPoints(image_path, imgGray)

    # criar grid
    pontosDoGrid = calculaGridROI(cornersPoints, pattern_size)

    # para cada area do grid achar o anel (duas circunferencias) e o centro adicina em centers
    nHorizontal = pattern_size[0]
    nVertical = pattern_size[1]

    # percorre o grid para cada celula procurar o anel
    ind = 0
    bias = int(cv2.norm(pontosDoGrid[1] - pontosDoGrid[0]) / 6.0)  # aumentar a celula do grid para pesquisa
    centers = np.ones((pattern_size[0] * pattern_size[1], 1, 2), dtype='float32')
    for i in range(0, nVertical):
        for j in range(0, nHorizontal):
            p = [pontosDoGrid[_ind(i, j, nVertical)],
                 pontosDoGrid[_ind(i, j + 1, nVertical)],
                 pontosDoGrid[_ind(i + 1, j + 1, nVertical)],
                 pontosDoGrid[_ind(i + 1, j, nVertical)]]
            c1 = getMenorPonto(p)
            c2 = getMaiorPonto(p)

            c1[0] -= bias
            c1[1] -= bias
            c2[0] += bias
            c2[1] += bias

            cell = img[int(c1[1]):int(c2[1]), int(c1[0]):int(c2[0])]

            [cx, cy], hierarquia = findSingleRind(cell, (i, j))

            centers[ind] = [cx + c1[0], cy + c1[1]]
            ind += 1

    return True, centers


def getAreaElipse(a, b):
    return a * b * 3.1415


# metodo tentativa de usar o find circle do opencv p achar os aneis
def findRingGrid_UsingOpenCV(img, pattern_size, centers, roi_side=50):
    # para cada centro, criar uma regiao ao redor para procurar duas elipses
    imgGray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    ell_in = []
    ell_out = []

    areaRoi = roi_side * roi_side * 4

    # cria roi para cada centro e salva as elipses de dentro e de fora para cada centro
    for i in range(0, len(centers)):
        y1 = int(centers[i][1] - roi_side)
        x1 = int(centers[i][0] - roi_side)
        cell = imgGray[y1:int(centers[i][1] + roi_side), x1:int(centers[i][0] + roi_side)]
        # mostra_imagem('cell' , cell)
        # blur e canny
        blur = cv2.GaussianBlur(cell, (5, 5), 0)
        edges = cv2.Canny(blur, 100, 150)

        contornos, hierarquia = cv2.findContours(edges, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

        # # mostra contornos que tem area pequena
        # if len(contornos) > 4 or len(contornos) < 4:
        #     print 'findRingGrid_UsingOpenCV:: Achou {} contornos '.format(len(contornos))
        #     for it in range(0, len(contornos)):
        #         img_deb = img.copy()[y1:int(centers[i][1] + roi_side), x1:int(centers[i][0] + roi_side)]
        #         # if getAreaOfContour(contornos[it]) < 50:
        #         print 'findRind:: Contorno {},length={},area={}'.format(it, len(contornos[it]),
        #                                                                 getAreaOfContour(contornos[it]))
        #         # print len(contornos[i]), getAreaOfContour(contornos[i])
        #         cv2.drawContours(img_deb, contornos, it, (0, 0, 255))
        #         mostra_imagem('debug contour', img_deb)

        # new

        # procura os contornos que podem ser elipses - tem area minima

        # count = 0
        indxEllipses = []
        elipses = []
        for it in range(0, len(contornos)):
            if len(contornos[it]) > 5 and getAreaOfContour(contornos[it]) > 50:
                el = cv2.fitEllipse(contornos[it])
                if getAreaElipse(el[1][0], el[1][0]) < areaRoi:
                    indxEllipses.append(it)
                    elipses.append(el)
                    # count += 1

        # dos que podem ser elipses, faz fit e pega a maior e a menor elipse
        el_menor = elipses[0]
        el_maior = elipses[0]
        id_interna = 0
        id_externa = 0
        for it in range(1, len(elipses)):
            if elipses[it][1][0] < el_menor[1][0]:
                id_interna = it
                el_menor = elipses[it]
            if elipses[it][1][1] < el_menor[1][1]:
                id_interna = it
                el_menor = elipses[it]
            if elipses[it][1][0] > el_maior[1][0]:
                id_externa = it
                el_maior = elipses[it]
            if elipses[it][1][1] > el_maior[1][1]:
                id_externa = it
                el_maior = elipses[it]

        # elipse central
        el1 = cv2.fitEllipse(contornos[indxEllipses[id_interna]])
        # elipse externa
        el2 = cv2.fitEllipse(contornos[indxEllipses[id_externa]])

        # old
        # idCentral = getIdCentralFourConcentricEllipses(contornos, hierarquia)
        '''
        if idCentral == -1:

            # TODO: mesmo achando 3 nao garante que sejam elipses perfeitas
            # verificar se 3 contornos tem tamanho maior que 30
            indicesContornosMaior30 = []
            for k in range(0, len(contornos)):
                if len(contornos[k]) > 30:
                    indicesContornosMaior30.append(k)

            # tem no minimo 3 contornos, procurar duas que tem eixos bem diferentes
            if len(contornos) >= 3 and len(indicesContornosMaior30) >= 3:
                eixos = []
                for m in range(len(indicesContornosMaior30)):
                    eixos.append(cv2.fitEllipse(contornos[indicesContornosMaior30[m]])[1])  # eixo_x,eixo_y

                # das varios contornos (geralm 4 no max) procura o que tem maior e menor eixo
                indx_menor = 0
                indx_maior = 0
                for m in range(len(indicesContornosMaior30)):
                    if eixos[m][0] < eixos[indx_menor][0]:
                        indx_menor = m
                    if eixos[m][0] > eixos[indx_maior][0]:
                        indx_maior = m

                # elipse central
                indx_menor = indicesContornosMaior30[indx_menor]
                el1 = cv2.fitEllipse(contornos[indx_menor])

                # elipse externa
                indx_maior = indicesContornosMaior30[indx_maior]
                el2 = cv2.fitEllipse(contornos[(indx_maior)])
        '''
        # end if idCentral == -1:

        # (nao ta separando corretamente)
        # adicao = meio entre as elipses
        adicao = (int((el2[1][0] - el1[1][0]) / 4.0), int((el2[1][1] - el1[1][1]) / 4.0))

        # ajusta espaco da imagem maior e append
        el1 = (
            (int(el1[0][0]) + x1, int(el1[0][1]) + y1),
            (int(el1[1][0] / 2) + adicao[0], int(el1[1][1] / 2) + adicao[1]),
            int(el1[2]))
        ell_in.append(el1)
        el2 = (
            (int(el2[0][0]) + x1, int(el2[0][1]) + y1),
            (int(el2[1][0] / 2) + adicao[0], int(el2[1][1] / 2) + adicao[0]),
            int(el2[2]))
        ell_out.append(el2)

        # mostra_imagem('img_deb', img_deb)
        # print len(ell_in), len(ell_out)

        # img_deb = img.copy()
        # for n in range(0, len(ell_in)):
        #     cv2.ellipse(img_deb, ell_in[n][0], ell_in[n][1], ell_in[n][2], 0, 360, (0, 0, 255))
        # mostra_imagem('img_deb', img_deb)

        # cv2.waitKey(0)
    # end for i in range(0, len(centers)):

    # # desenhar as elipses de dentro e fora achadas
    # img_deb = img.copy()
    # for n in range(0, len(ell_in)):
    #     cv2.ellipse(img_deb, ell_in[n][0], ell_in[n][1], ell_in[n][2], 0, 360, (0, 0, 255))
    #     cv2.ellipse(img_deb, ell_out[n][0], ell_out[n][1], ell_out[n][2], 0, 360, (0, 255, 0))
    # mostra_imagem('img_deb', img_deb)

    # mascaras 1 somente com elipses do centro
    mask_in = np.zeros(img.shape, img.dtype)
    for i in range(0, len(centers)):
        cv2.ellipse(mask_in, ell_in[i][0], ell_in[i][1], ell_in[i][2], 0, 380, (255, 255, 255), cv2.cv.CV_FILLED)
    # mostra_imagem('mask_in', mask_in)

    # mascaras 2 somente com elipses do centro (invertida)
    mask_in2 = np.zeros(img.shape, img.dtype)
    mask_in2[:] = (255, 255, 255)
    for i in range(0, len(centers)):
        cv2.ellipse(mask_in2, ell_in[i][0], ell_in[i][1], ell_in[i][2], 0, 380, (0, 0, 0), cv2.cv.CV_FILLED)
    # mostra_imagem('mask_in2', mask_in2)

    # pegando a cor entre as elipses
    angle = ell_in[0][2]
    axes = ell_in[0][1]
    axesMaior = ell_out[0][1]
    center = ell_in[0][0]

    dx = np.cos(np.radians(angle))
    dy = np.sin(np.radians(angle))
    scale = (axes[0] + axesMaior[0]) / 2.0
    nx, ny = dx * scale + center[0], dy * scale + center[1]
    pixel_cor = (255 - img.item(ny, nx, 0), 255 - img.item(ny, nx, 1), 255 - img.item(ny, nx, 2))

    # Circulos de dentro
    fundo = np.zeros(img.shape, img.dtype)
    fundo[:] = pixel_cor

    ones_mat = np.ones(img.shape, img.dtype)
    ones_mat[:] = (255, 255, 255)
    img_neg = cv2.subtract(ones_mat, img)

    fundo = cv2.bitwise_and(fundo, mask_in2)
    result_img_m1 = cv2.bitwise_and(img_neg, mask_in)
    result_img_m1 = cv2.bitwise_or(fundo, result_img_m1)

    found1, centers1 = cv2.findCirclesGrid(result_img_m1, pattern_size)

    if not found1:
        print '   Nao refinou. -> findCircle in false'
        mostraImagem('Cantos NAO achados dentro', result_img_m1)
        return False, None

    if MOSTRA_RING_TO_CIRCLE:
        cv2.drawChessboardCorners(result_img_m1, pattern_size, centers1, found1)
        mostraImagem('Cantos achados dentro', result_img_m1)

    # Circulos de fora
    pixel_cor = (255 - pixel_cor[0], 255 - pixel_cor[1], 255 - pixel_cor[2])
    fundo[:] = pixel_cor

    result_img_m1 = cv2.bitwise_and(fundo, mask_in)
    result_img_m2 = cv2.bitwise_and(img, mask_in2)
    result_img_m3 = cv2.bitwise_or(result_img_m1, result_img_m2)

    found2, centers2 = cv2.findCirclesGrid(result_img_m3, pattern_size)

    if not found2:
        print '   Nao refinou. -> findCircle out false'
        mostraImagem('Cantos NAO achados fora', result_img_m3)
        return False, None

    if MOSTRA_RING_TO_CIRCLE:
        cv2.drawChessboardCorners(result_img_m3, pattern_size, centers1, found1)
        mostraImagem('Cantos achados fora', result_img_m3)

    # Media entre os centros e retorna
    centers = np.zeros((pattern_size[0] * pattern_size[1], 2), dtype=np.float32)
    for i in range(0, len(centers1)):
        centers[i] = [(centers1[i][0][0] + centers2[i][0][0]) / 2.0, (centers1[i][0][1] + centers2[i][0][1]) / 2.0]

    return True, centers


def findCentrosFrontoParallel(unproject_images, centros_unprojected, roiSide, pattern, pattern_size):
    '''
    Aqui, deve se encontrar as novas posicoes dos centros nas imagens em fronto paralelo.
    Se o padrao for circulo, usa o proprio finCircles do OpenCV. Caso seja Ring, percorre os centros que foram
        colocados em fronto paralelo, criando uma roi de lado roiSide em torno desse centro e procurando os
        contornos dos aneis, fazendo o fitEllipse simples
    :param unproject_images:
    :param centros_unprojected:
    :param roiSide:
    :param pattern:
    :param pattern_size:
    :return:
    '''
    novos_centros_fronto = np.zeros((len(unproject_images), pattern_size[0] * pattern_size[1], 2), dtype=np.float32)

    if pattern == 'circle':
        for i in range(0, len(unproject_images)):
            found, refined_centers = cv2.findCirclesGrid(unproject_images[i], pattern_size)
            if not found:
                print 'Circle grid not found for image', i
            novos_centros_fronto[i] = refined_centers.reshape(-1, 2)

    elif pattern == 'ring':
        p = [None] * 4
        for i in range(0, len(unproject_images)):
            image = unproject_images[i].copy()
            centros = np.zeros((pattern_size[0] * pattern_size[1], 2), dtype=np.float32)

            # para cada centro antigo acha um centro novo e coloca em centros
            for j in range(0, len(centros_unprojected[i])):
                p[0] = (
                    int(centros_unprojected[i][j][0] - roiSide / 2.0),
                    int(centros_unprojected[i][j][1] - roiSide / 2.0))
                p[1] = (
                    int(centros_unprojected[i][j][0] + roiSide / 2.0),
                    int(centros_unprojected[i][j][1] - roiSide / 2.0))
                p[2] = (
                    int(centros_unprojected[i][j][0] + roiSide / 2.0),
                    int(centros_unprojected[i][j][1] + roiSide / 2.0))
                p[3] = (
                    int(centros_unprojected[i][j][0] - roiSide / 2.0),
                    int(centros_unprojected[i][j][1] + roiSide / 2.0))

                # print p
                # os.system("pause")

                roi = image[p[0][1]:p[3][1], p[0][0]:p[1][0]].copy()

                centro, hierarquia = findSingleRind(roi, (i, j))

                if centro[0] == -1:
                    centros[j][0] = centros_unprojected[i][j][0]
                    centros[j][1] = centros_unprojected[i][j][1]
                else:
                    centros[j][0] = centro[0] + p[0][0]
                    centros[j][1] = centro[1] + p[0][1]

            if REFINE_RING_TO_CIRCLE:
                found, refined_centers = findRingGrid_UsingOpenCV(image, pattern_size, centros, roiSide / 2)
                if found:
                    novos_centros_fronto[i] = refined_centers
                else:
                    print "find_centros_fronto_parallel:: nao refinou grid ring"
                    novos_centros_fronto[i] = centros
            else:
                novos_centros_fronto[i] = centros

            debugCompareCenters(MOSTRA_NOVOS_FRONTO, 'Novos centros no paralelo (novos = red)',
                                image, centros_unprojected[i], centros)

        cv2.destroyAllWindows()

    return novos_centros_fronto


def desenhaCentros(img, centros, color=(255, 255, 0), thickness=None):
    for i in range(0, len(centros)):
        if thickness:
            desenhaCruz(centros[i][0], centros[i][1], color, img, thickness)
        else:
            desenhaCruz(centros[i][0], centros[i][1], color, img)


def debugCompareCenters(showFlag, title, img, centers1, centers2, color1=(0, 255, 0), color2=(0, 0, 255)):
    if showFlag:
        imgCopy = img.copy()
        desenhaCentros(imgCopy, centers1, color1, 2)
        desenhaCentros(imgCopy, centers2, color2)
        mostraImagem(title, imgCopy)


# old functions optimization

def Rm1(x, y, r11, r12, t1):
    return r11 * x + r12 * y + t1


def Rm2(x, y, r21, r22, t2):
    return r21 * x + r22 * y + t2


def Rm3(x, y, r31, r32, t3):
    return r31 * x + r32 * y + t3


def func_deriv2(c, pontos):
    alfa, gamma, beta, x0, y0, r11, r21, r31, r12, r22, r32, t1, t2, t3 = \
        c[0], c[1], c[2], c[3], c[4], c[5], c[6], c[7], c[8], c[9], c[10], c[11], c[12], c[13]

    dfdc0, dfdc1, dfdc2, dfdc3, dfdc4, dfdc5, dfdc6, dfdc7, dfdc8, dfdc9, dfdc10, dfdc11, dfdc12, dfdc13 = [0] * 14

    for x, y in pontos:
        rm1 = Rm1(x, y, r11, r12, t1)
        rm2 = Rm2(x, y, r21, r22, t2)
        rm3 = Rm3(x, y, r31, r32, t3)

        x_proj = (alfa * rm1 + gamma * rm2 + x0 * rm3) / rm3
        y_proj = (beta * rm2 + y0 * rm3) / rm3

        m = x - x_proj
        n = y - y_proj
        u = m ** 2 + n ** 2
        one_over_sqrt_u = u ** 0.5

        """ Derivative of objective function """
        dfdc0 += - one_over_sqrt_u * m * rm1 / rm3
        dfdc1 += - one_over_sqrt_u * m * rm2 / rm3
        dfdc2 += - one_over_sqrt_u * n * rm2 / rm3

        dfdc3 += - one_over_sqrt_u * m
        dfdc4 += - one_over_sqrt_u * n

        dfdc5 += - one_over_sqrt_u * m * alfa * x / rm3
        dfdc6 += - one_over_sqrt_u * x * (m * alfa + n * beta) / rm3
        dfdc7 += one_over_sqrt_u * (
            m * x * (alfa * rm1 + gamma * rm2) / (rm3 ** 2) + n * x * (beta * rm2) / (rm3 ** 2))

        dfdc8 += - one_over_sqrt_u * m * alfa * y / rm3
        dfdc9 += - one_over_sqrt_u * y * (m * alfa + n * beta) / rm3
        dfdc10 += one_over_sqrt_u * (
            m * y * (alfa * rm1 + gamma * rm2) / (rm3 ** 2) + n * y * (beta * rm2) / (rm3 ** 2))

        dfdc11 += - one_over_sqrt_u * m * alfa / rm3
        dfdc12 += - one_over_sqrt_u * (m * alfa + n * beta) / rm3
        dfdc13 += one_over_sqrt_u * (m * (alfa * rm1 + gamma * rm2) + n * (beta * rm2)) / (rm3 ** 2)

    return np.array(
        [dfdc0, dfdc1, dfdc2, dfdc3, dfdc4, dfdc5, dfdc6, dfdc7, dfdc8, dfdc9, dfdc10, dfdc11, dfdc12, dfdc13])

