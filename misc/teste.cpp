void adaptiveThreshold(unsigned char* input, unsigned char* bin, int wImg, int hImg)
{
    int IMAGE_WIDTH = wImg;
    int IMAGE_HEIGHT = hImg;
    int S = (IMAGE_WIDTH / 8);
    float T = 0.15f;

    unsigned long* integralImg = 0;
    int i, j;
    long sum = 0;
    int count = 0;
    int index;
    int x1, y1, x2, y2;
    int s2 = S / 2;

    // create the integral image
    integralImg = (unsigned long*)malloc(IMAGE_WIDTH*IMAGE_HEIGHT*sizeof(unsigned long*));

    for (i = 0; i < IMAGE_WIDTH; i++)
    {
        // reset this column sum
        sum = 0;

        for (j = 0; j < IMAGE_HEIGHT; j++)
        {
            index = j*IMAGE_WIDTH + i;

            sum += input[index];
            if (i == 0)
                integralImg[index] = sum;
            else
                integralImg[index] = integralImg[index - 1] + sum;
        }
    }

    // perform thresholding
    for (i = 0; i < IMAGE_WIDTH; i++)
    {
        for (j = 0; j < IMAGE_HEIGHT; j++)
        {
            index = j*IMAGE_WIDTH + i;

            // set the SxS region
            x1 = i - s2; x2 = i + s2;
            y1 = j - s2; y2 = j + s2;

            // check the border
            if (x1 < 0) x1 = 0;
            if (x2 >= IMAGE_WIDTH) x2 = IMAGE_WIDTH - 1;
            if (y1 < 0) y1 = 0;
            if (y2 >= IMAGE_HEIGHT) y2 = IMAGE_HEIGHT - 1;

            count = (x2 - x1)*(y2 - y1);

            // I(x,y)=s(x2,y2)-s(x1,y2)-s(x2,y1)+s(x1,x1)
            sum = integralImg[y2*IMAGE_WIDTH + x2] -
                integralImg[y1*IMAGE_WIDTH + x2] -
                integralImg[y2*IMAGE_WIDTH + x1] +
                integralImg[y1*IMAGE_WIDTH + x1];

            if ((long)(input[index] * count) < (long)(sum*(1.0 - T)))
                bin[index] = 0;
            else
                bin[index] = 255;
        }
    }

    free(integralImg);
}