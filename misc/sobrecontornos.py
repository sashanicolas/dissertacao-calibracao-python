__author__ = 'sasha'

import cv2
import numpy as np
from find_ring_grid import RingsGrid


# ======================================================================================================================
# Funcoes
# ======================================================================================================================
def all_ellipses_contornos_to_center(contornos):
    x, y, count = 0, 0, 0

    for i in range(0, len(contornos)):
        if len(contornos[i]) <= 5:
            continue
        count += 1

        # print contornos[i].dtype, len(contornos[i])
        el = cv2.fitEllipse(contornos[i])
        x += el[0][0]
        y += el[0][1]

        print "   (" + str(count) + ")", el[0][0], el[0][1]

        # img_contorno = np.zeros(img.shape)
        # cv2.drawContours(img_contorno, contornos, i, (255, 255, 0))
        # cv2.imshow('contornos', img_contorno)
        # cv2.moveWindow('contornos', 300, 300)
        # cv2.waitKey(0)

    cv2.destroyAllWindows()

    x /= count
    y /= count
    print "count ", count
    return x, y


# ======================================================================================================================
# End Funcoes
# ======================================================================================================================

####################################################
#
# Abrir imagem
#
####################################################

img = cv2.imread('../images/single_anel2.png', cv2.IMREAD_UNCHANGED)
# cv2.imshow('img', img)

kernel_size = 5
img_gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)

# ======================================================================================================================
#
# 1 - Fit ellipse, com threshold media
#
# ======================================================================================================================
print "\n- Fazendo fit de ellipse com threshold media"

contornos, hierarquia = RingsGrid.get_contornos_threshold_weighted(img_gray)

print 'contornos ', len(contornos)
print 'hierarquia ', len(hierarquia[0])

x, y = all_ellipses_contornos_to_center(contornos)
print 'x,y ', x, y
print 'diff ', abs(200.0 - x), abs(200.0 - y)

# ======================================================================================================================
#
# 2 - Fit ellipse, blur gaussiana e canny
#
# ======================================================================================================================
print "\n- Fazendo fit de ellipse com blur gaussiana e canny"

contornos, hierarquia = RingsGrid.get_contornos_gaussian_blur_canny(img_gray)

print 'contornos ', len(contornos)
print 'hierarquia', len(hierarquia[0])

x2, y2 = all_ellipses_contornos_to_center(contornos)
print 'x2,y2 ', x2, y2
print 'diff ', abs(x2 - 200.0), abs(y2 - 200.0)

# ======================================================================================================================
#
# 3 - Fit ellipse, com get_contornos_adaptative_threshold
#
# ======================================================================================================================
print "\n- Fazendo fit de ellipse com threshold adaptativo"


# Supersample the roi
factor = 16.0
height, width = img_gray.shape[:2]
img_gray_4 = cv2.resize(img_gray, (int(factor * width), int(factor * height)), interpolation=cv2.INTER_CUBIC)

# Encontra os contornos na roi_4
contornos2, hierarquia2 = RingsGrid.get_contornos_adaptative_threshold(img_gray_4)

from find_ring_grid import DrawingUtils
# DrawingUtils.mostra_imagem("roi4", img_gray_4,0,0, wait_key_zero=True)
# RingsGrid.debugContoursEach(contornos2, np.ones(img_gray_4.shape))

print 'contornos ', len(contornos2)
print 'hierarquia ', len(hierarquia2[0])

# cv2.imshow('threshold threshold', cellbw)
# cv2.waitKey(0)
# cv2.destroyAllWindows()

x3, y3 = all_ellipses_contornos_to_center(contornos2)

x3 /= factor
y3 /= factor
print 'x3,y3 ', x3, y3
print 'diff ', abs(200.0 - x3), abs(200.0 - y3)


# ======================================================================================================================
#
# 4 - Adptative thrshold do Manuel
#
# ======================================================================================================================
# print "\n- Adptative thrshold do Manuel"
#
# img_gray_4_blur = cv2.GaussianBlur(img_gray_4, (RingsGrid.kernel_size, RingsGrid.kernel_size), 0)
# img_bin_4 = RingsGrid.binariza_adaptiveThreshold(img_gray_4_blur)
# DrawingUtils.mostra_imagem("Imagem Binarizada", img_bin_4)
#
# edges2 = cv2.Canny(img_bin_4, RingsGrid.canny_min, RingsGrid.canny_max)
#
# # Encontra os contornos na roi_4
# contornos2, hierarquia2 = cv2.findContours(edges2.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
#
# from find_ring_grid import Ring
# ringsCount, ringsIds, rings = Ring.countRingsInImage(contornos2, hierarquia2)
#
# print 'contornos ', len(rings[0].contours)
#
# x4, y4 = all_ellipses_contornos_to_center(rings[0].contours)
# x4 /= factor
# y4 /= factor
# print 'x4,y4 ', x4, y4
# print 'diff ', abs(200.0 - x4), abs(200.0 - y4)