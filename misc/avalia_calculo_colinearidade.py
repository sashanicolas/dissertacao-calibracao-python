############################################################################
#
#   Testeando funcao de calculo de colinearidade
#   Aqui crio um grid de pontos e aplico rotacao, translacao, escala e perspectiva
#   no conjunto de pontos e calculo o erro de colinearidade
#   O maior erro deu 0.0042835532513, explicado pela precisao do float 32
############################################################################

__author__ = 'sasha'

import calibration_util
import numpy as np
import math
import matplotlib.pyplot as plt

def rotation_matrix(axis, theta):
    """
    Return the rotation matrix associated with counterclockwise rotation about
    the given axis by theta radians.
    """
    axis = np.asarray(axis)
    theta = np.asarray(theta)
    axis = axis / math.sqrt(np.dot(axis, axis))
    a = math.cos(theta / 2.0)
    b, c, d = -axis * math.sin(theta / 2.0)
    aa, bb, cc, dd = a * a, b * b, c * c, d * d
    bc, ad, ac, ab, bd, cd = b * c, a * d, a * c, a * b, b * d, c * d
    return np.array([[aa + bb - cc - dd, 2 * (bc + ad), 2 * (bd - ac)],
                     [2 * (bc - ad), aa + cc - bb - dd, 2 * (cd + ab)],
                     [2 * (bd + ac), 2 * (cd - ab), aa + dd - bb - cc]])


# x = np.array([1,2,3,4,5], dtype=np.float32)
# y = np.array([1,1,1.1,1,1], dtype=np.float32)
# z = np.polyfit(x, y, 1, full=True)
# print "erro com fit ", z[1][0]

axis = [0, 0, 1]

# v = [[3, 5, 0],[3, 5, 0]]
# for i in range(0, len(v)):
#     print(np.dot(rotation_matrix(axis, theta), v[i]))

dim = (60, 40)
grid = calibration_util.getPatternPointsInZ(dim, 25)
# print grid
# print grid[6]

# Aplica escala, rotacao e translacao
scale = [2.1415, 3.8, 1]
theta = np.deg2rad(33.735)
translate = [11.11, 10.762, 0]

for i in range(0, len(grid)):
    grid[i] = scale * np.dot(rotation_matrix(axis, theta), grid[i]) + translate
    pass

import cv2

input = 200 * np.array([[0, 0], [1, 0], [1, 1], [0, 1]], dtype=np.float32)
output = 200 * np.array([[0, 0], [5, 2], [10, 10], [0, 2]], dtype=np.float32)
perspectiveMatrix = cv2.getPerspectiveTransform(input, output)

trasnformedPoints = grid
# trasnformedPoints = cv2.perspectiveTransform(np.array([grid[:,:2]]), perspectiveMatrix)[0]

# testar mudando um ponto
#  grid[7][0] += 1
#
# p = np.array([6, 6])
# s = np.array([0, 0])
# e = np.array([12, 12])

# print calibration_util.distancePointLine(p, s, e)

erro = calibration_util.computeCollinearityErrorOLD(trasnformedPoints, dim[0], dim[1])
print "erro de colinearidade ", erro
erro = calibration_util.computeCollinearityError(trasnformedPoints, dim[0], dim[1])
print "erro de colinearidade line fitting", erro

