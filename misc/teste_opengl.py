__author__ = 'sasha'

import OpenGL.GL as gl
import OpenGL.GLU as glu
import OpenGL.GLUT as glut
import sys
import numpy as np
from dotmap import DotMap

ballX = 0
ballY = 0
xSpeed = 0.02
ySpeed = 0.007
ballXMax = 0
ballXMin = 0
ballYMax = 0
ballYMin = 0
ballRadius = 0.5
clipAreaXLeft, clipAreaXRight, clipAreaYBottom, clipAreaYTop = 0, 0, 0, 0


class Quad:
    def __init__(self, vertices):
        self.vertices = vertices
        self.position = DotMap()
        self.position.x = 0
        self.position.y = 0

    def render(self):
        gl.glClear(gl.GL_COLOR_BUFFER_BIT)

        gl.glMatrixMode(gl.GL_MODELVIEW)

        gl.glLoadIdentity()
        gl.glTranslatef(self.position.x, self.position.y, 0.0)

        gl.glBegin(gl.GL_QUADS)
        gl.glColor3f(1, 1, 1)

        # square
        for i in range(0, 4):
            gl.glVertex3f(self.vertices[i][0], self.vertices[i][1], self.vertices[i][2])

        gl.glEnd()


def display():
    quad.render()

    glut.glutSwapBuffers()


def update_position():
    global ballX, ballY, xSpeed, ySpeed, ballXMax, ballXMin, ballYMax, ballYMin

    # Animation Control - compute the location for the next refresh
    ballX += xSpeed
    ballY += ySpeed
    # Check if the ball exceeds the edges
    if ballX > ballXMax:
        ballX = ballXMax
        xSpeed = -xSpeed
    elif ballX < ballXMin:
        ballX = ballXMin
        xSpeed = -xSpeed

    if ballY > ballYMax:
        ballY = ballYMax
        ySpeed = -ySpeed
    elif ballY < ballYMin:
        ballY = ballYMin
        ySpeed = -ySpeed


def reshape(width, height):
    global ballXMin, ballXMax, ballYMin, ballYMax, clipAreaXLeft, clipAreaXRight, clipAreaYBottom, clipAreaYTop, ballRadius

    # Compute aspect ratio of the window
    if (height == 0):
        height = 1
    # To prevent divide by 0
    aspect = width / float(height)

    # Set the viewport to cover the window
    gl.glViewport(0, 0, width, height)

    # Set the aspect ratio of the clipping area to match the viewport
    gl.glMatrixMode(gl.GL_PROJECTION)  # To operate on the Projection matrix
    gl.glLoadIdentity()  # Reset the projection matrix
    if width >= height:
        clipAreaXLeft = -1.0 * aspect
        clipAreaXRight = 1.0 * aspect
        clipAreaYBottom = -1.0
        clipAreaYTop = 1.0
    else:
        clipAreaXLeft = -1.0
        clipAreaXRight = 1.0
        clipAreaYBottom = -1.0 / aspect
        clipAreaYTop = 1.0 / aspect

    glu.gluOrtho2D(clipAreaXLeft, clipAreaXRight, clipAreaYBottom, clipAreaYTop)
    ballXMin = clipAreaXLeft + ballRadius
    ballXMax = clipAreaXRight - ballRadius
    ballYMin = clipAreaYBottom + ballRadius
    ballYMax = clipAreaYTop - ballRadius


def keyboard(key, x, y):
    if key == 'r':
        print 'rotate'
        quad.rotate = True
    if key == '\033':
        sys.exit()


def timer(value):
    glut.glutPostRedisplay()  # Post a paint request to activate display()
    glut.glutTimerFunc(30, timer, 0)  # subsequent timer call at milliseconds


if __name__ == "__main__":
    glut.glutInit()
    glut.glutInitDisplayMode(glut.GLUT_DOUBLE | glut.GLUT_RGBA)

    glut.glutCreateWindow('Hello world!')
    glut.glutTimerFunc(0, timer, 0)
    glut.glutKeyboardFunc(keyboard)
    glut.glutReshapeFunc(reshape)

    quad = Quad(np.array([[0.5, 0.5, 0], [-0.5, 0.5, 0],
                          [-0.5, -0.5, 0], [0.5, -0.5, 0]]))

    glut.glutDisplayFunc(display)
    glut.glutReshapeWindow(512, 512)
    glut.glutMainLoop()
