__author__ = 'sasha'

import cv2
import numpy as np
import random
import logging
from find_ring_grid import *


def drawlines(img_a, img_b, lines, pts1, pts2):
    ''' img1 - image on which we draw the epilines for the points in img2
        lines - corresponding epilines
        :param img_a: '''
    row, c = img_a.shape[:2]

    # img_a = cv2.cvtColor(img_a, cv2.COLOR_GRAY2BGR)
    # img_b = cv2.cvtColor(img_b, cv2.COLOR_GRAY2BGR)

    for r, pt1, pt2 in zip(lines, pts1, pts2):
        color = tuple(np.random.randint(0, 255, 3).tolist())
        x0, y0 = map(int, [0, -r[2] / r[1]])
        x1, y1 = map(int, [c, -(r[2] + r[0] * c) / r[1]])

        # print 'x0, y0 = ', x0, y0
        # print 'pt1 = ', pt1

        # img_a = cv2.line(img_a, (x0, y0), (x1, y1), color, 1)
        # img_a = cv2.circle(img_a, tuple(pt1), 5, color, -1)
        # img_b = cv2.circle(img_b, tuple(pt2), 5, color, -1)
        cv2.line(img_a, (x0, y0), (x1, y1), color, 1)
        cv2.circle(img_a, tuple(pt1), 5, color, -1)
        cv2.circle(img_b, tuple(pt2), 5, color, -1)

    # DrawingUtils.mostra_imagem("img_b", img_b)
    # print img_b
    return img_a, img_b


img1 = cv2.imread('../patterns/ring/sasha/img2.JPG', 1)
img2 = cv2.imread('../patterns/ring/sasha/img5.JPG', 1)
# cv2.imshow('img', img1)
# cv2.waitKey(0)
# DrawingUtils.mostra_imagem("img", img1)

found1, centers1 = RingsGrid.findRingsGrid(img1, (10, 7))
found2, centers2 = RingsGrid.findRingsGrid(img2, (10, 7))
# if found:
#     DrawingUtils.desenhaCentros_CV(img1, centers)
#     DrawingUtils.mostra_imagem("img", img1)

pts1 = centers1.reshape(-1, 2)
pts2 = centers2.reshape(-1, 2)

F, mask = cv2.findFundamentalMat(pts1, pts2, cv2.FM_LMEDS)
print 'F', F
# print 'mask', mask

# Find epilines corresponding to points in right image (second image) and
# drawing its lines on left image
lines1 = cv2.computeCorrespondEpilines(pts2.reshape(-1, 1, 2), 2, F)
lines1 = lines1.reshape(-1, 3)
DrawingUtils.mostra_imagem("img1", img1)
img5, img6 = drawlines(img1.copy(), img2.copy(), lines1, pts1, pts2)

# Find epilines corresponding to points in left image (first image) and
# drawing its lines on right image
lines2 = cv2.computeCorrespondEpilines(pts1.reshape(-1, 1, 2), 1, F)
lines2 = lines2.reshape(-1, 3)
# print lines2.shape
img3, img4 = drawlines(img2.copy(), img1.copy(), lines2, pts2, pts1)

# print 'img4 ', img4
# cv2.imshow('img', img3)
# cv2.waitKey(0)
DrawingUtils.mostra_imagem("img1", img5, wait_key_not_zero=True)
DrawingUtils.mostra_imagem("img2", img3)
