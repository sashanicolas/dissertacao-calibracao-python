# from matplotlib import pyplot as plt
# import numpy as np
#
# x = np.arange(0, 3, .25)
# y = np.sin(x)
# txt = '''
#     Lorem ipsum dolor sit amet, consectetur adipisicing elit,
#     sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
#     Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris
#     nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in
#     reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
#     pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
#     culpa qui officia deserunt mollit anim id est laborum.'''
#
# fig = plt.figure()
# ax1 = fig.add_axes((0.1, 0.4, 0.8, 0.5))
# # ax1 = fig.add_axes((0, 0, 1, 1))
# ax1.bar(x, y, .2)
# fig.text(0, 0, txt)
# plt.show()
# plt.savefig('teste.png')


# from numpy.random import random
# import matplotlib.pyplot as plt
#
# def print_fractures():
#     plt.subplot(122)
#     plt.plot([1,2,3,4])
#
# def histogram():
#     plt.subplot(121)
#     # ax = plt.axes([0.1,0.2,.5,.5])
#     # ax.plot(x, x*2)
#     plt.hist(random(1000), 100)
#     plt.xlabel('Spacing (m)', fontsize=15)
#     plt.ylabel('Frequency (count)', fontsize=15)
#
# plt.figure(figsize=(20, 8), dpi=80)
# histogram()
# print_fractures()
# plt.axis('equal')
# plt.show()


import numpy as np
import matplotlib.pyplot as plt

fig = plt.figure()
ax = fig.add_subplot(111)

t = np.arange(0.0, 5.0, 0.01)
s = np.cos(2*np.pi*t)
line, = ax.plot(t, s, lw=2)

# ax.annotate('local max', xy=[5, 1], xytext=(3, 1.5),
#             arrowprops=dict(facecolor='black', shrink=0.05)
#             )

ax.annotate('Minimo', xy=[5, 1], xycoords='data', xytext=(0, 30), textcoords='offset points', fontsize=16,
            arrowprops=dict(arrowstyle='->', connectionstyle='arc3, rad=.2')
            )

ax.set_ylim(-2,2)
plt.show()