__author__ = 'sasha'

import numpy as np
import cv2

cap = cv2.VideoCapture('C:/Users/sasha/PycharmProjects/videos/Video001_mpeg.avi')

length = int(cap.get(cv2.cv.CV_CAP_PROP_FRAME_COUNT))
width = int(cap.get(cv2.cv.CV_CAP_PROP_FRAME_WIDTH))
height = int(cap.get(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT))
fps = cap.get(cv2.cv.CV_CAP_PROP_FPS)
print length, width, height, fps

i = 1

while (cap.isOpened()):
    ret, frame = cap.read()

    cv2.imshow('frame', frame)

    k = cv2.waitKey(30)

    if k & 0xFF == ord('q'):
        break

    if k & 0xFF == ord('p'):
        k = cv2.waitKey(0)

        if k & 0xFF == ord('s'):  # salva
            path = 'C:/Users/sasha/PycharmProjects/CameraCalibration/patterns/ring/video1/'+ 'img'+ str(i)+ '.png'
            ret = cv2.imwrite(path, frame)
            print ret
            i += 1
            cv2.waitKey(30)

cap.release()
cv2.destroyAllWindows()
