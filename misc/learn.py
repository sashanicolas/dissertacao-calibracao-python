import numpy as np
# import cv2
from scipy.optimize import minimize

# from numpy import arange, sin, pi, random, array
# from scipy.optimize import leastsq
# import matplotlib.pyplot as plt
#
# x = arange(0, 6e-2, 6e-2 / 30)
# A, k, theta = 10, 1.0 / 3e-2, pi / 6
# y_true = A * sin(2 * pi * k * x + theta)
# y_meas = y_true + 2 * random.randn(len(x))
#
#
# def residuals(p, y, x):
#     A, k, theta = p
#     err = y - A * sin(2 * pi * k * x + theta)
#     return err
#
#
# def peval(x, p):
#     return p[0] * sin(2 * pi * p[1] * x + p[2])
#
#
# p0 = [8, 1 / 2.3e-2, pi / 3]
# p0 = array(p0)
#
# plsq = leastsq(residuals, p0, args=(y_meas, x))
# print plsq[0]
#
# print A, k, theta
#
# plt.plot(x, peval(x, plsq[0]), x, y_meas, 'o', x, y_true)
# plt.title('Least-squares fit to noisy data')
# plt.legend(['Fit', 'Noisy', 'True'])
# plt.show()

import theano
import theano.tensor as T
# from IPython.display import SVG

# a, b, c, d, e, f, g = T.fscalars('a', 'b', 'c', 'd', 'e', 'f', 'g')
# z = (a * (4 * b + 3 * c + d) + e * (3 * f + 2 * g)) / (3 * f + 2 * g)
# fz = theano.function([a, b, c, d, e, f, g], z)
# fz(1, 1, 1, 1, 1, 2, 3)
# dz_da = theano.grad(z, a)
# dfz_da = theano.function([a, b, c, d, e, f, g], dz_da)
# dfz_da(1, 1, 1, 1, 1, 2, 3)
# SVG(theano.printing.pydotprint(z, return_image=True, outfile="teste_imagem_derivada"))
# SVG(theano.printing.pydotprint(fz, return_image=True,
#                                format='svg'))
# theano.printing.debugprint(fz)
# print theano.printing.pprint(dz_da)

# a, b, c, d, e, f, g = T.fscalars('a', 'b', 'c', 'd', 'e', 'f', 'g')


def prepara_funcoes_theano():
    # define as variaveis
    var_x, var_y = T.fscalars('x', 'y')

    # forma a funcao
    func_f = 2 * var_x * var_y
    func_f = func_f + 2 * var_x
    func_f = func_f - var_x ** 2 - 2 * var_y ** 2
    comp_func_f = theano.function([var_x, var_y], func_f, allow_input_downcast=True)

    # calcula as derivadas
    df_dx = theano.grad(func_f, var_x)
    comp_func_df_dx = theano.function([var_x, var_y], df_dx, allow_input_downcast=True)

    df_dy = theano.grad(func_f, var_y)
    comp_func_df_dy = theano.function([var_x, var_y], df_dy, allow_input_downcast=True)

    return comp_func_f, [comp_func_df_dx, comp_func_df_dy]


calls_of = 0
calls_d = 0

def func(x, comp_func_f, derivadas):
    global calls_of
    calls_of += 1

    """ Objective function """
    # return -1.0 * (2 * x[0] * x[1] + 2 * x[0] - x[0] ** 2 - 2 * x[1] ** 2)
    return -1.0 * comp_func_f(x[0], x[1])


def func_deriv(x, comp_func_f, derivadas):
    global calls_d
    calls_d += 1

    """ Derivative of objective function """
    # dfdx0 = -1.0 * (-2 * x[0] + 2 * x[1] + 2)
    # dfdx1 = -1.0 * (2 * x[0] - 4 * x[1])

    dfdx0 = -1.0 * derivadas[0](x[0], x[1])
    dfdx1 = -1.0 * derivadas[1](x[0], x[1])

    return np.array([dfdx0, dfdx1])


cons = ({'type': 'eq',
         'fun': lambda x: np.array([x[0] ** 3 - x[1]]),
         'jac': lambda x: np.array([3.0 * (x[0] ** 2.0), -1.0])},
        {'type': 'ineq',
         'fun': lambda x: np.array([x[1] - 1]),
         'jac': lambda x: np.array([0.0, 1.0])})

comp_func_f, derivadas = prepara_funcoes_theano()
res = minimize(func, [-1.0, 1.0], args=(comp_func_f, derivadas, ), jac=func_deriv,
               constraints=cons, method='SLSQP', options={'disp': True})

print(res.x)

print 'call objective function ', calls_of
print 'call derivada ', calls_d