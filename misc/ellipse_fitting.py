import cv2
import numpy as np
from find_ring_grid import DrawingUtils
import math

img = np.zeros((640, 640, 3))
img.fill(0.98)

points = np.ndarray(dtype=np.float32, shape=(90, 2))
for i in range(0, 90, 1):
    p = -math.sin(np.deg2rad(i)) * 100.0
    q = -math.cos(np.deg2rad(i)) * 100.0
    points[i][0] = p
    points[i][1] = q

points = np.resize(points, new_shape=(360, 2))
for i in range(0, 90, 1):
    points[i + 90][0] = -points[i][0]
    points[i + 90][1] = -points[i][1]

    points[i + 180][0] = -points[i][0]
    points[i + 180][1] = points[i][1]

    points[i + 270][0] = points[i][0]
    points[i + 270][1] = -points[i][1]

points = (points + [50, 50]) * [1, 1 / 3.0] + [200, 400]
# points = np.append(points, [250, 403])
points[29] = [250, 133]

# print points
l = len(points)
newP = np.reshape(points, newshape=(l, 1, 2)).astype(int)
print newP
e = cv2.fitEllipse(newP)
print e

center = np.array(list(e[0]))
axis = np.array(list(e[1]))
angle = e[2]

# cv2.ellipse(img, (256, 256), (100, 50), 0, 0, 360, (0, 1, 1), -1)

cv2.ellipse(img,
            tuple(center.astype(int)),
            tuple((axis / 2.0).astype(int)),
            angle, 0, 360, (0, 1, 0), -1)

DrawingUtils.desenhaCentros(img, points, color=(1, 0, 0))
DrawingUtils.mostra_imagem('img', img)
