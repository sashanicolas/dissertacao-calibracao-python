import cv2
from glob import glob
from settings_flags import *
from sys import platform as _platform

__author__ = 'sasha'

default_pattern = 'circle'
default_resolution = '640x480'

pattern_options = ['circle', 'ring']
subfolder_options = ['640x480', 'ankur', 'kinect16', 'video1', 'video1_plus',
                     'Lens_2f_63z', 'Lens_0.4f_15z', 'new_manuel_data', 'manuel_set2', 'gopro_set1']


def getImages(pattern='circle', subfolder='640x480', directory=None):
    if directory is not None:
        images_paths = glob('{directory}/*.jpg'.format(directory=directory))
        images_paths.extend(glob('{directory}/*.bmp'.format(directory=directory)))
    else:
        if pattern not in pattern_options:
            print 'Source not available. Serving {}'.format(default_pattern)
            pattern = default_pattern

        if subfolder not in subfolder_options:
            print 'Source not available. Check subfolder_options. Serving {}'.format(default_resolution)
            subfolder = default_resolution

        images_paths = glob('patterns/{pattern}/{subfolder}/*.jpg'.format(pattern=pattern, subfolder=subfolder))
        images_paths.extend(glob('patterns/{pattern}/{subfolder}/*.bmp'.format(pattern=pattern, subfolder=subfolder)))
        images_paths.extend(glob('patterns/{pattern}/{subfolder}/*.png'.format(pattern=pattern, subfolder=subfolder)))
        if _platform == "darwin" or _platform == "linux2":
            images_paths.extend(
                glob('patterns/{pattern}/{subfolder}/*.JPG'.format(pattern=pattern, subfolder=subfolder)))
            images_paths.extend(
                glob('patterns/{pattern}/{subfolder}/*.BMP'.format(pattern=pattern, subfolder=subfolder)))
            images_paths.extend(
                glob('patterns/{pattern}/{subfolder}/*.PNG'.format(pattern=pattern, subfolder=subfolder)))

    original_images = []

    print '\nAbrindo imagens ...'
    for path in images_paths:
        if SHOW_MESSAGE_ABRINDO_IMAGENS:
            print '  ...', path
        original_images.append(cv2.imread(path, cv2.IMREAD_UNCHANGED))
    print ' ...Done'
    return images_paths, original_images, pattern
