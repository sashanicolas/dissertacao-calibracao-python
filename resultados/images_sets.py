####################################################
#                                                  #
#              Conjuntos de Imagens                #
#                                                  #
####################################################

# padrao, fonte, pattern_size, legenda, circulo_dist = 'circle', '640x480', (10, 7), "Circle Sasha", 23.0
# padrao, fonte, pattern_size, legenda, circulo_dist = 'circle', 'ankur', (10, 7), "Circle ankur", 23.0
# padrao, fonte, pattern_size, legenda, circulo_dist = 'circle', 'kinect16', (10, 7), "Circle kinect16", 23.0

# padrao, fonte, pattern_size, legenda, circulo_dist = 'ring', '640x480', (10, 7), "Ring Sasha", 23.0
# padrao, fonte, pattern_size, legenda, circulo_dist = 'ring', 'kinect16', (10, 7), "Ring kinect16", 23.0
padrao, fonte, pattern_size, legenda, circulo_dist = 'ring', 'ankur', (10, 7), "Ring ankur", 23.0
# padrao, fonte, pattern_size, legenda, circulo_dist = 'ring', 'video1', (7, 5), "Ring video1", 23.0
# padrao, fonte, pattern_size, legenda, circulo_dist = 'ring', 'video1_plus', (7, 5), "Ring video1_plus", 23.0
# padrao, fonte, pattern_size, legenda, circulo_dist = 'ring', 'Lens_2f_63z', (4, 3), "Ring Lens_2f_63z", 57.7
# padrao, fonte, pattern_size, legenda, circulo_dist = 'ring', 'Lens_0.4f_15z', (4, 3), "Ring Lens_0.4f_15z", 57.7


imagens_sets = [
    {
        'label': "Ring Kinect",
        'pattern': 'ring',
        'folder': 'kinect16',
        'pattern_dimension': (10, 7),
        'milimeters': 23.0,
        'activated': True
    },
    {
        'label': "Ring Kinect",
        'pattern': 'ring',
        'folder': 'kinect16',
        'pattern_dimension': (10, 7),
        'milimeters': 23.0,
        'activated': True
    },
    {
        'label': "Ring Kinect",
        'pattern': 'ring',
        'folder': 'kinect16',
        'pattern_dimension': (10, 7),
        'milimeters': 23.0,
        'activated': True
    }
]