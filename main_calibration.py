import images_input
import camera_calibration_opencv
import camera_calibration_sasha_nicolas

__author__ = 'sasha'

debug_found = True
subfolder = 'ankur'  # '640x480'

while True:
    print '\n\nO que deseja fazer:'
    print '1 - Rodar Calibracao do OpenCV - circle'
    print '2 - Rodar Calibracao do OpenCV - ring'
    print '3 - Rodar Calibracao do OpenCV + Refinamento Iterativo (Ankur Datta) - Sasha'

    option = int(raw_input())

    if option == 1:
        images_paths, original_images, pattern = images_input.getImages('circle', subfolder)
        camera_calibration_opencv.calibrate(images_paths, original_images, pattern, debug_found=debug_found)
    elif option == 2:
        images_paths, original_images, pattern = images_input.getImages('ring', subfolder)
        camera_calibration_opencv.calibrate(images_paths, original_images, pattern, debug_found=debug_found)
    elif option == 3:
        images_paths, original_images, pattern = images_input.getImages('ring', subfolder)
        camera_calibration_sasha_nicolas.calibrate(images_paths, original_images, pattern, debug_found=debug_found)
    else:
        print 'Opcao invalida'
